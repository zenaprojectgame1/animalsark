﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.Scripting.PreserveAttribute
struct PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// UnityEngine.SpaceAttribute
struct SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAsyncWaitForCompletionU3Ed__10_tAF83373BEFDCE5CDEE1D8D2213CE8CD7384CAC24_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAsyncWaitForElapsedLoopsU3Ed__13_t7E11D4778BC9859A55D5F850CD6DE389D4AC372A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAsyncWaitForKillU3Ed__12_t69F4C18D4B977504979616766BA83349830366AB_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAsyncWaitForPositionU3Ed__14_t8C34DB89D763058E32F5FECE7E62363A58303AE9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAsyncWaitForRewindU3Ed__11_t3CC63CE2CBCA49F9CADC6697FDDE8026B073132A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAsyncWaitForStartU3Ed__15_t6B840A7C6ECC2E009C3BC88B5DC9C937FBD80571_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCoHideU3Ed__6_tC237FBDC25A5EFD0AAD99C392FDCE64A2768E320_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCoHideU3Ed__6_tE1F78DB53A206B1707F2E85DB6DF244601F6EB5D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCoShowU3Ed__4_t2C33969A872D8A0E3B5B3422480C1459829043A6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCoShowU3Ed__4_tE8095F3AF162E6485D1F25668AA2BC0F0DB9FA25_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCoShowU3Ed__6_t70B62078D15F0FA5A50528A198DE50839239D5C5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CEffectLoopU3Ed__6_t25BAA433CE780795116A92A22C987A2CE9108F43_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadingToTopU3Ed__6_t1508D394749F69965BFAB2EDE4AC4CCDCDAA3948_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnShownFakeU3Ed__15_t767E7A793AE8EACD38B7D3C72FB0F6F574A56519_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRespawnU3Ed__7_t2F0FDE3C689F1226C7989D80298592F01E221CFC_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__0_tA0D462AF96FB395077D488099A998778AE762B12_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__30_t3226DA98802C80F9E4B46193F538AC97C51FC7ED_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Scripting.PreserveAttribute
struct PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.SpaceAttribute
struct SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.SpaceAttribute::height
	float ___height_0;

public:
	inline static int32_t get_offset_of_height_0() { return static_cast<int32_t>(offsetof(SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8, ___height_0)); }
	inline float get_height_0() const { return ___height_0; }
	inline float* get_address_of_height_0() { return &___height_0; }
	inline void set_height_0(float value)
	{
		___height_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44 (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * __this, float ___height0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void UnityEngine.Scripting.PreserveAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2 (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * __this, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[0];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[1];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[3];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
}
static void DemoController_t54E17C5DEE8D3AA65E5108AE99AFC0FFFC300A6F_CustomAttributesCacheGenerator_anim(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 10.0f, NULL);
	}
}
static void ControllerAnimal_tF06BE9A9E514CC46B7DE91D2C15FA5BB12D89B03_CustomAttributesCacheGenerator_m_Rigidbody(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ControllerAnimal_tF06BE9A9E514CC46B7DE91D2C15FA5BB12D89B03_CustomAttributesCacheGenerator_m_ConfigurableJoint(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RandomFlyer_tF96D498F1ABFF9A406B8CAEC7B285E07ED101305_CustomAttributesCacheGenerator_value(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void RectTransformExtensions_t9F14817931A99EDF0C9B376B0A277ACB51281904_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectTransformExtensions_t9F14817931A99EDF0C9B376B0A277ACB51281904_CustomAttributesCacheGenerator_RectTransformExtensions_SetLeft_mA86C085FF57265E25F8732197C2FA7E4E3377F05(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectTransformExtensions_t9F14817931A99EDF0C9B376B0A277ACB51281904_CustomAttributesCacheGenerator_RectTransformExtensions_SetRight_m41A7631DD85392C568598C634B73D1A7CA9C3BA8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectTransformExtensions_t9F14817931A99EDF0C9B376B0A277ACB51281904_CustomAttributesCacheGenerator_RectTransformExtensions_SetTop_m8E0112409A0B4AE8678D326E04149A7422CC0702(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectTransformExtensions_t9F14817931A99EDF0C9B376B0A277ACB51281904_CustomAttributesCacheGenerator_RectTransformExtensions_SetBottom_mD6AC62815DA40BDA0158DDBF0DBC8AFAF8B9C038(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void AudioManager_tD91555488B83E322DEC589BDB624FC46E66CB148_CustomAttributesCacheGenerator_m_BgmPath(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AudioManager_tD91555488B83E322DEC589BDB624FC46E66CB148_CustomAttributesCacheGenerator_m_SfxPath(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AudioManager_tD91555488B83E322DEC589BDB624FC46E66CB148_CustomAttributesCacheGenerator_m_VoicePath(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AudioManager_tD91555488B83E322DEC589BDB624FC46E66CB148_CustomAttributesCacheGenerator_m_ButtonTapSfx(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AudioManager_tD91555488B83E322DEC589BDB624FC46E66CB148_CustomAttributesCacheGenerator_m_BgmSource(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AudioManager_tD91555488B83E322DEC589BDB624FC46E66CB148_CustomAttributesCacheGenerator_m_SfxSource(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AudioManager_tD91555488B83E322DEC589BDB624FC46E66CB148_CustomAttributesCacheGenerator_m_VoiceSource(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AudioManager_tD91555488B83E322DEC589BDB624FC46E66CB148_CustomAttributesCacheGenerator_U3CinstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AudioManager_tD91555488B83E322DEC589BDB624FC46E66CB148_CustomAttributesCacheGenerator_AudioManager_get_instance_m36FB14B490ECEFC3EBF150336F7547156DFB1B8D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AudioManager_tD91555488B83E322DEC589BDB624FC46E66CB148_CustomAttributesCacheGenerator_AudioManager_set_instance_m666A0E6C0CF110F46FA9BF050EFFA22214C7D674(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ETFXProjectileScript_tED63BF9EB347489D6D56FF6E6265BA4EC7457B03_CustomAttributesCacheGenerator_colliderRadius(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x64\x6A\x75\x73\x74\x20\x69\x66\x20\x6E\x6F\x74\x20\x75\x73\x69\x6E\x67\x20\x53\x70\x68\x65\x72\x65\x20\x43\x6F\x6C\x6C\x69\x64\x65\x72"), NULL);
	}
}
static void ETFXProjectileScript_tED63BF9EB347489D6D56FF6E6265BA4EC7457B03_CustomAttributesCacheGenerator_collideOffset(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void MgAnimationFade_t8DC6F26A99BAB51886BE866591B651E89629E2A0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F_0_0_0_var), NULL);
	}
}
static void MgAnimationFade_t8DC6F26A99BAB51886BE866591B651E89629E2A0_CustomAttributesCacheGenerator_animDuration(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MgAnimationLink_t3FAE4F7171808AABAA99133DAB0552DDB36AA946_CustomAttributesCacheGenerator_m_Link(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MgAnimationLink_t3FAE4F7171808AABAA99133DAB0552DDB36AA946_CustomAttributesCacheGenerator_delay(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MgAnimationLink_t3FAE4F7171808AABAA99133DAB0552DDB36AA946_CustomAttributesCacheGenerator_delayHide(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MgAnimationLink_t3FAE4F7171808AABAA99133DAB0552DDB36AA946_CustomAttributesCacheGenerator_MgAnimationLink_CoShow_mE5861550ECA367E4BACE78F17D99DF226500D8CC(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCoShowU3Ed__4_tE8095F3AF162E6485D1F25668AA2BC0F0DB9FA25_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCoShowU3Ed__4_tE8095F3AF162E6485D1F25668AA2BC0F0DB9FA25_0_0_0_var), NULL);
	}
}
static void MgAnimationLink_t3FAE4F7171808AABAA99133DAB0552DDB36AA946_CustomAttributesCacheGenerator_MgAnimationLink_CoHide_m8A0E7D3AD3E93E482FC47FE0DC472A5A5286538B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCoHideU3Ed__6_tC237FBDC25A5EFD0AAD99C392FDCE64A2768E320_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCoHideU3Ed__6_tC237FBDC25A5EFD0AAD99C392FDCE64A2768E320_0_0_0_var), NULL);
	}
}
static void U3CCoShowU3Ed__4_tE8095F3AF162E6485D1F25668AA2BC0F0DB9FA25_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCoShowU3Ed__4_tE8095F3AF162E6485D1F25668AA2BC0F0DB9FA25_CustomAttributesCacheGenerator_U3CCoShowU3Ed__4__ctor_m554EB8574D3EC74588B8C229FEA1C0B0A291546F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoShowU3Ed__4_tE8095F3AF162E6485D1F25668AA2BC0F0DB9FA25_CustomAttributesCacheGenerator_U3CCoShowU3Ed__4_System_IDisposable_Dispose_m07C82A5E84196EA1B1972E780C6DD1FE46574BC2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoShowU3Ed__4_tE8095F3AF162E6485D1F25668AA2BC0F0DB9FA25_CustomAttributesCacheGenerator_U3CCoShowU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC9030BE2687FCFE6F576F56732CE39EEC045140B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoShowU3Ed__4_tE8095F3AF162E6485D1F25668AA2BC0F0DB9FA25_CustomAttributesCacheGenerator_U3CCoShowU3Ed__4_System_Collections_IEnumerator_Reset_m964546A98807BDFDDFB6F9B948CFEA111004BC87(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoShowU3Ed__4_tE8095F3AF162E6485D1F25668AA2BC0F0DB9FA25_CustomAttributesCacheGenerator_U3CCoShowU3Ed__4_System_Collections_IEnumerator_get_Current_m5F2C6965EC88D18494A8D38BDD4716692853BB18(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoHideU3Ed__6_tC237FBDC25A5EFD0AAD99C392FDCE64A2768E320_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCoHideU3Ed__6_tC237FBDC25A5EFD0AAD99C392FDCE64A2768E320_CustomAttributesCacheGenerator_U3CCoHideU3Ed__6__ctor_m4090DB038FFA544ADA13CD42C5846426F206DC1F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoHideU3Ed__6_tC237FBDC25A5EFD0AAD99C392FDCE64A2768E320_CustomAttributesCacheGenerator_U3CCoHideU3Ed__6_System_IDisposable_Dispose_m6FDB5A58039591E3210957C83AE99716A836B3D8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoHideU3Ed__6_tC237FBDC25A5EFD0AAD99C392FDCE64A2768E320_CustomAttributesCacheGenerator_U3CCoHideU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE3180BCF97A6E0B69FEC3031203639BF46203B01(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoHideU3Ed__6_tC237FBDC25A5EFD0AAD99C392FDCE64A2768E320_CustomAttributesCacheGenerator_U3CCoHideU3Ed__6_System_Collections_IEnumerator_Reset_m08C249CB2DAB83DFC80C43F0AADBF65BEF26E138(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoHideU3Ed__6_tC237FBDC25A5EFD0AAD99C392FDCE64A2768E320_CustomAttributesCacheGenerator_U3CCoHideU3Ed__6_System_Collections_IEnumerator_get_Current_mAC9F050CAA3A19D7A27B8D7BEFC50B0F77039748(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MgAnimationLinkObject_t0CB17FF21BD5ACAE7ABB251CA9C35698C3E3E111_CustomAttributesCacheGenerator_m_Link(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MgAnimationLinkObject_t0CB17FF21BD5ACAE7ABB251CA9C35698C3E3E111_CustomAttributesCacheGenerator_delay(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MgAnimationLinkObject_t0CB17FF21BD5ACAE7ABB251CA9C35698C3E3E111_CustomAttributesCacheGenerator_delayHide(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MgAnimationLinkObject_t0CB17FF21BD5ACAE7ABB251CA9C35698C3E3E111_CustomAttributesCacheGenerator_MgAnimationLinkObject_CoShow_mDC6098417478F81E119F7A1ED3CFF28504F10B8C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCoShowU3Ed__4_t2C33969A872D8A0E3B5B3422480C1459829043A6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCoShowU3Ed__4_t2C33969A872D8A0E3B5B3422480C1459829043A6_0_0_0_var), NULL);
	}
}
static void MgAnimationLinkObject_t0CB17FF21BD5ACAE7ABB251CA9C35698C3E3E111_CustomAttributesCacheGenerator_MgAnimationLinkObject_CoHide_m343BB45CFA8555A43752D7CC98DBD9AA14F2FF2C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCoHideU3Ed__6_tE1F78DB53A206B1707F2E85DB6DF244601F6EB5D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCoHideU3Ed__6_tE1F78DB53A206B1707F2E85DB6DF244601F6EB5D_0_0_0_var), NULL);
	}
}
static void U3CCoShowU3Ed__4_t2C33969A872D8A0E3B5B3422480C1459829043A6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCoShowU3Ed__4_t2C33969A872D8A0E3B5B3422480C1459829043A6_CustomAttributesCacheGenerator_U3CCoShowU3Ed__4__ctor_m1726143C8B21533706114E0E90EFA33FAC5D33DA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoShowU3Ed__4_t2C33969A872D8A0E3B5B3422480C1459829043A6_CustomAttributesCacheGenerator_U3CCoShowU3Ed__4_System_IDisposable_Dispose_mBD83DECBF938630809CFC6823D1BBF2287B14AF1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoShowU3Ed__4_t2C33969A872D8A0E3B5B3422480C1459829043A6_CustomAttributesCacheGenerator_U3CCoShowU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6AA628E39ECD848EB5417D012CCABAA563FAC3EB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoShowU3Ed__4_t2C33969A872D8A0E3B5B3422480C1459829043A6_CustomAttributesCacheGenerator_U3CCoShowU3Ed__4_System_Collections_IEnumerator_Reset_mA33FD166C110C0853517FB614C738B06DEC9520D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoShowU3Ed__4_t2C33969A872D8A0E3B5B3422480C1459829043A6_CustomAttributesCacheGenerator_U3CCoShowU3Ed__4_System_Collections_IEnumerator_get_Current_mC02634706ABCCA9E4F9C808BE0475E9042DA01AF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoHideU3Ed__6_tE1F78DB53A206B1707F2E85DB6DF244601F6EB5D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCoHideU3Ed__6_tE1F78DB53A206B1707F2E85DB6DF244601F6EB5D_CustomAttributesCacheGenerator_U3CCoHideU3Ed__6__ctor_m1B9365AD609078A363564D5938D192A89625AF71(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoHideU3Ed__6_tE1F78DB53A206B1707F2E85DB6DF244601F6EB5D_CustomAttributesCacheGenerator_U3CCoHideU3Ed__6_System_IDisposable_Dispose_mBD6FC0B545911FAB141FC358F1C67688DEA32191(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoHideU3Ed__6_tE1F78DB53A206B1707F2E85DB6DF244601F6EB5D_CustomAttributesCacheGenerator_U3CCoHideU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m43DF0B545FB7DD89B2572D0705D2F83C4F05CF9D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoHideU3Ed__6_tE1F78DB53A206B1707F2E85DB6DF244601F6EB5D_CustomAttributesCacheGenerator_U3CCoHideU3Ed__6_System_Collections_IEnumerator_Reset_m304762ABA63837D8B6483A489789E6061CBAA982(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoHideU3Ed__6_tE1F78DB53A206B1707F2E85DB6DF244601F6EB5D_CustomAttributesCacheGenerator_U3CCoHideU3Ed__6_System_Collections_IEnumerator_get_Current_m7F143AAD687B3EE3B6309FBF1FAB3E547A681355(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MgAnimationMove_tAB0EC396D9DB19ED467027171AEB84B1344F1EDB_CustomAttributesCacheGenerator_m_Start(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MgAnimationMove_tAB0EC396D9DB19ED467027171AEB84B1344F1EDB_CustomAttributesCacheGenerator_m_End(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MgAnimationMove_tAB0EC396D9DB19ED467027171AEB84B1344F1EDB_CustomAttributesCacheGenerator_animDuration(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MgAnimationMove_tAB0EC396D9DB19ED467027171AEB84B1344F1EDB_CustomAttributesCacheGenerator_showEase(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MgAnimationMove_tAB0EC396D9DB19ED467027171AEB84B1344F1EDB_CustomAttributesCacheGenerator_hideEase(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MgAnimationMove_tAB0EC396D9DB19ED467027171AEB84B1344F1EDB_CustomAttributesCacheGenerator_moveEase(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MgAnimationPScaleLoop_tBA3F73A255354D6BABD22AFC7D7FBAFEA057917E_CustomAttributesCacheGenerator_MgAnimationPScaleLoop_U3CLoopAnimU3Eb__5_0_m62EB2B13EFD62CF7C44BFEFA5FE44EF1ADAC7292(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MgAnimationUnity_t59C8A3C4965C83BBABCD5C2F27ACD224938C81F1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var), NULL);
	}
}
static void MgAnimationUnity_t59C8A3C4965C83BBABCD5C2F27ACD224938C81F1_CustomAttributesCacheGenerator_customDelay(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MgAnimationUnity_t59C8A3C4965C83BBABCD5C2F27ACD224938C81F1_CustomAttributesCacheGenerator_showAnim_paramName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MgAnimationUnity_t59C8A3C4965C83BBABCD5C2F27ACD224938C81F1_CustomAttributesCacheGenerator_hideAnim_paramName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MgAnimationUnity_t59C8A3C4965C83BBABCD5C2F27ACD224938C81F1_CustomAttributesCacheGenerator_MgAnimationUnity_CoShow_m45D03ACE03ADF8509F5F30851DF9333DE68A0B87(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCoShowU3Ed__6_t70B62078D15F0FA5A50528A198DE50839239D5C5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCoShowU3Ed__6_t70B62078D15F0FA5A50528A198DE50839239D5C5_0_0_0_var), NULL);
	}
}
static void U3CCoShowU3Ed__6_t70B62078D15F0FA5A50528A198DE50839239D5C5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCoShowU3Ed__6_t70B62078D15F0FA5A50528A198DE50839239D5C5_CustomAttributesCacheGenerator_U3CCoShowU3Ed__6__ctor_mFFA3E76623E10E9F668CA1CBF411F626D53964A1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoShowU3Ed__6_t70B62078D15F0FA5A50528A198DE50839239D5C5_CustomAttributesCacheGenerator_U3CCoShowU3Ed__6_System_IDisposable_Dispose_m0072088E4B8ED1D71D8EA1F6AEB9FAF65095113B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoShowU3Ed__6_t70B62078D15F0FA5A50528A198DE50839239D5C5_CustomAttributesCacheGenerator_U3CCoShowU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m72E89C7F88FFDBF545B5F398A67CA5515106A0BB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoShowU3Ed__6_t70B62078D15F0FA5A50528A198DE50839239D5C5_CustomAttributesCacheGenerator_U3CCoShowU3Ed__6_System_Collections_IEnumerator_Reset_m19937A01FE7B527C7B7DE39A1360330701041EC5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoShowU3Ed__6_t70B62078D15F0FA5A50528A198DE50839239D5C5_CustomAttributesCacheGenerator_U3CCoShowU3Ed__6_System_Collections_IEnumerator_get_Current_mC04A44C677D01377DA40F515F01E51814858CD9D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MgObject_tF1EE47FAAF51C4095CEC6D76BE6A0C59B4A1F99A_CustomAttributesCacheGenerator_MgObject_U3CShakeU3Eb__10_0_mCA287CDA1850D00060EE3FCAF67CAF86CB310910(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MgObject_tF1EE47FAAF51C4095CEC6D76BE6A0C59B4A1F99A_CustomAttributesCacheGenerator_MgObject_U3CPunchScaleU3Eb__11_0_m523F3F64B37548FC411AF2A549713B8806752E3F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DGameController_t0982F5E1D62017B811C5390685DD2A5E7D2B633E_CustomAttributesCacheGenerator_DGameController_LoadingToTop_m16D87897ACBBDCDB17EA184E1CD158D8EDF252FC(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadingToTopU3Ed__6_t1508D394749F69965BFAB2EDE4AC4CCDCDAA3948_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadingToTopU3Ed__6_t1508D394749F69965BFAB2EDE4AC4CCDCDAA3948_0_0_0_var), NULL);
	}
}
static void U3CLoadingToTopU3Ed__6_t1508D394749F69965BFAB2EDE4AC4CCDCDAA3948_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadingToTopU3Ed__6_t1508D394749F69965BFAB2EDE4AC4CCDCDAA3948_CustomAttributesCacheGenerator_U3CLoadingToTopU3Ed__6__ctor_m4DF50C93EAB6A04080FF76FE71AFB1D9E2C85569(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadingToTopU3Ed__6_t1508D394749F69965BFAB2EDE4AC4CCDCDAA3948_CustomAttributesCacheGenerator_U3CLoadingToTopU3Ed__6_System_IDisposable_Dispose_m55B486431095FC8199CE1CC08EDC327E83B15BC0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadingToTopU3Ed__6_t1508D394749F69965BFAB2EDE4AC4CCDCDAA3948_CustomAttributesCacheGenerator_U3CLoadingToTopU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m359167E9C986E70CB8901CA63CBE7C418B2F3A7D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadingToTopU3Ed__6_t1508D394749F69965BFAB2EDE4AC4CCDCDAA3948_CustomAttributesCacheGenerator_U3CLoadingToTopU3Ed__6_System_Collections_IEnumerator_Reset_mFBBF4C2124B15D0F6FF9516AB15AB66D50A53879(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadingToTopU3Ed__6_t1508D394749F69965BFAB2EDE4AC4CCDCDAA3948_CustomAttributesCacheGenerator_U3CLoadingToTopU3Ed__6_System_Collections_IEnumerator_get_Current_m0D7A10CDEA3F5BB3B9B90102ED2E62EB48B600EB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DMain_tA881A8A301A40DE2168DF9A1F5F716C9A2D4B7AE_CustomAttributesCacheGenerator_DMain_Start_mCB88609F3C58BD213EAC9D1203B1A69610179042(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__0_tA0D462AF96FB395077D488099A998778AE762B12_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__0_tA0D462AF96FB395077D488099A998778AE762B12_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__0_tA0D462AF96FB395077D488099A998778AE762B12_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__0_tA0D462AF96FB395077D488099A998778AE762B12_CustomAttributesCacheGenerator_U3CStartU3Ed__0__ctor_mE4C50085C2DAE6B9AE876845A98A256A8CFD3E3B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__0_tA0D462AF96FB395077D488099A998778AE762B12_CustomAttributesCacheGenerator_U3CStartU3Ed__0_System_IDisposable_Dispose_mCB8CDA3D64F78F8C356E9A9F80DA4E48EE53F5CB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__0_tA0D462AF96FB395077D488099A998778AE762B12_CustomAttributesCacheGenerator_U3CStartU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mACE8A452F4692BC6E0FC38AC9DBB35072A007AC1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__0_tA0D462AF96FB395077D488099A998778AE762B12_CustomAttributesCacheGenerator_U3CStartU3Ed__0_System_Collections_IEnumerator_Reset_m0CB66531627AC524221E203F6FFBC2A527CDEC74(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__0_tA0D462AF96FB395077D488099A998778AE762B12_CustomAttributesCacheGenerator_U3CStartU3Ed__0_System_Collections_IEnumerator_get_Current_mC2698620ADBE0D6372150B44731A486A9F313AA3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DPopupController_tD2B8D5BB72BBC82747E5B6D76ACE0E4083A41EC7_CustomAttributesCacheGenerator_m_Text(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec_t6981D3FEAACC85C8C791C504710DBE30733CD5C1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SceneAnimation_t903268BBAB234E7C9E6489A124937DB719143682_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void SceneAnimation_t903268BBAB234E7C9E6489A124937DB719143682_CustomAttributesCacheGenerator_m_Controller(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SceneDefaultAnimation_tB8AD18A8003BE1661928EED784456BEFC4FDA9D9_CustomAttributesCacheGenerator_m_AnimationType(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SceneDefaultAnimation_tB8AD18A8003BE1661928EED784456BEFC4FDA9D9_CustomAttributesCacheGenerator_m_ShowEaseType(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SceneDefaultAnimation_tB8AD18A8003BE1661928EED784456BEFC4FDA9D9_CustomAttributesCacheGenerator_m_HideEaseType(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SceneLegacyAnimation_tAEBE1D3E936E86F2345DA287CD0C467D6A7D10C2_CustomAttributesCacheGenerator_m_ShowAnimName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SceneLegacyAnimation_tAEBE1D3E936E86F2345DA287CD0C467D6A7D10C2_CustomAttributesCacheGenerator_m_HideAnimName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Controller_t88C72B3044FCDB927F2FFEF7A44CE97D95FA1D2F_CustomAttributesCacheGenerator_m_Canvas(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Controller_t88C72B3044FCDB927F2FFEF7A44CE97D95FA1D2F_CustomAttributesCacheGenerator_m_Camera(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Controller_t88C72B3044FCDB927F2FFEF7A44CE97D95FA1D2F_CustomAttributesCacheGenerator_FullScreen(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Controller_t88C72B3044FCDB927F2FFEF7A44CE97D95FA1D2F_CustomAttributesCacheGenerator_Animation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Controller_t88C72B3044FCDB927F2FFEF7A44CE97D95FA1D2F_CustomAttributesCacheGenerator_U3CDataU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Controller_t88C72B3044FCDB927F2FFEF7A44CE97D95FA1D2F_CustomAttributesCacheGenerator_Controller_get_Data_m5FBB080ED88F0FCCE300D8AE0BE74E1B7B66D6B4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Controller_t88C72B3044FCDB927F2FFEF7A44CE97D95FA1D2F_CustomAttributesCacheGenerator_Controller_set_Data_m134299AB8FCB15566C6B2AE114B040FF8FEE2E71(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Manager_tCB461C15C123767AFE54F53DB85B845D6678B928_CustomAttributesCacheGenerator_U3CShieldColorU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Manager_tCB461C15C123767AFE54F53DB85B845D6678B928_CustomAttributesCacheGenerator_U3CSceneFadeInDurationU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Manager_tCB461C15C123767AFE54F53DB85B845D6678B928_CustomAttributesCacheGenerator_U3CSceneFadeOutDurationU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Manager_tCB461C15C123767AFE54F53DB85B845D6678B928_CustomAttributesCacheGenerator_U3CSceneAnimationDurationU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Manager_tCB461C15C123767AFE54F53DB85B845D6678B928_CustomAttributesCacheGenerator_U3CObjectU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Manager_tCB461C15C123767AFE54F53DB85B845D6678B928_CustomAttributesCacheGenerator_Manager_get_ShieldColor_mEDF473CD5B3B0E921EDCC9A8ECDED1A59AEAF66C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Manager_tCB461C15C123767AFE54F53DB85B845D6678B928_CustomAttributesCacheGenerator_Manager_set_ShieldColor_mFAC7D6CD1EA0F67EE06DB6900180748261E1ABB9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Manager_tCB461C15C123767AFE54F53DB85B845D6678B928_CustomAttributesCacheGenerator_Manager_get_SceneFadeInDuration_m98102617D5859290DAA2F5E61A47ED72EF3AD434(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Manager_tCB461C15C123767AFE54F53DB85B845D6678B928_CustomAttributesCacheGenerator_Manager_set_SceneFadeInDuration_m2DA5251F0F6483A90F57212516A186F19A45FAE6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Manager_tCB461C15C123767AFE54F53DB85B845D6678B928_CustomAttributesCacheGenerator_Manager_get_SceneFadeOutDuration_m2AF5EBBF4DE770502820BE14793AEE44420B7745(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Manager_tCB461C15C123767AFE54F53DB85B845D6678B928_CustomAttributesCacheGenerator_Manager_set_SceneFadeOutDuration_mA6B28932A9C0AA18771DCF4B9387CD64176FDEAC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Manager_tCB461C15C123767AFE54F53DB85B845D6678B928_CustomAttributesCacheGenerator_Manager_get_SceneAnimationDuration_m5933AAE547D9E16299D8ED64A5CE8E07256D0020(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Manager_tCB461C15C123767AFE54F53DB85B845D6678B928_CustomAttributesCacheGenerator_Manager_set_SceneAnimationDuration_mDCECEFFEAAD73AC441388B41B9AAE73BE20E3ABE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Manager_tCB461C15C123767AFE54F53DB85B845D6678B928_CustomAttributesCacheGenerator_Manager_get_Object_m9396DC89955E29CCAA00FA873142C1FED1205D43(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Manager_tCB461C15C123767AFE54F53DB85B845D6678B928_CustomAttributesCacheGenerator_Manager_set_Object_m89E507CF550DDCFE52A6BFDEDB7EB186E7A3817A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ManagerObject_t213FB0BD5082B3E55614C463F0B3D790F315FAF8_CustomAttributesCacheGenerator_m_Canvas(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ManagerObject_t213FB0BD5082B3E55614C463F0B3D790F315FAF8_CustomAttributesCacheGenerator_m_BgCamera(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ManagerObject_t213FB0BD5082B3E55614C463F0B3D790F315FAF8_CustomAttributesCacheGenerator_m_UiCamera(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ManagerObject_t213FB0BD5082B3E55614C463F0B3D790F315FAF8_CustomAttributesCacheGenerator_m_Shield(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ManagerObject_t213FB0BD5082B3E55614C463F0B3D790F315FAF8_CustomAttributesCacheGenerator_m_FadeInEaseType(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ManagerObject_t213FB0BD5082B3E55614C463F0B3D790F315FAF8_CustomAttributesCacheGenerator_m_FadeOutEaseType(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ManagerObject_t213FB0BD5082B3E55614C463F0B3D790F315FAF8_CustomAttributesCacheGenerator_m_ShieldColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ManagerObject_t213FB0BD5082B3E55614C463F0B3D790F315FAF8_CustomAttributesCacheGenerator_ManagerObject_Start_m3CC4A4092C1D4D8FB7DBD6C80156FA6BA028EEB7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__30_t3226DA98802C80F9E4B46193F538AC97C51FC7ED_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__30_t3226DA98802C80F9E4B46193F538AC97C51FC7ED_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__30_t3226DA98802C80F9E4B46193F538AC97C51FC7ED_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__30_t3226DA98802C80F9E4B46193F538AC97C51FC7ED_CustomAttributesCacheGenerator_U3CStartU3Ed__30__ctor_m3278BA8A2B7B1FE63ABBE55EF001B33E930B64C1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__30_t3226DA98802C80F9E4B46193F538AC97C51FC7ED_CustomAttributesCacheGenerator_U3CStartU3Ed__30_System_IDisposable_Dispose_mA073B863AD0267E5ED9DE807DC0884E5F880EF12(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__30_t3226DA98802C80F9E4B46193F538AC97C51FC7ED_CustomAttributesCacheGenerator_U3CStartU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m35A692CC36E0BA65212115187F232830504A9BA7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__30_t3226DA98802C80F9E4B46193F538AC97C51FC7ED_CustomAttributesCacheGenerator_U3CStartU3Ed__30_System_Collections_IEnumerator_Reset_m1DB36B586D00CDF821E2C9D38F12E26E1182C5B2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__30_t3226DA98802C80F9E4B46193F538AC97C51FC7ED_CustomAttributesCacheGenerator_U3CStartU3Ed__30_System_Collections_IEnumerator_get_Current_m38675A3A033151C858C740BBB4D18165212F6C60(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TabController_t60DB225A33AC4C45D570AF49BCA2388CBEEF0C08_CustomAttributesCacheGenerator_m_StartingScreen(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TabController_t60DB225A33AC4C45D570AF49BCA2388CBEEF0C08_CustomAttributesCacheGenerator_m_HorizontalScrollSnap(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TabController_t60DB225A33AC4C45D570AF49BCA2388CBEEF0C08_CustomAttributesCacheGenerator_m_TabBar(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TabSubController_tB3772D8A074842FE21786C6BD93BB706AE0DEAB6_CustomAttributesCacheGenerator_U3CtabU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TabSubController_tB3772D8A074842FE21786C6BD93BB706AE0DEAB6_CustomAttributesCacheGenerator_TabSubController_get_tab_mF46921860DC621EBB80E7F2EDF23FAA59B2FB421(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TabSubController_tB3772D8A074842FE21786C6BD93BB706AE0DEAB6_CustomAttributesCacheGenerator_TabSubController_set_tab_m0077B05DB648639D5AF793DF548126E57B2A9C30(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TabSubController_tB3772D8A074842FE21786C6BD93BB706AE0DEAB6_CustomAttributesCacheGenerator_TabSubController_OnShownFake_mD1BCB8520C389DD75B15A22137C006A9A2D772D9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnShownFakeU3Ed__15_t767E7A793AE8EACD38B7D3C72FB0F6F574A56519_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3COnShownFakeU3Ed__15_t767E7A793AE8EACD38B7D3C72FB0F6F574A56519_0_0_0_var), NULL);
	}
}
static void U3COnShownFakeU3Ed__15_t767E7A793AE8EACD38B7D3C72FB0F6F574A56519_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnShownFakeU3Ed__15_t767E7A793AE8EACD38B7D3C72FB0F6F574A56519_CustomAttributesCacheGenerator_U3COnShownFakeU3Ed__15__ctor_mBDD5A6F79B6B054D1ABB955F21066CBBE307DB0D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnShownFakeU3Ed__15_t767E7A793AE8EACD38B7D3C72FB0F6F574A56519_CustomAttributesCacheGenerator_U3COnShownFakeU3Ed__15_System_IDisposable_Dispose_mB8DD835A213DD3A0597AB08AD08A58C087DACD4A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnShownFakeU3Ed__15_t767E7A793AE8EACD38B7D3C72FB0F6F574A56519_CustomAttributesCacheGenerator_U3COnShownFakeU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m931BA7BBF4F775B400B233466BB956C8A034DDDF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnShownFakeU3Ed__15_t767E7A793AE8EACD38B7D3C72FB0F6F574A56519_CustomAttributesCacheGenerator_U3COnShownFakeU3Ed__15_System_Collections_IEnumerator_Reset_m8A93BAC0233FF83645A9B29522A2E000442A3E87(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnShownFakeU3Ed__15_t767E7A793AE8EACD38B7D3C72FB0F6F574A56519_CustomAttributesCacheGenerator_U3COnShownFakeU3Ed__15_System_Collections_IEnumerator_get_Current_m38136C0C9E43E72FB4150E594AEE59CFE16FDCCB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Banner_tB5E229CE6E4C38728C9BE0D72E377ED4A523FB7F_CustomAttributesCacheGenerator_U3CisSmartBannerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Banner_tB5E229CE6E4C38728C9BE0D72E377ED4A523FB7F_CustomAttributesCacheGenerator_Banner_get_isSmartBanner_mC04C86A7BF69C5E97835D5F2E6AB90631BAC6401(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Banner_tB5E229CE6E4C38728C9BE0D72E377ED4A523FB7F_CustomAttributesCacheGenerator_Banner_set_isSmartBanner_m0F76B11CB3EDEA25DFE884DEAA6AA341642513BF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t94A68B4FB4B95114B20E69A229C1AE27C732B4EC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ETFXFireProjectile_t13466BBD977B94CACB65A92CE5452EF030A99328_CustomAttributesCacheGenerator_projectiles(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ETFXFireProjectile_t13466BBD977B94CACB65A92CE5452EF030A99328_CustomAttributesCacheGenerator_spawnPosition(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x73\x73\x69\x6C\x65\x20\x73\x70\x61\x77\x6E\x73\x20\x61\x74\x20\x61\x74\x74\x61\x63\x68\x65\x64\x20\x67\x61\x6D\x65\x20\x6F\x62\x6A\x65\x63\x74"), NULL);
	}
}
static void ETFXFireProjectile_t13466BBD977B94CACB65A92CE5452EF030A99328_CustomAttributesCacheGenerator_currentProjectile(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ETFXLoopScript_tF4DFDBD5AF30EE9D60CDB5D8E64685454AD7CA75_CustomAttributesCacheGenerator_spawnWithoutLight(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x61\x77\x6E\x20\x77\x69\x74\x68\x6F\x75\x74"), NULL);
	}
}
static void ETFXLoopScript_tF4DFDBD5AF30EE9D60CDB5D8E64685454AD7CA75_CustomAttributesCacheGenerator_ETFXLoopScript_EffectLoop_m728B76DFD989B681D2C93D7A88E9D76C9696CAF4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CEffectLoopU3Ed__6_t25BAA433CE780795116A92A22C987A2CE9108F43_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CEffectLoopU3Ed__6_t25BAA433CE780795116A92A22C987A2CE9108F43_0_0_0_var), NULL);
	}
}
static void U3CEffectLoopU3Ed__6_t25BAA433CE780795116A92A22C987A2CE9108F43_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CEffectLoopU3Ed__6_t25BAA433CE780795116A92A22C987A2CE9108F43_CustomAttributesCacheGenerator_U3CEffectLoopU3Ed__6__ctor_m553F1C586BBB6C68EEB9710F62438A8EDCE136DA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEffectLoopU3Ed__6_t25BAA433CE780795116A92A22C987A2CE9108F43_CustomAttributesCacheGenerator_U3CEffectLoopU3Ed__6_System_IDisposable_Dispose_m8142A69BF60253C0139648E9350176EB9EFD8BFA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEffectLoopU3Ed__6_t25BAA433CE780795116A92A22C987A2CE9108F43_CustomAttributesCacheGenerator_U3CEffectLoopU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m93EE11485A4BA42D80F240C1703D673774889545(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEffectLoopU3Ed__6_t25BAA433CE780795116A92A22C987A2CE9108F43_CustomAttributesCacheGenerator_U3CEffectLoopU3Ed__6_System_Collections_IEnumerator_Reset_mB35745D308CDED6CAB69D52DEFC2D84AB6CC5896(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEffectLoopU3Ed__6_t25BAA433CE780795116A92A22C987A2CE9108F43_CustomAttributesCacheGenerator_U3CEffectLoopU3Ed__6_System_Collections_IEnumerator_get_Current_mB56FD34E205892C6B85E8FD18DAE68F19ECBDE2E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ETFXTarget_tF0B3C5AAE0244F70D1701A4B14AED2D0A8F4B02F_CustomAttributesCacheGenerator_hitParticle(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x66\x66\x65\x63\x74\x20\x73\x68\x6F\x77\x6E\x20\x6F\x6E\x20\x74\x61\x72\x67\x65\x74\x20\x68\x69\x74"), NULL);
	}
}
static void ETFXTarget_tF0B3C5AAE0244F70D1701A4B14AED2D0A8F4B02F_CustomAttributesCacheGenerator_respawnParticle(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x66\x66\x65\x63\x74\x20\x73\x68\x6F\x77\x6E\x20\x6F\x6E\x20\x74\x61\x72\x67\x65\x74\x20\x72\x65\x73\x70\x61\x77\x6E"), NULL);
	}
}
static void ETFXTarget_tF0B3C5AAE0244F70D1701A4B14AED2D0A8F4B02F_CustomAttributesCacheGenerator_ETFXTarget_Respawn_m679D0B0188B3DA281D2BE313E7212FC1D6F2B905(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRespawnU3Ed__7_t2F0FDE3C689F1226C7989D80298592F01E221CFC_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRespawnU3Ed__7_t2F0FDE3C689F1226C7989D80298592F01E221CFC_0_0_0_var), NULL);
	}
}
static void U3CRespawnU3Ed__7_t2F0FDE3C689F1226C7989D80298592F01E221CFC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRespawnU3Ed__7_t2F0FDE3C689F1226C7989D80298592F01E221CFC_CustomAttributesCacheGenerator_U3CRespawnU3Ed__7__ctor_m415513F665DE8F3777A3B21AF9C5B45A90CB1BF3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRespawnU3Ed__7_t2F0FDE3C689F1226C7989D80298592F01E221CFC_CustomAttributesCacheGenerator_U3CRespawnU3Ed__7_System_IDisposable_Dispose_m63C5D414DB8D33EB86AFF6D5569B6C4E18C32615(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRespawnU3Ed__7_t2F0FDE3C689F1226C7989D80298592F01E221CFC_CustomAttributesCacheGenerator_U3CRespawnU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF70DB558C29B7FFE4A790553093711C3478A2C54(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRespawnU3Ed__7_t2F0FDE3C689F1226C7989D80298592F01E221CFC_CustomAttributesCacheGenerator_U3CRespawnU3Ed__7_System_Collections_IEnumerator_Reset_m37DFF9115E528EA7997B687DD51321AB843D9BE7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRespawnU3Ed__7_t2F0FDE3C689F1226C7989D80298592F01E221CFC_CustomAttributesCacheGenerator_U3CRespawnU3Ed__7_System_Collections_IEnumerator_get_Current_mA84FD67FBB5DE784889C67146E66570FC2E5D80E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ETFXLightFade_t2C9509BEFEF0040FB2595BFE837F88894E570BCB_CustomAttributesCacheGenerator_life(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x63\x6F\x6E\x64\x73\x20\x74\x6F\x20\x64\x69\x6D\x20\x74\x68\x65\x20\x6C\x69\x67\x68\x74"), NULL);
	}
}
static void ETFXRotation_tFAD3FD3D4A0D634146B68B89E9BBF223473BA881_CustomAttributesCacheGenerator_rotateVector(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x6F\x74\x61\x74\x65\x20\x61\x78\x69\x73\x65\x73\x20\x62\x79\x20\x64\x65\x67\x72\x65\x65\x73\x20\x70\x65\x72\x20\x73\x65\x63\x6F\x6E\x64"), NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOFade_mEA99A941484D71522BCDD1CCC43B790A6AF4A0B3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOPitch_m6512EC4BEE6C70E110DDFC1048BFCE13FA87E914(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOSetFloat_m8831E89C2E951DDAC9C20A46D3AAD329BAD9C09C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOComplete_m49C5BD0D315241E09EA8D6398F8011C5A34646AE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOKill_m2CED6088E868165A2D91768409FAFFD980993747(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOFlip_m080FDDE3ECDF68901289FBC6EA1297EF278D9D36(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOGoto_mD4A9A702C6187927FDBDD3220570820533459D16(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOPause_mD42B2DFA89EA1635A6F9E813F7B852C852F282E5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOPlay_m95942072A27A597B1FF4FA58A56EC35EC3A8DC15(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOPlayBackwards_mA2814EE20AB3527ADC90DAC391BDF4AD2DA63104(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOPlayForward_m808D504EA5FCA7B43292D4024735C69339BEC168(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DORestart_m53CC4DB3E3B69233410704AC28B0E7E5FE15F9B6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DORewind_m1102048CF3E759CC5A0B7AF48B37092DD8625D5E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOSmoothRewind_m4C703A054EDA680CDA0FE09924AD97775BF70DA1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOTogglePause_mA1AE6D3A3A8FA9692CE0695E915AA08626EF61CF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_t8EDD49424F7AFC055DC1442B3F99B3BFCF6B09F0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass1_0_tA5DAB4AB298719A5FC9203633FE7CAE2D7AFBF0A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_tB2A5900BBCFDCC67A4CCA370F4284F7494E958E1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOMove_mAD664CEB3F56A94F3481FA83FC799F0D7F8FF53F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOMoveX_m4CD527117E19DE2009884655A131DF83B9EAD4A9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOMoveY_m126BCA31B85BF67D1B5E85549A95802B69777A2C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOMoveZ_mDC3444100D64651B456A118C92B6F77C99C32597(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DORotate_m2BDE8501CE323175A7471D694249E660F5BF068C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOLookAt_m706C3BF65EDBCCB2562FB115E61151945941C5FC(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOJump_mCDD95D56D93498D68A345E57B3C359388A816821(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOPath_mF69DF997EF91CD5347391BC89E6CE268A5CD39DD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOLocalPath_mB056B81341D5048E516138EBE4A680B3F4664D6D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOPath_m3C49FFEB71D494F474F7223248D7105E90CF5BE7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOLocalPath_m6F939DAF66D56DA9B52DA62546C70F3862E14193(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_tF06F41913CFED04E5FC3E2C676BCEAC2D3E53CAC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass1_0_t5BD4116FD80F4DAD54FD39BBD7A2CFBDAEAB3AF3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_t35A2F555A5E408DEE2EA63254AACB729C014757D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_0_t39BE4FE1476B4D5D73FC150406C576C638973F8D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_t3A8BE7E3EE8D0963B5FC3AE4F184CAF2EB8E1D84_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass5_0_tC3DF99D3D2F6A0C0E6651294AE0DF0537CEC4617_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass6_0_t97B2374B522E403B5DF1B457D5B6EBF73C0D5CC9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass7_0_tF8E56709396C6DFFECA7608E4CD67CBE2C1C1CE5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass8_0_t7D774AA07804F8A87A6F27483038CEC5FB0CC440_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass9_0_tB9266040A99BBE64FC737A175355345A13E86D21_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass10_0_t6B6DFEAD8FF1DD6A724C294AA0B120F82A7A6937_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DOMove_mC7C9AD67230F89E53BCC36B37BC699E6E3DBFCC9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DOMoveX_m47B42CE055F1892B75820F6FDE992E541394D23E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DOMoveY_mBFBABC3C73C6D858C280B8E0E21869BCFC57938F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DORotate_m8E6DBA35EFB07C71A288D36A86B1299D99D2BE6A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DOJump_m13774D66ED37FC3FF7BA6862C306D32C0B01C351(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DOPath_mC8E35580B3D00EC82A6EB5150838D2D220978092(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DOLocalPath_m56F836B516F2576C6CF48E07518EF403ED233844(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DOPath_m45376A9D96DC70D4150654D08A1A15AD21A158B1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DOLocalPath_mE80C06CF4F281FC00E6E4DEDDDADB145C36091E6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_tED64664CBF4D43290FDDCBF237DF29F9DB8F92A9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass1_0_tDABE9F243A4095C0C2295D15C58B0D3334267A3A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_tB7DA75EB7F11962B20C503C1B2A50BF4D821E318_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_0_tB63C203014F824EA9466F064AF2D8A157A7F4CBF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_tC3FB3913A0B5BA22F06134B01808D6D94A5F618E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass5_0_t440FEF8A7D3454EC9E176430ADEF6185E58594EF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass6_0_t9AB75C89DF5C6CBF983D29D70DA8497E2A734640_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass7_0_t66671DE07A5C515FFE94AE0533494C196D1EC667_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass8_0_tFA3A18575BDE132964DD38569105CF44D9FEE420_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DOTweenModuleSprite_t13F23C4EBAB3A379FF429E99421267128B73C247_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleSprite_t13F23C4EBAB3A379FF429E99421267128B73C247_CustomAttributesCacheGenerator_DOTweenModuleSprite_DOColor_mA67D7B77174B479F8AE20F739A74BA7A2BBC9F0F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleSprite_t13F23C4EBAB3A379FF429E99421267128B73C247_CustomAttributesCacheGenerator_DOTweenModuleSprite_DOFade_mEED04CEE3EE9D32B87478A3CFDB9CFB710307769(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleSprite_t13F23C4EBAB3A379FF429E99421267128B73C247_CustomAttributesCacheGenerator_DOTweenModuleSprite_DOGradientColor_m47670ABD64AEDC972E8210CAF4C8A778B0B7676D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleSprite_t13F23C4EBAB3A379FF429E99421267128B73C247_CustomAttributesCacheGenerator_DOTweenModuleSprite_DOBlendableColor_m51BBDB4A69B0560481736E6734EA2BF52DB1537D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_t865A37B70235C08B0F32F1F40EAB16312D05FD50_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass1_0_t0A5F164BAD8ACAD20D458366B497C9B9C1974AE4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_0_tE81EA512AF1E002F436D3758BC5CE9D93CED44E4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFade_mF927A1EDCA91814D8A466A343E3223EEF4ED7478(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOColor_m7067683474EE6A600294C2CCFD941403FA789A6E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFade_m41638D05EB7678C523D0D39E320FAC80B8E48658(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOColor_mF4B41443760536F3C399612AD0283A52937D95CC(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFade_mE66C8B7D137E0DDE54AAA96F551811FB8BDB8754(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFillAmount_mE4ED52B826A6F8035320E980F7C701B4047C4DBA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOGradientColor_mB4B8D1DEDA92464670EB8A6D985973477D51A4D5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFlexibleSize_mF0916C4E7A906202EFBAEA89CC2B55A9C3CF48B3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOMinSize_m54A6919625499AFD6F801C6EC9672965DBDC96E1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOPreferredSize_mE26AFB2C606F095483EDED449FB8A5A80C08A8DE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOColor_m15BC0FCF035E3DEEC3CFC30465EDE0FE6FD6D2EA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFade_m86DB80308D92FC4DFD3E981F9E958FF2A207EA74(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOScale_mFA27F29E3DFB6B1CFCAC3CE73F06DF3737A3EB06(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPos_m63999D434CE7E8F6BE66BBA92532DE9D98DC9F79(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPosX_m9B57E02E9902C7A3F1A8DD1021AF630C10478380(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPosY_m14CCA63E545D7C28AD1A76488D0084F55EE94CA8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPos3D_m6DEB7C4AE4CD2118BF83D268945340F3373DC7BB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPos3DX_mA40303AC769280BDE74EE58BE5AD80BEA427CE08(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPos3DY_mE6BE56AE421B959F22CBA6318BABC3CD367A7277(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPos3DZ_m702F7D4FE9DC8E6116FB5E0DD478D2BB88ED1242(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorMax_mDD97F6A8B4468622E3808507CB6B89A8A109BDCE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorMin_m60C4775B1EEDBCF2CD3E96A8171C8A83FD7F400D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOPivot_mF623939EDCC152F664FECC8B71E321B0AA13F0A9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOPivotX_mC21C031A61BDB7C3A8087719F23CBC6D1D762D6B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOPivotY_m9922C28ADECA433362D3C6C816318CA00705996A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOSizeDelta_mC4FD07525E1CA5BBF577030D571CE0CCEFD4E6B7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOPunchAnchorPos_m058E3DA140B9C497727F05AEAA296425E585B9EA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOShakeAnchorPos_m89D1C74E940B9434B2234FB49C031AD8FD1170AB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOShakeAnchorPos_m7818867062313A535AB2CD6F86267E2AB9357AC6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOJumpAnchorPos_m7046EE1C82AF0159CFAAA460E4C3D22797625480(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DONormalizedPos_m300F112FE2F67D4DC718083340C7FD27BC96F248(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOHorizontalNormalizedPos_mCB4BCD85B49C458BE871F890A86FEFE0D7F41B42(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOVerticalNormalizedPos_mC0269B4644816A0D5C09717CA92E4229CB31CDF1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOValue_mFDF45D60F52C82AACD8B50859A51B8F315A44F08(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOColor_mFD36CA1EA683A0D2312EE87BCF3B0A924504E797(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOCounter_m6C87AB89E5B14F1FB82630CFF15791678D7DF055(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFade_m21F7CCBB732ABBD63BFD893D1A8D9837532A70D1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOText_mC0932E45473278B2FFB1698954CC0EB0422C626F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOBlendableColor_m4C5E9A68BFBEE876E1FF8D8E9A4BA77856F144C1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOBlendableColor_m18196516A24780A550314E9327868D6D6F3EAA4E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOBlendableColor_mC30D1F805FE976FB25EC361AB0ABA94413361B6F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOShapeCircle_mF3FE405DFB4AB92952A45A6E716682AAC3D57639(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_t7D1FCC95A881539756647760D7E3BCAD117D9C2F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass1_0_t585165D7235045AD6EACEA96E3084E01A742C936_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_tD73E62A2224FE13C8F6B52FCE8BD1C5FD313B99F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_0_t9A0201E5801BCA86E98A5567791D120983EAD387_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_t9CD30C09F13A8901FBD37CEEE628023A3AC88587_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass5_0_tB80886C18986402D46C539623ED89C069D383A01_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass7_0_t3101E53746ED12113974445EFE5F8ED7275D9845_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass8_0_tE55619A7933D4E2E6A3AF6BCBAB6B4C17A865A55_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass9_0_t7A8C490B052492D0F6162243E706C61D6E7EA629_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass10_0_t8BFF1858EDF6EB2283AE20F2D8CC662A1FA5B379_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass11_0_t5C701670B3C5449F73D82915BD4DA267A8B8E8F5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass12_0_t9C18A438FEAA8C5C1A925F5F28D6DF30A8D5B269_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass13_0_t27965A830C5CE83826CC357F50299FE28BE9CABB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass14_0_t532827BA234D3C2B9DA5E065866B8C8F605250F4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass15_0_t1E74F8DF7C76B80F85C66967590B72EF52627D1C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass16_0_t6FE99B74BA758AD9E3C5FA4B81F22E50E2878289_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass17_0_t161B9E0C9F3AF194B3F6E501D4E253148BFDFEB6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass18_0_t28369CB684CE937EF263102FBF87D2E7FC952FD0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass19_0_t7047CE2AF01FC75FD677DF4C1C7E4B32EC2452A5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass20_0_tB3B621D850CE15CA278C20DAAA2C3C0358206A21_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass21_0_t770F928128D9FC7ADA02EA054C711D60F30E16B9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass22_0_t97D2AA4FE9552148F7E37D2DFED676A57DFB1351_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass23_0_t26F763CBFD1D4D4EDFFE9BD383DB924D3B6033E7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass24_0_tF09BD80EEE9ECC7296C10982CEDF0DBFD274B895_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass25_0_tA347965DD1CA3D351E3B581654CC8128EDFF3061_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass26_0_t93DB5B4DF0357D11E4A951B037CFD527BF3B56C8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass27_0_t4D4A1506D5AC9A010D11414FAD7E48C6A7FBA299_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass28_0_tE68276C76E7215C53283D3ADC6A731F1DF29E8C8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass29_0_t5555723CB03030A05216BCB8B27846D583B2D84D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass30_0_t94F73A3FFC30F6C2558392FF5F07AE2E096BF84F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass31_0_t805529EB975D0626EBC97ACAB3880EFC6AB317A3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass32_0_tA414D6A179D68B4F320ED1DB2C432127B75AB0E9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass33_0_t5CB8EE7F07D06E29D59588955B144762AF73E4F6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass34_0_t60E5325D834E607C2110DB690899FC3C7A44D407_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass35_0_t8938338C4A6E58BB25C48A1F318E0458FD3B8CC4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass36_0_tCE8E904A62CBD9F143B3D692CA532EFA7A6F777D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass37_0_tFAFEF1E41FDD4BAE035F7732E85AA44BBFD9DFD1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass38_0_t25800BA4D30D443A7F92C326FA62E4A0351DD2CA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass39_0_t51E4686289D218AD17F28EABBF58A0628BFD6B10_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass40_0_t1DAB80471E2CE447FD9DC4EFC18B5D4E48D8120A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass41_0_tDF0E06C268466ED1E2AD5E601FDD115D64B7E793_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_DOGradientColor_m7E9B0D004D4B3D602EBB852CA1755FE863C6B839(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_DOGradientColor_mA4E191E86F2EF81966259A40B2CDADE375437920(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForCompletion_m4BCEDFA5ED1ACF4BC1B3531C0C306A7AC553C1F2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForRewind_mD9F37D5AAD9B665EE07F083D026E65194A8E3E6E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForKill_m02BD7F3B018BB6B4ED4FCAF47B8DA435EC60BD6B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForElapsedLoops_m46D46788899029A20F73BE252F7427D853131404(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForPosition_m1771625E294B38C5D13DCA40846AEA881C1B23E6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForStart_mB4AAFE83B50AEF3A167F31C14B2F58794527F905(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_DOOffset_m09CA228EBED193314D3D9A41BBDC97393E1FC4D2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_DOTiling_m6A3CD06EC3030A93EC3E93DE678F34048D5C1141(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_AsyncWaitForCompletion_m886CBFB2DCD9455C2762F639F065A7AC9F1EC2E5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAsyncWaitForCompletionU3Ed__10_tAF83373BEFDCE5CDEE1D8D2213CE8CD7384CAC24_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[1];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CAsyncWaitForCompletionU3Ed__10_tAF83373BEFDCE5CDEE1D8D2213CE8CD7384CAC24_0_0_0_var), NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_AsyncWaitForRewind_mCC0B2A591BA46DE9E5CB59EA84461051477D5642(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAsyncWaitForRewindU3Ed__11_t3CC63CE2CBCA49F9CADC6697FDDE8026B073132A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[1];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CAsyncWaitForRewindU3Ed__11_t3CC63CE2CBCA49F9CADC6697FDDE8026B073132A_0_0_0_var), NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_AsyncWaitForKill_m11E8731B8A8E89493FB7976A35DCF4DD85E2132B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAsyncWaitForKillU3Ed__12_t69F4C18D4B977504979616766BA83349830366AB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[1];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CAsyncWaitForKillU3Ed__12_t69F4C18D4B977504979616766BA83349830366AB_0_0_0_var), NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_AsyncWaitForElapsedLoops_m9F3D38817E779AA9C5F0BA7811E6626A7BFBB5B1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAsyncWaitForElapsedLoopsU3Ed__13_t7E11D4778BC9859A55D5F850CD6DE389D4AC372A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CAsyncWaitForElapsedLoopsU3Ed__13_t7E11D4778BC9859A55D5F850CD6DE389D4AC372A_0_0_0_var), NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[1];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_AsyncWaitForPosition_m6207EF50EF7DE8402EDC806F8FEF6CC313C4DB03(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAsyncWaitForPositionU3Ed__14_t8C34DB89D763058E32F5FECE7E62363A58303AE9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[1];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CAsyncWaitForPositionU3Ed__14_t8C34DB89D763058E32F5FECE7E62363A58303AE9_0_0_0_var), NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_AsyncWaitForStart_m637C3E8C05D413E080897E3A99CF21C4BD859F25(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAsyncWaitForStartU3Ed__15_t6B840A7C6ECC2E009C3BC88B5DC9C937FBD80571_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[1];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CAsyncWaitForStartU3Ed__15_t6B840A7C6ECC2E009C3BC88B5DC9C937FBD80571_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass8_0_tE5BFC2D888ECF087A82B3D73CECEAC3525EE4581_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass9_0_tACB95D3310A47E0C0B9E503E6931B7F7BAC03551_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAsyncWaitForCompletionU3Ed__10_tAF83373BEFDCE5CDEE1D8D2213CE8CD7384CAC24_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAsyncWaitForCompletionU3Ed__10_tAF83373BEFDCE5CDEE1D8D2213CE8CD7384CAC24_CustomAttributesCacheGenerator_U3CAsyncWaitForCompletionU3Ed__10_SetStateMachine_m85F5E4DAF7F7E7AB65762F3F6C95BFAE3F039819(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAsyncWaitForRewindU3Ed__11_t3CC63CE2CBCA49F9CADC6697FDDE8026B073132A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAsyncWaitForRewindU3Ed__11_t3CC63CE2CBCA49F9CADC6697FDDE8026B073132A_CustomAttributesCacheGenerator_U3CAsyncWaitForRewindU3Ed__11_SetStateMachine_mAC2B29B1F4653A143591E2BB2388F8AD9F9A0D64(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAsyncWaitForKillU3Ed__12_t69F4C18D4B977504979616766BA83349830366AB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAsyncWaitForKillU3Ed__12_t69F4C18D4B977504979616766BA83349830366AB_CustomAttributesCacheGenerator_U3CAsyncWaitForKillU3Ed__12_SetStateMachine_m47EB31E97C542A8D7AC82CC7EAF613DC8EEDEABD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAsyncWaitForElapsedLoopsU3Ed__13_t7E11D4778BC9859A55D5F850CD6DE389D4AC372A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAsyncWaitForElapsedLoopsU3Ed__13_t7E11D4778BC9859A55D5F850CD6DE389D4AC372A_CustomAttributesCacheGenerator_U3CAsyncWaitForElapsedLoopsU3Ed__13_SetStateMachine_m5E02C400A516339612E14DF2ED0B6046D90F6F47(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAsyncWaitForPositionU3Ed__14_t8C34DB89D763058E32F5FECE7E62363A58303AE9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAsyncWaitForPositionU3Ed__14_t8C34DB89D763058E32F5FECE7E62363A58303AE9_CustomAttributesCacheGenerator_U3CAsyncWaitForPositionU3Ed__14_SetStateMachine_mA77CBC54370E798D88ACF48F9D68586A41FC741E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAsyncWaitForStartU3Ed__15_t6B840A7C6ECC2E009C3BC88B5DC9C937FBD80571_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAsyncWaitForStartU3Ed__15_t6B840A7C6ECC2E009C3BC88B5DC9C937FBD80571_CustomAttributesCacheGenerator_U3CAsyncWaitForStartU3Ed__15_SetStateMachine_mAD7263100F3F3DBD57295879534C47061409AEB0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DOTweenModuleUtils_t1924C98C4EB46411DA5B22524EB14EFDC0203EA0_CustomAttributesCacheGenerator_DOTweenModuleUtils_Init_mFF2188F42FA7128FE1A9BBC2CB9E6351919C9D15(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void DOTweenModuleUtils_t1924C98C4EB46411DA5B22524EB14EFDC0203EA0_CustomAttributesCacheGenerator_DOTweenModuleUtils_Preserver_mBE8D1FE2AE913FBFFCCF43B5A615D941B3706F43(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void Physics_t379352FCCF26C01744720033D3784D7F57DE0D8F_CustomAttributesCacheGenerator_Physics_HasRigidbody_m3082D72A658CCDDF6BB1F3D4A3EFBD6397D6E862(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void Physics_t379352FCCF26C01744720033D3784D7F57DE0D8F_CustomAttributesCacheGenerator_Physics_CreateDOTweenPathTween_m94D41B65E37500E5ACCF6B6F3B5D53FB031C4245(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[365] = 
{
	RectTransformExtensions_t9F14817931A99EDF0C9B376B0A277ACB51281904_CustomAttributesCacheGenerator,
	MgAnimationFade_t8DC6F26A99BAB51886BE866591B651E89629E2A0_CustomAttributesCacheGenerator,
	U3CCoShowU3Ed__4_tE8095F3AF162E6485D1F25668AA2BC0F0DB9FA25_CustomAttributesCacheGenerator,
	U3CCoHideU3Ed__6_tC237FBDC25A5EFD0AAD99C392FDCE64A2768E320_CustomAttributesCacheGenerator,
	U3CCoShowU3Ed__4_t2C33969A872D8A0E3B5B3422480C1459829043A6_CustomAttributesCacheGenerator,
	U3CCoHideU3Ed__6_tE1F78DB53A206B1707F2E85DB6DF244601F6EB5D_CustomAttributesCacheGenerator,
	MgAnimationUnity_t59C8A3C4965C83BBABCD5C2F27ACD224938C81F1_CustomAttributesCacheGenerator,
	U3CCoShowU3Ed__6_t70B62078D15F0FA5A50528A198DE50839239D5C5_CustomAttributesCacheGenerator,
	U3CLoadingToTopU3Ed__6_t1508D394749F69965BFAB2EDE4AC4CCDCDAA3948_CustomAttributesCacheGenerator,
	U3CStartU3Ed__0_tA0D462AF96FB395077D488099A998778AE762B12_CustomAttributesCacheGenerator,
	U3CU3Ec_t6981D3FEAACC85C8C791C504710DBE30733CD5C1_CustomAttributesCacheGenerator,
	SceneAnimation_t903268BBAB234E7C9E6489A124937DB719143682_CustomAttributesCacheGenerator,
	U3CStartU3Ed__30_t3226DA98802C80F9E4B46193F538AC97C51FC7ED_CustomAttributesCacheGenerator,
	U3COnShownFakeU3Ed__15_t767E7A793AE8EACD38B7D3C72FB0F6F574A56519_CustomAttributesCacheGenerator,
	U3CU3Ec_t94A68B4FB4B95114B20E69A229C1AE27C732B4EC_CustomAttributesCacheGenerator,
	U3CEffectLoopU3Ed__6_t25BAA433CE780795116A92A22C987A2CE9108F43_CustomAttributesCacheGenerator,
	U3CRespawnU3Ed__7_t2F0FDE3C689F1226C7989D80298592F01E221CFC_CustomAttributesCacheGenerator,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_t8EDD49424F7AFC055DC1442B3F99B3BFCF6B09F0_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass1_0_tA5DAB4AB298719A5FC9203633FE7CAE2D7AFBF0A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_tB2A5900BBCFDCC67A4CCA370F4284F7494E958E1_CustomAttributesCacheGenerator,
	DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_tF06F41913CFED04E5FC3E2C676BCEAC2D3E53CAC_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass1_0_t5BD4116FD80F4DAD54FD39BBD7A2CFBDAEAB3AF3_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_t35A2F555A5E408DEE2EA63254AACB729C014757D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_0_t39BE4FE1476B4D5D73FC150406C576C638973F8D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_t3A8BE7E3EE8D0963B5FC3AE4F184CAF2EB8E1D84_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass5_0_tC3DF99D3D2F6A0C0E6651294AE0DF0537CEC4617_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass6_0_t97B2374B522E403B5DF1B457D5B6EBF73C0D5CC9_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass7_0_tF8E56709396C6DFFECA7608E4CD67CBE2C1C1CE5_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass8_0_t7D774AA07804F8A87A6F27483038CEC5FB0CC440_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass9_0_tB9266040A99BBE64FC737A175355345A13E86D21_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass10_0_t6B6DFEAD8FF1DD6A724C294AA0B120F82A7A6937_CustomAttributesCacheGenerator,
	DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_tED64664CBF4D43290FDDCBF237DF29F9DB8F92A9_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass1_0_tDABE9F243A4095C0C2295D15C58B0D3334267A3A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_tB7DA75EB7F11962B20C503C1B2A50BF4D821E318_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_0_tB63C203014F824EA9466F064AF2D8A157A7F4CBF_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_tC3FB3913A0B5BA22F06134B01808D6D94A5F618E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass5_0_t440FEF8A7D3454EC9E176430ADEF6185E58594EF_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass6_0_t9AB75C89DF5C6CBF983D29D70DA8497E2A734640_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass7_0_t66671DE07A5C515FFE94AE0533494C196D1EC667_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass8_0_tFA3A18575BDE132964DD38569105CF44D9FEE420_CustomAttributesCacheGenerator,
	DOTweenModuleSprite_t13F23C4EBAB3A379FF429E99421267128B73C247_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_t865A37B70235C08B0F32F1F40EAB16312D05FD50_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass1_0_t0A5F164BAD8ACAD20D458366B497C9B9C1974AE4_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_0_tE81EA512AF1E002F436D3758BC5CE9D93CED44E4_CustomAttributesCacheGenerator,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_t7D1FCC95A881539756647760D7E3BCAD117D9C2F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass1_0_t585165D7235045AD6EACEA96E3084E01A742C936_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_tD73E62A2224FE13C8F6B52FCE8BD1C5FD313B99F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_0_t9A0201E5801BCA86E98A5567791D120983EAD387_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_t9CD30C09F13A8901FBD37CEEE628023A3AC88587_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass5_0_tB80886C18986402D46C539623ED89C069D383A01_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass7_0_t3101E53746ED12113974445EFE5F8ED7275D9845_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass8_0_tE55619A7933D4E2E6A3AF6BCBAB6B4C17A865A55_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass9_0_t7A8C490B052492D0F6162243E706C61D6E7EA629_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass10_0_t8BFF1858EDF6EB2283AE20F2D8CC662A1FA5B379_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass11_0_t5C701670B3C5449F73D82915BD4DA267A8B8E8F5_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass12_0_t9C18A438FEAA8C5C1A925F5F28D6DF30A8D5B269_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass13_0_t27965A830C5CE83826CC357F50299FE28BE9CABB_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass14_0_t532827BA234D3C2B9DA5E065866B8C8F605250F4_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass15_0_t1E74F8DF7C76B80F85C66967590B72EF52627D1C_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass16_0_t6FE99B74BA758AD9E3C5FA4B81F22E50E2878289_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass17_0_t161B9E0C9F3AF194B3F6E501D4E253148BFDFEB6_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass18_0_t28369CB684CE937EF263102FBF87D2E7FC952FD0_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass19_0_t7047CE2AF01FC75FD677DF4C1C7E4B32EC2452A5_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass20_0_tB3B621D850CE15CA278C20DAAA2C3C0358206A21_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass21_0_t770F928128D9FC7ADA02EA054C711D60F30E16B9_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass22_0_t97D2AA4FE9552148F7E37D2DFED676A57DFB1351_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass23_0_t26F763CBFD1D4D4EDFFE9BD383DB924D3B6033E7_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass24_0_tF09BD80EEE9ECC7296C10982CEDF0DBFD274B895_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass25_0_tA347965DD1CA3D351E3B581654CC8128EDFF3061_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass26_0_t93DB5B4DF0357D11E4A951B037CFD527BF3B56C8_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass27_0_t4D4A1506D5AC9A010D11414FAD7E48C6A7FBA299_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass28_0_tE68276C76E7215C53283D3ADC6A731F1DF29E8C8_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass29_0_t5555723CB03030A05216BCB8B27846D583B2D84D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass30_0_t94F73A3FFC30F6C2558392FF5F07AE2E096BF84F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass31_0_t805529EB975D0626EBC97ACAB3880EFC6AB317A3_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass32_0_tA414D6A179D68B4F320ED1DB2C432127B75AB0E9_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass33_0_t5CB8EE7F07D06E29D59588955B144762AF73E4F6_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass34_0_t60E5325D834E607C2110DB690899FC3C7A44D407_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass35_0_t8938338C4A6E58BB25C48A1F318E0458FD3B8CC4_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass36_0_tCE8E904A62CBD9F143B3D692CA532EFA7A6F777D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass37_0_tFAFEF1E41FDD4BAE035F7732E85AA44BBFD9DFD1_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass38_0_t25800BA4D30D443A7F92C326FA62E4A0351DD2CA_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass39_0_t51E4686289D218AD17F28EABBF58A0628BFD6B10_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass40_0_t1DAB80471E2CE447FD9DC4EFC18B5D4E48D8120A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass41_0_tDF0E06C268466ED1E2AD5E601FDD115D64B7E793_CustomAttributesCacheGenerator,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass8_0_tE5BFC2D888ECF087A82B3D73CECEAC3525EE4581_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass9_0_tACB95D3310A47E0C0B9E503E6931B7F7BAC03551_CustomAttributesCacheGenerator,
	U3CAsyncWaitForCompletionU3Ed__10_tAF83373BEFDCE5CDEE1D8D2213CE8CD7384CAC24_CustomAttributesCacheGenerator,
	U3CAsyncWaitForRewindU3Ed__11_t3CC63CE2CBCA49F9CADC6697FDDE8026B073132A_CustomAttributesCacheGenerator,
	U3CAsyncWaitForKillU3Ed__12_t69F4C18D4B977504979616766BA83349830366AB_CustomAttributesCacheGenerator,
	U3CAsyncWaitForElapsedLoopsU3Ed__13_t7E11D4778BC9859A55D5F850CD6DE389D4AC372A_CustomAttributesCacheGenerator,
	U3CAsyncWaitForPositionU3Ed__14_t8C34DB89D763058E32F5FECE7E62363A58303AE9_CustomAttributesCacheGenerator,
	U3CAsyncWaitForStartU3Ed__15_t6B840A7C6ECC2E009C3BC88B5DC9C937FBD80571_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528_CustomAttributesCacheGenerator,
	DemoController_t54E17C5DEE8D3AA65E5108AE99AFC0FFFC300A6F_CustomAttributesCacheGenerator_anim,
	ControllerAnimal_tF06BE9A9E514CC46B7DE91D2C15FA5BB12D89B03_CustomAttributesCacheGenerator_m_Rigidbody,
	ControllerAnimal_tF06BE9A9E514CC46B7DE91D2C15FA5BB12D89B03_CustomAttributesCacheGenerator_m_ConfigurableJoint,
	RandomFlyer_tF96D498F1ABFF9A406B8CAEC7B285E07ED101305_CustomAttributesCacheGenerator_value,
	AudioManager_tD91555488B83E322DEC589BDB624FC46E66CB148_CustomAttributesCacheGenerator_m_BgmPath,
	AudioManager_tD91555488B83E322DEC589BDB624FC46E66CB148_CustomAttributesCacheGenerator_m_SfxPath,
	AudioManager_tD91555488B83E322DEC589BDB624FC46E66CB148_CustomAttributesCacheGenerator_m_VoicePath,
	AudioManager_tD91555488B83E322DEC589BDB624FC46E66CB148_CustomAttributesCacheGenerator_m_ButtonTapSfx,
	AudioManager_tD91555488B83E322DEC589BDB624FC46E66CB148_CustomAttributesCacheGenerator_m_BgmSource,
	AudioManager_tD91555488B83E322DEC589BDB624FC46E66CB148_CustomAttributesCacheGenerator_m_SfxSource,
	AudioManager_tD91555488B83E322DEC589BDB624FC46E66CB148_CustomAttributesCacheGenerator_m_VoiceSource,
	AudioManager_tD91555488B83E322DEC589BDB624FC46E66CB148_CustomAttributesCacheGenerator_U3CinstanceU3Ek__BackingField,
	ETFXProjectileScript_tED63BF9EB347489D6D56FF6E6265BA4EC7457B03_CustomAttributesCacheGenerator_colliderRadius,
	ETFXProjectileScript_tED63BF9EB347489D6D56FF6E6265BA4EC7457B03_CustomAttributesCacheGenerator_collideOffset,
	MgAnimationFade_t8DC6F26A99BAB51886BE866591B651E89629E2A0_CustomAttributesCacheGenerator_animDuration,
	MgAnimationLink_t3FAE4F7171808AABAA99133DAB0552DDB36AA946_CustomAttributesCacheGenerator_m_Link,
	MgAnimationLink_t3FAE4F7171808AABAA99133DAB0552DDB36AA946_CustomAttributesCacheGenerator_delay,
	MgAnimationLink_t3FAE4F7171808AABAA99133DAB0552DDB36AA946_CustomAttributesCacheGenerator_delayHide,
	MgAnimationLinkObject_t0CB17FF21BD5ACAE7ABB251CA9C35698C3E3E111_CustomAttributesCacheGenerator_m_Link,
	MgAnimationLinkObject_t0CB17FF21BD5ACAE7ABB251CA9C35698C3E3E111_CustomAttributesCacheGenerator_delay,
	MgAnimationLinkObject_t0CB17FF21BD5ACAE7ABB251CA9C35698C3E3E111_CustomAttributesCacheGenerator_delayHide,
	MgAnimationMove_tAB0EC396D9DB19ED467027171AEB84B1344F1EDB_CustomAttributesCacheGenerator_m_Start,
	MgAnimationMove_tAB0EC396D9DB19ED467027171AEB84B1344F1EDB_CustomAttributesCacheGenerator_m_End,
	MgAnimationMove_tAB0EC396D9DB19ED467027171AEB84B1344F1EDB_CustomAttributesCacheGenerator_animDuration,
	MgAnimationMove_tAB0EC396D9DB19ED467027171AEB84B1344F1EDB_CustomAttributesCacheGenerator_showEase,
	MgAnimationMove_tAB0EC396D9DB19ED467027171AEB84B1344F1EDB_CustomAttributesCacheGenerator_hideEase,
	MgAnimationMove_tAB0EC396D9DB19ED467027171AEB84B1344F1EDB_CustomAttributesCacheGenerator_moveEase,
	MgAnimationUnity_t59C8A3C4965C83BBABCD5C2F27ACD224938C81F1_CustomAttributesCacheGenerator_customDelay,
	MgAnimationUnity_t59C8A3C4965C83BBABCD5C2F27ACD224938C81F1_CustomAttributesCacheGenerator_showAnim_paramName,
	MgAnimationUnity_t59C8A3C4965C83BBABCD5C2F27ACD224938C81F1_CustomAttributesCacheGenerator_hideAnim_paramName,
	DPopupController_tD2B8D5BB72BBC82747E5B6D76ACE0E4083A41EC7_CustomAttributesCacheGenerator_m_Text,
	SceneAnimation_t903268BBAB234E7C9E6489A124937DB719143682_CustomAttributesCacheGenerator_m_Controller,
	SceneDefaultAnimation_tB8AD18A8003BE1661928EED784456BEFC4FDA9D9_CustomAttributesCacheGenerator_m_AnimationType,
	SceneDefaultAnimation_tB8AD18A8003BE1661928EED784456BEFC4FDA9D9_CustomAttributesCacheGenerator_m_ShowEaseType,
	SceneDefaultAnimation_tB8AD18A8003BE1661928EED784456BEFC4FDA9D9_CustomAttributesCacheGenerator_m_HideEaseType,
	SceneLegacyAnimation_tAEBE1D3E936E86F2345DA287CD0C467D6A7D10C2_CustomAttributesCacheGenerator_m_ShowAnimName,
	SceneLegacyAnimation_tAEBE1D3E936E86F2345DA287CD0C467D6A7D10C2_CustomAttributesCacheGenerator_m_HideAnimName,
	Controller_t88C72B3044FCDB927F2FFEF7A44CE97D95FA1D2F_CustomAttributesCacheGenerator_m_Canvas,
	Controller_t88C72B3044FCDB927F2FFEF7A44CE97D95FA1D2F_CustomAttributesCacheGenerator_m_Camera,
	Controller_t88C72B3044FCDB927F2FFEF7A44CE97D95FA1D2F_CustomAttributesCacheGenerator_FullScreen,
	Controller_t88C72B3044FCDB927F2FFEF7A44CE97D95FA1D2F_CustomAttributesCacheGenerator_Animation,
	Controller_t88C72B3044FCDB927F2FFEF7A44CE97D95FA1D2F_CustomAttributesCacheGenerator_U3CDataU3Ek__BackingField,
	Manager_tCB461C15C123767AFE54F53DB85B845D6678B928_CustomAttributesCacheGenerator_U3CShieldColorU3Ek__BackingField,
	Manager_tCB461C15C123767AFE54F53DB85B845D6678B928_CustomAttributesCacheGenerator_U3CSceneFadeInDurationU3Ek__BackingField,
	Manager_tCB461C15C123767AFE54F53DB85B845D6678B928_CustomAttributesCacheGenerator_U3CSceneFadeOutDurationU3Ek__BackingField,
	Manager_tCB461C15C123767AFE54F53DB85B845D6678B928_CustomAttributesCacheGenerator_U3CSceneAnimationDurationU3Ek__BackingField,
	Manager_tCB461C15C123767AFE54F53DB85B845D6678B928_CustomAttributesCacheGenerator_U3CObjectU3Ek__BackingField,
	ManagerObject_t213FB0BD5082B3E55614C463F0B3D790F315FAF8_CustomAttributesCacheGenerator_m_Canvas,
	ManagerObject_t213FB0BD5082B3E55614C463F0B3D790F315FAF8_CustomAttributesCacheGenerator_m_BgCamera,
	ManagerObject_t213FB0BD5082B3E55614C463F0B3D790F315FAF8_CustomAttributesCacheGenerator_m_UiCamera,
	ManagerObject_t213FB0BD5082B3E55614C463F0B3D790F315FAF8_CustomAttributesCacheGenerator_m_Shield,
	ManagerObject_t213FB0BD5082B3E55614C463F0B3D790F315FAF8_CustomAttributesCacheGenerator_m_FadeInEaseType,
	ManagerObject_t213FB0BD5082B3E55614C463F0B3D790F315FAF8_CustomAttributesCacheGenerator_m_FadeOutEaseType,
	ManagerObject_t213FB0BD5082B3E55614C463F0B3D790F315FAF8_CustomAttributesCacheGenerator_m_ShieldColor,
	TabController_t60DB225A33AC4C45D570AF49BCA2388CBEEF0C08_CustomAttributesCacheGenerator_m_StartingScreen,
	TabController_t60DB225A33AC4C45D570AF49BCA2388CBEEF0C08_CustomAttributesCacheGenerator_m_HorizontalScrollSnap,
	TabController_t60DB225A33AC4C45D570AF49BCA2388CBEEF0C08_CustomAttributesCacheGenerator_m_TabBar,
	TabSubController_tB3772D8A074842FE21786C6BD93BB706AE0DEAB6_CustomAttributesCacheGenerator_U3CtabU3Ek__BackingField,
	Banner_tB5E229CE6E4C38728C9BE0D72E377ED4A523FB7F_CustomAttributesCacheGenerator_U3CisSmartBannerU3Ek__BackingField,
	ETFXFireProjectile_t13466BBD977B94CACB65A92CE5452EF030A99328_CustomAttributesCacheGenerator_projectiles,
	ETFXFireProjectile_t13466BBD977B94CACB65A92CE5452EF030A99328_CustomAttributesCacheGenerator_spawnPosition,
	ETFXFireProjectile_t13466BBD977B94CACB65A92CE5452EF030A99328_CustomAttributesCacheGenerator_currentProjectile,
	ETFXLoopScript_tF4DFDBD5AF30EE9D60CDB5D8E64685454AD7CA75_CustomAttributesCacheGenerator_spawnWithoutLight,
	ETFXTarget_tF0B3C5AAE0244F70D1701A4B14AED2D0A8F4B02F_CustomAttributesCacheGenerator_hitParticle,
	ETFXTarget_tF0B3C5AAE0244F70D1701A4B14AED2D0A8F4B02F_CustomAttributesCacheGenerator_respawnParticle,
	ETFXLightFade_t2C9509BEFEF0040FB2595BFE837F88894E570BCB_CustomAttributesCacheGenerator_life,
	ETFXRotation_tFAD3FD3D4A0D634146B68B89E9BBF223473BA881_CustomAttributesCacheGenerator_rotateVector,
	RectTransformExtensions_t9F14817931A99EDF0C9B376B0A277ACB51281904_CustomAttributesCacheGenerator_RectTransformExtensions_SetLeft_mA86C085FF57265E25F8732197C2FA7E4E3377F05,
	RectTransformExtensions_t9F14817931A99EDF0C9B376B0A277ACB51281904_CustomAttributesCacheGenerator_RectTransformExtensions_SetRight_m41A7631DD85392C568598C634B73D1A7CA9C3BA8,
	RectTransformExtensions_t9F14817931A99EDF0C9B376B0A277ACB51281904_CustomAttributesCacheGenerator_RectTransformExtensions_SetTop_m8E0112409A0B4AE8678D326E04149A7422CC0702,
	RectTransformExtensions_t9F14817931A99EDF0C9B376B0A277ACB51281904_CustomAttributesCacheGenerator_RectTransformExtensions_SetBottom_mD6AC62815DA40BDA0158DDBF0DBC8AFAF8B9C038,
	AudioManager_tD91555488B83E322DEC589BDB624FC46E66CB148_CustomAttributesCacheGenerator_AudioManager_get_instance_m36FB14B490ECEFC3EBF150336F7547156DFB1B8D,
	AudioManager_tD91555488B83E322DEC589BDB624FC46E66CB148_CustomAttributesCacheGenerator_AudioManager_set_instance_m666A0E6C0CF110F46FA9BF050EFFA22214C7D674,
	MgAnimationLink_t3FAE4F7171808AABAA99133DAB0552DDB36AA946_CustomAttributesCacheGenerator_MgAnimationLink_CoShow_mE5861550ECA367E4BACE78F17D99DF226500D8CC,
	MgAnimationLink_t3FAE4F7171808AABAA99133DAB0552DDB36AA946_CustomAttributesCacheGenerator_MgAnimationLink_CoHide_m8A0E7D3AD3E93E482FC47FE0DC472A5A5286538B,
	U3CCoShowU3Ed__4_tE8095F3AF162E6485D1F25668AA2BC0F0DB9FA25_CustomAttributesCacheGenerator_U3CCoShowU3Ed__4__ctor_m554EB8574D3EC74588B8C229FEA1C0B0A291546F,
	U3CCoShowU3Ed__4_tE8095F3AF162E6485D1F25668AA2BC0F0DB9FA25_CustomAttributesCacheGenerator_U3CCoShowU3Ed__4_System_IDisposable_Dispose_m07C82A5E84196EA1B1972E780C6DD1FE46574BC2,
	U3CCoShowU3Ed__4_tE8095F3AF162E6485D1F25668AA2BC0F0DB9FA25_CustomAttributesCacheGenerator_U3CCoShowU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC9030BE2687FCFE6F576F56732CE39EEC045140B,
	U3CCoShowU3Ed__4_tE8095F3AF162E6485D1F25668AA2BC0F0DB9FA25_CustomAttributesCacheGenerator_U3CCoShowU3Ed__4_System_Collections_IEnumerator_Reset_m964546A98807BDFDDFB6F9B948CFEA111004BC87,
	U3CCoShowU3Ed__4_tE8095F3AF162E6485D1F25668AA2BC0F0DB9FA25_CustomAttributesCacheGenerator_U3CCoShowU3Ed__4_System_Collections_IEnumerator_get_Current_m5F2C6965EC88D18494A8D38BDD4716692853BB18,
	U3CCoHideU3Ed__6_tC237FBDC25A5EFD0AAD99C392FDCE64A2768E320_CustomAttributesCacheGenerator_U3CCoHideU3Ed__6__ctor_m4090DB038FFA544ADA13CD42C5846426F206DC1F,
	U3CCoHideU3Ed__6_tC237FBDC25A5EFD0AAD99C392FDCE64A2768E320_CustomAttributesCacheGenerator_U3CCoHideU3Ed__6_System_IDisposable_Dispose_m6FDB5A58039591E3210957C83AE99716A836B3D8,
	U3CCoHideU3Ed__6_tC237FBDC25A5EFD0AAD99C392FDCE64A2768E320_CustomAttributesCacheGenerator_U3CCoHideU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE3180BCF97A6E0B69FEC3031203639BF46203B01,
	U3CCoHideU3Ed__6_tC237FBDC25A5EFD0AAD99C392FDCE64A2768E320_CustomAttributesCacheGenerator_U3CCoHideU3Ed__6_System_Collections_IEnumerator_Reset_m08C249CB2DAB83DFC80C43F0AADBF65BEF26E138,
	U3CCoHideU3Ed__6_tC237FBDC25A5EFD0AAD99C392FDCE64A2768E320_CustomAttributesCacheGenerator_U3CCoHideU3Ed__6_System_Collections_IEnumerator_get_Current_mAC9F050CAA3A19D7A27B8D7BEFC50B0F77039748,
	MgAnimationLinkObject_t0CB17FF21BD5ACAE7ABB251CA9C35698C3E3E111_CustomAttributesCacheGenerator_MgAnimationLinkObject_CoShow_mDC6098417478F81E119F7A1ED3CFF28504F10B8C,
	MgAnimationLinkObject_t0CB17FF21BD5ACAE7ABB251CA9C35698C3E3E111_CustomAttributesCacheGenerator_MgAnimationLinkObject_CoHide_m343BB45CFA8555A43752D7CC98DBD9AA14F2FF2C,
	U3CCoShowU3Ed__4_t2C33969A872D8A0E3B5B3422480C1459829043A6_CustomAttributesCacheGenerator_U3CCoShowU3Ed__4__ctor_m1726143C8B21533706114E0E90EFA33FAC5D33DA,
	U3CCoShowU3Ed__4_t2C33969A872D8A0E3B5B3422480C1459829043A6_CustomAttributesCacheGenerator_U3CCoShowU3Ed__4_System_IDisposable_Dispose_mBD83DECBF938630809CFC6823D1BBF2287B14AF1,
	U3CCoShowU3Ed__4_t2C33969A872D8A0E3B5B3422480C1459829043A6_CustomAttributesCacheGenerator_U3CCoShowU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6AA628E39ECD848EB5417D012CCABAA563FAC3EB,
	U3CCoShowU3Ed__4_t2C33969A872D8A0E3B5B3422480C1459829043A6_CustomAttributesCacheGenerator_U3CCoShowU3Ed__4_System_Collections_IEnumerator_Reset_mA33FD166C110C0853517FB614C738B06DEC9520D,
	U3CCoShowU3Ed__4_t2C33969A872D8A0E3B5B3422480C1459829043A6_CustomAttributesCacheGenerator_U3CCoShowU3Ed__4_System_Collections_IEnumerator_get_Current_mC02634706ABCCA9E4F9C808BE0475E9042DA01AF,
	U3CCoHideU3Ed__6_tE1F78DB53A206B1707F2E85DB6DF244601F6EB5D_CustomAttributesCacheGenerator_U3CCoHideU3Ed__6__ctor_m1B9365AD609078A363564D5938D192A89625AF71,
	U3CCoHideU3Ed__6_tE1F78DB53A206B1707F2E85DB6DF244601F6EB5D_CustomAttributesCacheGenerator_U3CCoHideU3Ed__6_System_IDisposable_Dispose_mBD6FC0B545911FAB141FC358F1C67688DEA32191,
	U3CCoHideU3Ed__6_tE1F78DB53A206B1707F2E85DB6DF244601F6EB5D_CustomAttributesCacheGenerator_U3CCoHideU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m43DF0B545FB7DD89B2572D0705D2F83C4F05CF9D,
	U3CCoHideU3Ed__6_tE1F78DB53A206B1707F2E85DB6DF244601F6EB5D_CustomAttributesCacheGenerator_U3CCoHideU3Ed__6_System_Collections_IEnumerator_Reset_m304762ABA63837D8B6483A489789E6061CBAA982,
	U3CCoHideU3Ed__6_tE1F78DB53A206B1707F2E85DB6DF244601F6EB5D_CustomAttributesCacheGenerator_U3CCoHideU3Ed__6_System_Collections_IEnumerator_get_Current_m7F143AAD687B3EE3B6309FBF1FAB3E547A681355,
	MgAnimationPScaleLoop_tBA3F73A255354D6BABD22AFC7D7FBAFEA057917E_CustomAttributesCacheGenerator_MgAnimationPScaleLoop_U3CLoopAnimU3Eb__5_0_m62EB2B13EFD62CF7C44BFEFA5FE44EF1ADAC7292,
	MgAnimationUnity_t59C8A3C4965C83BBABCD5C2F27ACD224938C81F1_CustomAttributesCacheGenerator_MgAnimationUnity_CoShow_m45D03ACE03ADF8509F5F30851DF9333DE68A0B87,
	U3CCoShowU3Ed__6_t70B62078D15F0FA5A50528A198DE50839239D5C5_CustomAttributesCacheGenerator_U3CCoShowU3Ed__6__ctor_mFFA3E76623E10E9F668CA1CBF411F626D53964A1,
	U3CCoShowU3Ed__6_t70B62078D15F0FA5A50528A198DE50839239D5C5_CustomAttributesCacheGenerator_U3CCoShowU3Ed__6_System_IDisposable_Dispose_m0072088E4B8ED1D71D8EA1F6AEB9FAF65095113B,
	U3CCoShowU3Ed__6_t70B62078D15F0FA5A50528A198DE50839239D5C5_CustomAttributesCacheGenerator_U3CCoShowU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m72E89C7F88FFDBF545B5F398A67CA5515106A0BB,
	U3CCoShowU3Ed__6_t70B62078D15F0FA5A50528A198DE50839239D5C5_CustomAttributesCacheGenerator_U3CCoShowU3Ed__6_System_Collections_IEnumerator_Reset_m19937A01FE7B527C7B7DE39A1360330701041EC5,
	U3CCoShowU3Ed__6_t70B62078D15F0FA5A50528A198DE50839239D5C5_CustomAttributesCacheGenerator_U3CCoShowU3Ed__6_System_Collections_IEnumerator_get_Current_mC04A44C677D01377DA40F515F01E51814858CD9D,
	MgObject_tF1EE47FAAF51C4095CEC6D76BE6A0C59B4A1F99A_CustomAttributesCacheGenerator_MgObject_U3CShakeU3Eb__10_0_mCA287CDA1850D00060EE3FCAF67CAF86CB310910,
	MgObject_tF1EE47FAAF51C4095CEC6D76BE6A0C59B4A1F99A_CustomAttributesCacheGenerator_MgObject_U3CPunchScaleU3Eb__11_0_m523F3F64B37548FC411AF2A549713B8806752E3F,
	DGameController_t0982F5E1D62017B811C5390685DD2A5E7D2B633E_CustomAttributesCacheGenerator_DGameController_LoadingToTop_m16D87897ACBBDCDB17EA184E1CD158D8EDF252FC,
	U3CLoadingToTopU3Ed__6_t1508D394749F69965BFAB2EDE4AC4CCDCDAA3948_CustomAttributesCacheGenerator_U3CLoadingToTopU3Ed__6__ctor_m4DF50C93EAB6A04080FF76FE71AFB1D9E2C85569,
	U3CLoadingToTopU3Ed__6_t1508D394749F69965BFAB2EDE4AC4CCDCDAA3948_CustomAttributesCacheGenerator_U3CLoadingToTopU3Ed__6_System_IDisposable_Dispose_m55B486431095FC8199CE1CC08EDC327E83B15BC0,
	U3CLoadingToTopU3Ed__6_t1508D394749F69965BFAB2EDE4AC4CCDCDAA3948_CustomAttributesCacheGenerator_U3CLoadingToTopU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m359167E9C986E70CB8901CA63CBE7C418B2F3A7D,
	U3CLoadingToTopU3Ed__6_t1508D394749F69965BFAB2EDE4AC4CCDCDAA3948_CustomAttributesCacheGenerator_U3CLoadingToTopU3Ed__6_System_Collections_IEnumerator_Reset_mFBBF4C2124B15D0F6FF9516AB15AB66D50A53879,
	U3CLoadingToTopU3Ed__6_t1508D394749F69965BFAB2EDE4AC4CCDCDAA3948_CustomAttributesCacheGenerator_U3CLoadingToTopU3Ed__6_System_Collections_IEnumerator_get_Current_m0D7A10CDEA3F5BB3B9B90102ED2E62EB48B600EB,
	DMain_tA881A8A301A40DE2168DF9A1F5F716C9A2D4B7AE_CustomAttributesCacheGenerator_DMain_Start_mCB88609F3C58BD213EAC9D1203B1A69610179042,
	U3CStartU3Ed__0_tA0D462AF96FB395077D488099A998778AE762B12_CustomAttributesCacheGenerator_U3CStartU3Ed__0__ctor_mE4C50085C2DAE6B9AE876845A98A256A8CFD3E3B,
	U3CStartU3Ed__0_tA0D462AF96FB395077D488099A998778AE762B12_CustomAttributesCacheGenerator_U3CStartU3Ed__0_System_IDisposable_Dispose_mCB8CDA3D64F78F8C356E9A9F80DA4E48EE53F5CB,
	U3CStartU3Ed__0_tA0D462AF96FB395077D488099A998778AE762B12_CustomAttributesCacheGenerator_U3CStartU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mACE8A452F4692BC6E0FC38AC9DBB35072A007AC1,
	U3CStartU3Ed__0_tA0D462AF96FB395077D488099A998778AE762B12_CustomAttributesCacheGenerator_U3CStartU3Ed__0_System_Collections_IEnumerator_Reset_m0CB66531627AC524221E203F6FFBC2A527CDEC74,
	U3CStartU3Ed__0_tA0D462AF96FB395077D488099A998778AE762B12_CustomAttributesCacheGenerator_U3CStartU3Ed__0_System_Collections_IEnumerator_get_Current_mC2698620ADBE0D6372150B44731A486A9F313AA3,
	Controller_t88C72B3044FCDB927F2FFEF7A44CE97D95FA1D2F_CustomAttributesCacheGenerator_Controller_get_Data_m5FBB080ED88F0FCCE300D8AE0BE74E1B7B66D6B4,
	Controller_t88C72B3044FCDB927F2FFEF7A44CE97D95FA1D2F_CustomAttributesCacheGenerator_Controller_set_Data_m134299AB8FCB15566C6B2AE114B040FF8FEE2E71,
	Manager_tCB461C15C123767AFE54F53DB85B845D6678B928_CustomAttributesCacheGenerator_Manager_get_ShieldColor_mEDF473CD5B3B0E921EDCC9A8ECDED1A59AEAF66C,
	Manager_tCB461C15C123767AFE54F53DB85B845D6678B928_CustomAttributesCacheGenerator_Manager_set_ShieldColor_mFAC7D6CD1EA0F67EE06DB6900180748261E1ABB9,
	Manager_tCB461C15C123767AFE54F53DB85B845D6678B928_CustomAttributesCacheGenerator_Manager_get_SceneFadeInDuration_m98102617D5859290DAA2F5E61A47ED72EF3AD434,
	Manager_tCB461C15C123767AFE54F53DB85B845D6678B928_CustomAttributesCacheGenerator_Manager_set_SceneFadeInDuration_m2DA5251F0F6483A90F57212516A186F19A45FAE6,
	Manager_tCB461C15C123767AFE54F53DB85B845D6678B928_CustomAttributesCacheGenerator_Manager_get_SceneFadeOutDuration_m2AF5EBBF4DE770502820BE14793AEE44420B7745,
	Manager_tCB461C15C123767AFE54F53DB85B845D6678B928_CustomAttributesCacheGenerator_Manager_set_SceneFadeOutDuration_mA6B28932A9C0AA18771DCF4B9387CD64176FDEAC,
	Manager_tCB461C15C123767AFE54F53DB85B845D6678B928_CustomAttributesCacheGenerator_Manager_get_SceneAnimationDuration_m5933AAE547D9E16299D8ED64A5CE8E07256D0020,
	Manager_tCB461C15C123767AFE54F53DB85B845D6678B928_CustomAttributesCacheGenerator_Manager_set_SceneAnimationDuration_mDCECEFFEAAD73AC441388B41B9AAE73BE20E3ABE,
	Manager_tCB461C15C123767AFE54F53DB85B845D6678B928_CustomAttributesCacheGenerator_Manager_get_Object_m9396DC89955E29CCAA00FA873142C1FED1205D43,
	Manager_tCB461C15C123767AFE54F53DB85B845D6678B928_CustomAttributesCacheGenerator_Manager_set_Object_m89E507CF550DDCFE52A6BFDEDB7EB186E7A3817A,
	ManagerObject_t213FB0BD5082B3E55614C463F0B3D790F315FAF8_CustomAttributesCacheGenerator_ManagerObject_Start_m3CC4A4092C1D4D8FB7DBD6C80156FA6BA028EEB7,
	U3CStartU3Ed__30_t3226DA98802C80F9E4B46193F538AC97C51FC7ED_CustomAttributesCacheGenerator_U3CStartU3Ed__30__ctor_m3278BA8A2B7B1FE63ABBE55EF001B33E930B64C1,
	U3CStartU3Ed__30_t3226DA98802C80F9E4B46193F538AC97C51FC7ED_CustomAttributesCacheGenerator_U3CStartU3Ed__30_System_IDisposable_Dispose_mA073B863AD0267E5ED9DE807DC0884E5F880EF12,
	U3CStartU3Ed__30_t3226DA98802C80F9E4B46193F538AC97C51FC7ED_CustomAttributesCacheGenerator_U3CStartU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m35A692CC36E0BA65212115187F232830504A9BA7,
	U3CStartU3Ed__30_t3226DA98802C80F9E4B46193F538AC97C51FC7ED_CustomAttributesCacheGenerator_U3CStartU3Ed__30_System_Collections_IEnumerator_Reset_m1DB36B586D00CDF821E2C9D38F12E26E1182C5B2,
	U3CStartU3Ed__30_t3226DA98802C80F9E4B46193F538AC97C51FC7ED_CustomAttributesCacheGenerator_U3CStartU3Ed__30_System_Collections_IEnumerator_get_Current_m38675A3A033151C858C740BBB4D18165212F6C60,
	TabSubController_tB3772D8A074842FE21786C6BD93BB706AE0DEAB6_CustomAttributesCacheGenerator_TabSubController_get_tab_mF46921860DC621EBB80E7F2EDF23FAA59B2FB421,
	TabSubController_tB3772D8A074842FE21786C6BD93BB706AE0DEAB6_CustomAttributesCacheGenerator_TabSubController_set_tab_m0077B05DB648639D5AF793DF548126E57B2A9C30,
	TabSubController_tB3772D8A074842FE21786C6BD93BB706AE0DEAB6_CustomAttributesCacheGenerator_TabSubController_OnShownFake_mD1BCB8520C389DD75B15A22137C006A9A2D772D9,
	U3COnShownFakeU3Ed__15_t767E7A793AE8EACD38B7D3C72FB0F6F574A56519_CustomAttributesCacheGenerator_U3COnShownFakeU3Ed__15__ctor_mBDD5A6F79B6B054D1ABB955F21066CBBE307DB0D,
	U3COnShownFakeU3Ed__15_t767E7A793AE8EACD38B7D3C72FB0F6F574A56519_CustomAttributesCacheGenerator_U3COnShownFakeU3Ed__15_System_IDisposable_Dispose_mB8DD835A213DD3A0597AB08AD08A58C087DACD4A,
	U3COnShownFakeU3Ed__15_t767E7A793AE8EACD38B7D3C72FB0F6F574A56519_CustomAttributesCacheGenerator_U3COnShownFakeU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m931BA7BBF4F775B400B233466BB956C8A034DDDF,
	U3COnShownFakeU3Ed__15_t767E7A793AE8EACD38B7D3C72FB0F6F574A56519_CustomAttributesCacheGenerator_U3COnShownFakeU3Ed__15_System_Collections_IEnumerator_Reset_m8A93BAC0233FF83645A9B29522A2E000442A3E87,
	U3COnShownFakeU3Ed__15_t767E7A793AE8EACD38B7D3C72FB0F6F574A56519_CustomAttributesCacheGenerator_U3COnShownFakeU3Ed__15_System_Collections_IEnumerator_get_Current_m38136C0C9E43E72FB4150E594AEE59CFE16FDCCB,
	Banner_tB5E229CE6E4C38728C9BE0D72E377ED4A523FB7F_CustomAttributesCacheGenerator_Banner_get_isSmartBanner_mC04C86A7BF69C5E97835D5F2E6AB90631BAC6401,
	Banner_tB5E229CE6E4C38728C9BE0D72E377ED4A523FB7F_CustomAttributesCacheGenerator_Banner_set_isSmartBanner_m0F76B11CB3EDEA25DFE884DEAA6AA341642513BF,
	ETFXLoopScript_tF4DFDBD5AF30EE9D60CDB5D8E64685454AD7CA75_CustomAttributesCacheGenerator_ETFXLoopScript_EffectLoop_m728B76DFD989B681D2C93D7A88E9D76C9696CAF4,
	U3CEffectLoopU3Ed__6_t25BAA433CE780795116A92A22C987A2CE9108F43_CustomAttributesCacheGenerator_U3CEffectLoopU3Ed__6__ctor_m553F1C586BBB6C68EEB9710F62438A8EDCE136DA,
	U3CEffectLoopU3Ed__6_t25BAA433CE780795116A92A22C987A2CE9108F43_CustomAttributesCacheGenerator_U3CEffectLoopU3Ed__6_System_IDisposable_Dispose_m8142A69BF60253C0139648E9350176EB9EFD8BFA,
	U3CEffectLoopU3Ed__6_t25BAA433CE780795116A92A22C987A2CE9108F43_CustomAttributesCacheGenerator_U3CEffectLoopU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m93EE11485A4BA42D80F240C1703D673774889545,
	U3CEffectLoopU3Ed__6_t25BAA433CE780795116A92A22C987A2CE9108F43_CustomAttributesCacheGenerator_U3CEffectLoopU3Ed__6_System_Collections_IEnumerator_Reset_mB35745D308CDED6CAB69D52DEFC2D84AB6CC5896,
	U3CEffectLoopU3Ed__6_t25BAA433CE780795116A92A22C987A2CE9108F43_CustomAttributesCacheGenerator_U3CEffectLoopU3Ed__6_System_Collections_IEnumerator_get_Current_mB56FD34E205892C6B85E8FD18DAE68F19ECBDE2E,
	ETFXTarget_tF0B3C5AAE0244F70D1701A4B14AED2D0A8F4B02F_CustomAttributesCacheGenerator_ETFXTarget_Respawn_m679D0B0188B3DA281D2BE313E7212FC1D6F2B905,
	U3CRespawnU3Ed__7_t2F0FDE3C689F1226C7989D80298592F01E221CFC_CustomAttributesCacheGenerator_U3CRespawnU3Ed__7__ctor_m415513F665DE8F3777A3B21AF9C5B45A90CB1BF3,
	U3CRespawnU3Ed__7_t2F0FDE3C689F1226C7989D80298592F01E221CFC_CustomAttributesCacheGenerator_U3CRespawnU3Ed__7_System_IDisposable_Dispose_m63C5D414DB8D33EB86AFF6D5569B6C4E18C32615,
	U3CRespawnU3Ed__7_t2F0FDE3C689F1226C7989D80298592F01E221CFC_CustomAttributesCacheGenerator_U3CRespawnU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF70DB558C29B7FFE4A790553093711C3478A2C54,
	U3CRespawnU3Ed__7_t2F0FDE3C689F1226C7989D80298592F01E221CFC_CustomAttributesCacheGenerator_U3CRespawnU3Ed__7_System_Collections_IEnumerator_Reset_m37DFF9115E528EA7997B687DD51321AB843D9BE7,
	U3CRespawnU3Ed__7_t2F0FDE3C689F1226C7989D80298592F01E221CFC_CustomAttributesCacheGenerator_U3CRespawnU3Ed__7_System_Collections_IEnumerator_get_Current_mA84FD67FBB5DE784889C67146E66570FC2E5D80E,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOFade_mEA99A941484D71522BCDD1CCC43B790A6AF4A0B3,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOPitch_m6512EC4BEE6C70E110DDFC1048BFCE13FA87E914,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOSetFloat_m8831E89C2E951DDAC9C20A46D3AAD329BAD9C09C,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOComplete_m49C5BD0D315241E09EA8D6398F8011C5A34646AE,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOKill_m2CED6088E868165A2D91768409FAFFD980993747,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOFlip_m080FDDE3ECDF68901289FBC6EA1297EF278D9D36,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOGoto_mD4A9A702C6187927FDBDD3220570820533459D16,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOPause_mD42B2DFA89EA1635A6F9E813F7B852C852F282E5,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOPlay_m95942072A27A597B1FF4FA58A56EC35EC3A8DC15,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOPlayBackwards_mA2814EE20AB3527ADC90DAC391BDF4AD2DA63104,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOPlayForward_m808D504EA5FCA7B43292D4024735C69339BEC168,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DORestart_m53CC4DB3E3B69233410704AC28B0E7E5FE15F9B6,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DORewind_m1102048CF3E759CC5A0B7AF48B37092DD8625D5E,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOSmoothRewind_m4C703A054EDA680CDA0FE09924AD97775BF70DA1,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOTogglePause_mA1AE6D3A3A8FA9692CE0695E915AA08626EF61CF,
	DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOMove_mAD664CEB3F56A94F3481FA83FC799F0D7F8FF53F,
	DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOMoveX_m4CD527117E19DE2009884655A131DF83B9EAD4A9,
	DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOMoveY_m126BCA31B85BF67D1B5E85549A95802B69777A2C,
	DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOMoveZ_mDC3444100D64651B456A118C92B6F77C99C32597,
	DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DORotate_m2BDE8501CE323175A7471D694249E660F5BF068C,
	DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOLookAt_m706C3BF65EDBCCB2562FB115E61151945941C5FC,
	DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOJump_mCDD95D56D93498D68A345E57B3C359388A816821,
	DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOPath_mF69DF997EF91CD5347391BC89E6CE268A5CD39DD,
	DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOLocalPath_mB056B81341D5048E516138EBE4A680B3F4664D6D,
	DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOPath_m3C49FFEB71D494F474F7223248D7105E90CF5BE7,
	DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOLocalPath_m6F939DAF66D56DA9B52DA62546C70F3862E14193,
	DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DOMove_mC7C9AD67230F89E53BCC36B37BC699E6E3DBFCC9,
	DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DOMoveX_m47B42CE055F1892B75820F6FDE992E541394D23E,
	DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DOMoveY_mBFBABC3C73C6D858C280B8E0E21869BCFC57938F,
	DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DORotate_m8E6DBA35EFB07C71A288D36A86B1299D99D2BE6A,
	DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DOJump_m13774D66ED37FC3FF7BA6862C306D32C0B01C351,
	DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DOPath_mC8E35580B3D00EC82A6EB5150838D2D220978092,
	DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DOLocalPath_m56F836B516F2576C6CF48E07518EF403ED233844,
	DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DOPath_m45376A9D96DC70D4150654D08A1A15AD21A158B1,
	DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DOLocalPath_mE80C06CF4F281FC00E6E4DEDDDADB145C36091E6,
	DOTweenModuleSprite_t13F23C4EBAB3A379FF429E99421267128B73C247_CustomAttributesCacheGenerator_DOTweenModuleSprite_DOColor_mA67D7B77174B479F8AE20F739A74BA7A2BBC9F0F,
	DOTweenModuleSprite_t13F23C4EBAB3A379FF429E99421267128B73C247_CustomAttributesCacheGenerator_DOTweenModuleSprite_DOFade_mEED04CEE3EE9D32B87478A3CFDB9CFB710307769,
	DOTweenModuleSprite_t13F23C4EBAB3A379FF429E99421267128B73C247_CustomAttributesCacheGenerator_DOTweenModuleSprite_DOGradientColor_m47670ABD64AEDC972E8210CAF4C8A778B0B7676D,
	DOTweenModuleSprite_t13F23C4EBAB3A379FF429E99421267128B73C247_CustomAttributesCacheGenerator_DOTweenModuleSprite_DOBlendableColor_m51BBDB4A69B0560481736E6734EA2BF52DB1537D,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFade_mF927A1EDCA91814D8A466A343E3223EEF4ED7478,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOColor_m7067683474EE6A600294C2CCFD941403FA789A6E,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFade_m41638D05EB7678C523D0D39E320FAC80B8E48658,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOColor_mF4B41443760536F3C399612AD0283A52937D95CC,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFade_mE66C8B7D137E0DDE54AAA96F551811FB8BDB8754,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFillAmount_mE4ED52B826A6F8035320E980F7C701B4047C4DBA,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOGradientColor_mB4B8D1DEDA92464670EB8A6D985973477D51A4D5,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFlexibleSize_mF0916C4E7A906202EFBAEA89CC2B55A9C3CF48B3,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOMinSize_m54A6919625499AFD6F801C6EC9672965DBDC96E1,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOPreferredSize_mE26AFB2C606F095483EDED449FB8A5A80C08A8DE,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOColor_m15BC0FCF035E3DEEC3CFC30465EDE0FE6FD6D2EA,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFade_m86DB80308D92FC4DFD3E981F9E958FF2A207EA74,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOScale_mFA27F29E3DFB6B1CFCAC3CE73F06DF3737A3EB06,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPos_m63999D434CE7E8F6BE66BBA92532DE9D98DC9F79,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPosX_m9B57E02E9902C7A3F1A8DD1021AF630C10478380,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPosY_m14CCA63E545D7C28AD1A76488D0084F55EE94CA8,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPos3D_m6DEB7C4AE4CD2118BF83D268945340F3373DC7BB,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPos3DX_mA40303AC769280BDE74EE58BE5AD80BEA427CE08,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPos3DY_mE6BE56AE421B959F22CBA6318BABC3CD367A7277,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPos3DZ_m702F7D4FE9DC8E6116FB5E0DD478D2BB88ED1242,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorMax_mDD97F6A8B4468622E3808507CB6B89A8A109BDCE,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorMin_m60C4775B1EEDBCF2CD3E96A8171C8A83FD7F400D,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOPivot_mF623939EDCC152F664FECC8B71E321B0AA13F0A9,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOPivotX_mC21C031A61BDB7C3A8087719F23CBC6D1D762D6B,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOPivotY_m9922C28ADECA433362D3C6C816318CA00705996A,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOSizeDelta_mC4FD07525E1CA5BBF577030D571CE0CCEFD4E6B7,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOPunchAnchorPos_m058E3DA140B9C497727F05AEAA296425E585B9EA,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOShakeAnchorPos_m89D1C74E940B9434B2234FB49C031AD8FD1170AB,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOShakeAnchorPos_m7818867062313A535AB2CD6F86267E2AB9357AC6,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOJumpAnchorPos_m7046EE1C82AF0159CFAAA460E4C3D22797625480,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DONormalizedPos_m300F112FE2F67D4DC718083340C7FD27BC96F248,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOHorizontalNormalizedPos_mCB4BCD85B49C458BE871F890A86FEFE0D7F41B42,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOVerticalNormalizedPos_mC0269B4644816A0D5C09717CA92E4229CB31CDF1,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOValue_mFDF45D60F52C82AACD8B50859A51B8F315A44F08,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOColor_mFD36CA1EA683A0D2312EE87BCF3B0A924504E797,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOCounter_m6C87AB89E5B14F1FB82630CFF15791678D7DF055,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFade_m21F7CCBB732ABBD63BFD893D1A8D9837532A70D1,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOText_mC0932E45473278B2FFB1698954CC0EB0422C626F,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOBlendableColor_m4C5E9A68BFBEE876E1FF8D8E9A4BA77856F144C1,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOBlendableColor_m18196516A24780A550314E9327868D6D6F3EAA4E,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOBlendableColor_mC30D1F805FE976FB25EC361AB0ABA94413361B6F,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOShapeCircle_mF3FE405DFB4AB92952A45A6E716682AAC3D57639,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_DOGradientColor_m7E9B0D004D4B3D602EBB852CA1755FE863C6B839,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_DOGradientColor_mA4E191E86F2EF81966259A40B2CDADE375437920,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForCompletion_m4BCEDFA5ED1ACF4BC1B3531C0C306A7AC553C1F2,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForRewind_mD9F37D5AAD9B665EE07F083D026E65194A8E3E6E,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForKill_m02BD7F3B018BB6B4ED4FCAF47B8DA435EC60BD6B,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForElapsedLoops_m46D46788899029A20F73BE252F7427D853131404,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForPosition_m1771625E294B38C5D13DCA40846AEA881C1B23E6,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForStart_mB4AAFE83B50AEF3A167F31C14B2F58794527F905,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_DOOffset_m09CA228EBED193314D3D9A41BBDC97393E1FC4D2,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_DOTiling_m6A3CD06EC3030A93EC3E93DE678F34048D5C1141,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_AsyncWaitForCompletion_m886CBFB2DCD9455C2762F639F065A7AC9F1EC2E5,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_AsyncWaitForRewind_mCC0B2A591BA46DE9E5CB59EA84461051477D5642,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_AsyncWaitForKill_m11E8731B8A8E89493FB7976A35DCF4DD85E2132B,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_AsyncWaitForElapsedLoops_m9F3D38817E779AA9C5F0BA7811E6626A7BFBB5B1,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_AsyncWaitForPosition_m6207EF50EF7DE8402EDC806F8FEF6CC313C4DB03,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_AsyncWaitForStart_m637C3E8C05D413E080897E3A99CF21C4BD859F25,
	U3CAsyncWaitForCompletionU3Ed__10_tAF83373BEFDCE5CDEE1D8D2213CE8CD7384CAC24_CustomAttributesCacheGenerator_U3CAsyncWaitForCompletionU3Ed__10_SetStateMachine_m85F5E4DAF7F7E7AB65762F3F6C95BFAE3F039819,
	U3CAsyncWaitForRewindU3Ed__11_t3CC63CE2CBCA49F9CADC6697FDDE8026B073132A_CustomAttributesCacheGenerator_U3CAsyncWaitForRewindU3Ed__11_SetStateMachine_mAC2B29B1F4653A143591E2BB2388F8AD9F9A0D64,
	U3CAsyncWaitForKillU3Ed__12_t69F4C18D4B977504979616766BA83349830366AB_CustomAttributesCacheGenerator_U3CAsyncWaitForKillU3Ed__12_SetStateMachine_m47EB31E97C542A8D7AC82CC7EAF613DC8EEDEABD,
	U3CAsyncWaitForElapsedLoopsU3Ed__13_t7E11D4778BC9859A55D5F850CD6DE389D4AC372A_CustomAttributesCacheGenerator_U3CAsyncWaitForElapsedLoopsU3Ed__13_SetStateMachine_m5E02C400A516339612E14DF2ED0B6046D90F6F47,
	U3CAsyncWaitForPositionU3Ed__14_t8C34DB89D763058E32F5FECE7E62363A58303AE9_CustomAttributesCacheGenerator_U3CAsyncWaitForPositionU3Ed__14_SetStateMachine_mA77CBC54370E798D88ACF48F9D68586A41FC741E,
	U3CAsyncWaitForStartU3Ed__15_t6B840A7C6ECC2E009C3BC88B5DC9C937FBD80571_CustomAttributesCacheGenerator_U3CAsyncWaitForStartU3Ed__15_SetStateMachine_mAD7263100F3F3DBD57295879534C47061409AEB0,
	DOTweenModuleUtils_t1924C98C4EB46411DA5B22524EB14EFDC0203EA0_CustomAttributesCacheGenerator_DOTweenModuleUtils_Init_mFF2188F42FA7128FE1A9BBC2CB9E6351919C9D15,
	DOTweenModuleUtils_t1924C98C4EB46411DA5B22524EB14EFDC0203EA0_CustomAttributesCacheGenerator_DOTweenModuleUtils_Preserver_mBE8D1FE2AE913FBFFCCF43B5A615D941B3706F43,
	Physics_t379352FCCF26C01744720033D3784D7F57DE0D8F_CustomAttributesCacheGenerator_Physics_HasRigidbody_m3082D72A658CCDDF6BB1F3D4A3EFBD6397D6E862,
	Physics_t379352FCCF26C01744720033D3784D7F57DE0D8F_CustomAttributesCacheGenerator_Physics_CreateDOTweenPathTween_m94D41B65E37500E5ACCF6B6F3B5D53FB031C4245,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
