﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// UnityEngine.ContextMenu
struct ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210;
// UnityEngine.HelpURLAttribute
struct HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AddComponentMenu_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// UnityEngine.ContextMenu
struct ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.ContextMenu::menuItem
	String_t* ___menuItem_0;
	// System.Boolean UnityEngine.ContextMenu::validate
	bool ___validate_1;
	// System.Int32 UnityEngine.ContextMenu::priority
	int32_t ___priority_2;

public:
	inline static int32_t get_offset_of_menuItem_0() { return static_cast<int32_t>(offsetof(ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861, ___menuItem_0)); }
	inline String_t* get_menuItem_0() const { return ___menuItem_0; }
	inline String_t** get_address_of_menuItem_0() { return &___menuItem_0; }
	inline void set_menuItem_0(String_t* value)
	{
		___menuItem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___menuItem_0), (void*)value);
	}

	inline static int32_t get_offset_of_validate_1() { return static_cast<int32_t>(offsetof(ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861, ___validate_1)); }
	inline bool get_validate_1() const { return ___validate_1; }
	inline bool* get_address_of_validate_1() { return &___validate_1; }
	inline void set_validate_1(bool value)
	{
		___validate_1 = value;
	}

	inline static int32_t get_offset_of_priority_2() { return static_cast<int32_t>(offsetof(ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861, ___priority_2)); }
	inline int32_t get_priority_2() const { return ___priority_2; }
	inline int32_t* get_address_of_priority_2() { return &___priority_2; }
	inline void set_priority_2(int32_t value)
	{
		___priority_2 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.Serialization.FormerlySerializedAsAttribute::m_oldName
	String_t* ___m_oldName_0;

public:
	inline static int32_t get_offset_of_m_oldName_0() { return static_cast<int32_t>(offsetof(FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210, ___m_oldName_0)); }
	inline String_t* get_m_oldName_0() const { return ___m_oldName_0; }
	inline String_t** get_address_of_m_oldName_0() { return &___m_oldName_0; }
	inline void set_m_oldName_0(String_t* value)
	{
		___m_oldName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_oldName_0), (void*)value);
	}
};


// UnityEngine.HelpURLAttribute
struct HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.HelpURLAttribute::m_Url
	String_t* ___m_Url_0;
	// System.Boolean UnityEngine.HelpURLAttribute::m_Dispatcher
	bool ___m_Dispatcher_1;
	// System.String UnityEngine.HelpURLAttribute::m_DispatchingFieldName
	String_t* ___m_DispatchingFieldName_2;

public:
	inline static int32_t get_offset_of_m_Url_0() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_Url_0)); }
	inline String_t* get_m_Url_0() const { return ___m_Url_0; }
	inline String_t** get_address_of_m_Url_0() { return &___m_Url_0; }
	inline void set_m_Url_0(String_t* value)
	{
		___m_Url_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Url_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Dispatcher_1() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_Dispatcher_1)); }
	inline bool get_m_Dispatcher_1() const { return ___m_Dispatcher_1; }
	inline bool* get_address_of_m_Dispatcher_1() { return &___m_Dispatcher_1; }
	inline void set_m_Dispatcher_1(bool value)
	{
		___m_Dispatcher_1 = value;
	}

	inline static int32_t get_offset_of_m_DispatchingFieldName_2() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_DispatchingFieldName_2)); }
	inline String_t* get_m_DispatchingFieldName_2() const { return ___m_DispatchingFieldName_2; }
	inline String_t** get_address_of_m_DispatchingFieldName_2() { return &___m_DispatchingFieldName_2; }
	inline void set_m_DispatchingFieldName_2(String_t* value)
	{
		___m_DispatchingFieldName_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DispatchingFieldName_2), (void*)value);
	}
};


// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void UnityEngine.HelpURLAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215 (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * __this, String_t* ___url0, const RuntimeMethod* method);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549 (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * __this, String_t* ___menuName0, const RuntimeMethod* method);
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * __this, String_t* ___oldName0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ContextMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * __this, String_t* ___itemName0, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
static void LeanTouchPlus_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void LeanDragColorMesh_tC3EEA8593FCA1935C16D6C86951A0819E2695BCE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x44\x72\x61\x67\x43\x6F\x6C\x6F\x72\x4D\x65\x73\x68"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x44\x72\x61\x67\x20\x43\x6F\x6C\x6F\x72\x20\x4D\x65\x73\x68"), NULL);
	}
}
static void LeanDragColorMesh_tC3EEA8593FCA1935C16D6C86951A0819E2695BCE_CustomAttributesCacheGenerator_paintColor(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x50\x61\x69\x6E\x74\x43\x6F\x6C\x6F\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDragColorMesh_tC3EEA8593FCA1935C16D6C86951A0819E2695BCE_CustomAttributesCacheGenerator__camera(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6D\x65\x72\x61"), NULL);
	}
}
static void LeanDragDeformMesh_t67021E1B1C14EE2F7E9C6ED3BD2E1D636E4BB284_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x44\x72\x61\x67\x20\x44\x65\x66\x6F\x72\x6D\x20\x4D\x65\x73\x68"), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[2];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x44\x72\x61\x67\x44\x65\x66\x6F\x72\x6D\x4D\x65\x73\x68"), NULL);
	}
}
static void LeanDragDeformMesh_t67021E1B1C14EE2F7E9C6ED3BD2E1D636E4BB284_CustomAttributesCacheGenerator_scaledRadius(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x61\x6C\x65\x64\x52\x61\x64\x69\x75\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDragDeformMesh_t67021E1B1C14EE2F7E9C6ED3BD2E1D636E4BB284_CustomAttributesCacheGenerator_applyToMeshCollider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x41\x70\x70\x6C\x79\x54\x6F\x4D\x65\x73\x68\x43\x6F\x6C\x6C\x69\x64\x65\x72"), NULL);
	}
}
static void LeanDragDeformMesh_t67021E1B1C14EE2F7E9C6ED3BD2E1D636E4BB284_CustomAttributesCacheGenerator__camera(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6D\x65\x72\x61"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDragLine_t19C2B178D12E85AC6B607B08F32FD3003C3CEFED_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x44\x72\x61\x67\x20\x4C\x69\x6E\x65"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x44\x72\x61\x67\x4C\x69\x6E\x65"), NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[2];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void LeanDragLine_t19C2B178D12E85AC6B607B08F32FD3003C3CEFED_CustomAttributesCacheGenerator_widthScale(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x57\x69\x64\x74\x68\x53\x63\x61\x6C\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDragLine_t19C2B178D12E85AC6B607B08F32FD3003C3CEFED_CustomAttributesCacheGenerator_lengthMin(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x6E\x67\x74\x68\x4D\x69\x6E"), NULL);
	}
}
static void LeanDragLine_t19C2B178D12E85AC6B607B08F32FD3003C3CEFED_CustomAttributesCacheGenerator_lengthMax(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x6E\x67\x74\x68\x4D\x61\x78"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDragLine_t19C2B178D12E85AC6B607B08F32FD3003C3CEFED_CustomAttributesCacheGenerator_startAtOrigin(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x72\x74\x41\x74\x4F\x72\x69\x67\x69\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDragLine_t19C2B178D12E85AC6B607B08F32FD3003C3CEFED_CustomAttributesCacheGenerator_invert(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x65\x72\x74"), NULL);
	}
}
static void LeanDragLine_t19C2B178D12E85AC6B607B08F32FD3003C3CEFED_CustomAttributesCacheGenerator_onReleasedFrom(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDragLine_t19C2B178D12E85AC6B607B08F32FD3003C3CEFED_CustomAttributesCacheGenerator_onReleasedTo(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDragLine_t19C2B178D12E85AC6B607B08F32FD3003C3CEFED_CustomAttributesCacheGenerator_onReleasedDelta(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDragLine_t19C2B178D12E85AC6B607B08F32FD3003C3CEFED_CustomAttributesCacheGenerator_onReleasedFromTo(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDragSelect_t081AF0F20B2579E49E757E42A8C30B8A3964E99E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x44\x72\x61\x67\x20\x53\x65\x6C\x65\x63\x74"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x44\x72\x61\x67\x53\x65\x6C\x65\x63\x74"), NULL);
	}
}
static void LeanDragSelect_t081AF0F20B2579E49E757E42A8C30B8A3964E99E_CustomAttributesCacheGenerator_select(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x6C\x65\x63\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDragSelect_t081AF0F20B2579E49E757E42A8C30B8A3964E99E_CustomAttributesCacheGenerator_requireNoSelectables(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x71\x75\x69\x72\x65\x4E\x6F\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65\x73"), NULL);
	}
}
static void LeanDragSelect_t081AF0F20B2579E49E757E42A8C30B8A3964E99E_CustomAttributesCacheGenerator_requireInitialSelection(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x71\x75\x69\x72\x65\x49\x6E\x69\x74\x69\x61\x6C\x53\x65\x6C\x65\x63\x74\x69\x6F\x6E"), NULL);
	}
}
static void LeanDragSelect_t081AF0F20B2579E49E757E42A8C30B8A3964E99E_CustomAttributesCacheGenerator_deselectAllAtStart(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x73\x65\x6C\x65\x63\x74\x41\x6C\x6C\x41\x74\x53\x74\x61\x72\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDragSelect_t081AF0F20B2579E49E757E42A8C30B8A3964E99E_CustomAttributesCacheGenerator_maximumSeparation(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x69\x6D\x75\x6D\x53\x65\x70\x61\x72\x61\x74\x69\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDragTranslateAlong_tACA31121D5367B039300B0EE76656C3CF9C4D30D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x44\x72\x61\x67\x20\x54\x72\x61\x6E\x73\x6C\x61\x74\x65\x20\x41\x6C\x6F\x6E\x67"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x44\x72\x61\x67\x54\x72\x61\x6E\x73\x6C\x61\x74\x65\x41\x6C\x6F\x6E\x67"), NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[2];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void LeanDragTranslateAlong_tACA31121D5367B039300B0EE76656C3CF9C4D30D_CustomAttributesCacheGenerator_target(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x72\x67\x65\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDragTranslateAlong_tACA31121D5367B039300B0EE76656C3CF9C4D30D_CustomAttributesCacheGenerator_trackScreenPosition(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x72\x61\x63\x6B\x53\x63\x72\x65\x65\x6E\x50\x6F\x73\x69\x74\x69\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDragTranslateAlong_tACA31121D5367B039300B0EE76656C3CF9C4D30D_CustomAttributesCacheGenerator_damping(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x65\x6E\x69\x6E\x67"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x69\x6E\x67"), NULL);
	}
}
static void LeanDragTranslateAlong_tACA31121D5367B039300B0EE76656C3CF9C4D30D_CustomAttributesCacheGenerator_remainingDelta(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDragTranslateRigidbody_t5ECFD6EACD00232E073F81F7C8964BF1E983A74F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x44\x72\x61\x67\x54\x72\x61\x6E\x73\x6C\x61\x74\x65\x52\x69\x67\x69\x64\x62\x6F\x64\x79"), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_0_0_0_var), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x44\x72\x61\x67\x20\x54\x72\x61\x6E\x73\x6C\x61\x74\x65\x20\x52\x69\x67\x69\x64\x62\x6F\x64\x79"), NULL);
	}
}
static void LeanDragTranslateRigidbody_t5ECFD6EACD00232E073F81F7C8964BF1E983A74F_CustomAttributesCacheGenerator__camera(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6D\x65\x72\x61"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDragTranslateRigidbody_t5ECFD6EACD00232E073F81F7C8964BF1E983A74F_CustomAttributesCacheGenerator_damping(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x69\x6E\x67"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x65\x6E\x69\x6E\x67"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDragTranslateRigidbody2D_tF462EA4DFA66EB0B9587FBDD52294448D068454B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_0_0_0_var), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x44\x72\x61\x67\x54\x72\x61\x6E\x73\x6C\x61\x74\x65\x52\x69\x67\x69\x64\x62\x6F\x64\x79\x32\x44"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x44\x72\x61\x67\x20\x54\x72\x61\x6E\x73\x6C\x61\x74\x65\x20\x52\x69\x67\x69\x64\x62\x6F\x64\x79\x32\x44"), NULL);
	}
}
static void LeanDragTranslateRigidbody2D_tF462EA4DFA66EB0B9587FBDD52294448D068454B_CustomAttributesCacheGenerator__camera(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6D\x65\x72\x61"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDragTranslateRigidbody2D_tF462EA4DFA66EB0B9587FBDD52294448D068454B_CustomAttributesCacheGenerator_damping(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x65\x6E\x69\x6E\x67"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x69\x6E\x67"), NULL);
	}
}
static void LeanDrop_tEF0A60D70FDD5DE77D81E36E3BD9C15751EA9D89_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x44\x72\x6F\x70"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x44\x72\x6F\x70"), NULL);
	}
}
static void LeanDrop_tEF0A60D70FDD5DE77D81E36E3BD9C15751EA9D89_CustomAttributesCacheGenerator_onDropped(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDropCount_tC0C50670F8D2E1159EF63EA0E2DAC6430FDD974C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x44\x72\x6F\x70\x43\x6F\x75\x6E\x74"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x44\x72\x6F\x70\x20\x43\x6F\x75\x6E\x74"), NULL);
	}
}
static void LeanDropCount_tC0C50670F8D2E1159EF63EA0E2DAC6430FDD974C_CustomAttributesCacheGenerator_count(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDropCount_tC0C50670F8D2E1159EF63EA0E2DAC6430FDD974C_CustomAttributesCacheGenerator_onCount(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDropCount_tC0C50670F8D2E1159EF63EA0E2DAC6430FDD974C_CustomAttributesCacheGenerator_matchMin(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDropCount_tC0C50670F8D2E1159EF63EA0E2DAC6430FDD974C_CustomAttributesCacheGenerator_matchMax(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDropCount_tC0C50670F8D2E1159EF63EA0E2DAC6430FDD974C_CustomAttributesCacheGenerator_onMatch(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDropCount_tC0C50670F8D2E1159EF63EA0E2DAC6430FDD974C_CustomAttributesCacheGenerator_onUnmatch(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDropCount_tC0C50670F8D2E1159EF63EA0E2DAC6430FDD974C_CustomAttributesCacheGenerator_inside(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerDownCanvas_t83AFC349A56E4FA44F186C3B01769F1E7104EDBD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x46\x69\x6E\x67\x65\x72\x20\x44\x6F\x77\x6E\x20\x43\x61\x6E\x76\x61\x73"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x46\x69\x6E\x67\x65\x72\x44\x6F\x77\x6E\x43\x61\x6E\x76\x61\x73"), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[2];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_0_0_0_var), NULL);
	}
}
static void LeanFingerDownCanvas_t83AFC349A56E4FA44F186C3B01769F1E7104EDBD_CustomAttributesCacheGenerator_ignoreStartedOverGui(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x49\x67\x6E\x6F\x72\x65\x53\x74\x61\x72\x74\x65\x64\x4F\x76\x65\x72\x47\x75\x69"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerDownCanvas_t83AFC349A56E4FA44F186C3B01769F1E7104EDBD_CustomAttributesCacheGenerator_requiredSelectable(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x71\x75\x69\x72\x65\x64\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65"), NULL);
	}
}
static void LeanFingerDownCanvas_t83AFC349A56E4FA44F186C3B01769F1E7104EDBD_CustomAttributesCacheGenerator_onFinger(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x44\x6F\x77\x6E"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x44\x6F\x77\x6E"), NULL);
	}
}
static void LeanFingerDownCanvas_t83AFC349A56E4FA44F186C3B01769F1E7104EDBD_CustomAttributesCacheGenerator_onWorld(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerFlick_t59E62BBD06F440FF7849B79C9CC62FE5E34658E2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x46\x69\x6E\x67\x65\x72\x46\x6C\x69\x63\x6B"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x46\x69\x6E\x67\x65\x72\x20\x46\x6C\x69\x63\x6B"), NULL);
	}
}
static void LeanFingerFlick_t59E62BBD06F440FF7849B79C9CC62FE5E34658E2_CustomAttributesCacheGenerator_ignoreStartedOverGui(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x49\x67\x6E\x6F\x72\x65\x53\x74\x61\x72\x74\x65\x64\x4F\x76\x65\x72\x47\x75\x69"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerFlick_t59E62BBD06F440FF7849B79C9CC62FE5E34658E2_CustomAttributesCacheGenerator_ignoreIsOverGui(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x49\x67\x6E\x6F\x72\x65\x49\x73\x4F\x76\x65\x72\x47\x75\x69"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerFlick_t59E62BBD06F440FF7849B79C9CC62FE5E34658E2_CustomAttributesCacheGenerator_requiredSelectable(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x71\x75\x69\x72\x65\x64\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65"), NULL);
	}
}
static void LeanFingerFlick_t59E62BBD06F440FF7849B79C9CC62FE5E34658E2_CustomAttributesCacheGenerator_check(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x68\x65\x63\x6B"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerFlick_t59E62BBD06F440FF7849B79C9CC62FE5E34658E2_CustomAttributesCacheGenerator_fingerDatas(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerHeld_t2D73601702184B4AB62B43E6349F5932F51F5BE2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x46\x69\x6E\x67\x65\x72\x20\x48\x65\x6C\x64"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x23\x4C\x65\x61\x6E\x46\x69\x6E\x67\x65\x72\x48\x65\x6C\x64"), NULL);
	}
}
static void LeanFingerHeld_t2D73601702184B4AB62B43E6349F5932F51F5BE2_CustomAttributesCacheGenerator_ignoreStartedOverGui(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x49\x67\x6E\x6F\x72\x65\x53\x74\x61\x72\x74\x65\x64\x4F\x76\x65\x72\x47\x75\x69"), NULL);
	}
}
static void LeanFingerHeld_t2D73601702184B4AB62B43E6349F5932F51F5BE2_CustomAttributesCacheGenerator_ignoreIsOverGui(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x49\x67\x6E\x6F\x72\x65\x49\x73\x4F\x76\x65\x72\x47\x75\x69"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerHeld_t2D73601702184B4AB62B43E6349F5932F51F5BE2_CustomAttributesCacheGenerator_requiredSelectable(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x71\x75\x69\x72\x65\x64\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerHeld_t2D73601702184B4AB62B43E6349F5932F51F5BE2_CustomAttributesCacheGenerator_minimumAge(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E\x69\x6D\x75\x6D\x41\x67\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerHeld_t2D73601702184B4AB62B43E6349F5932F51F5BE2_CustomAttributesCacheGenerator_maximumMovement(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x69\x6D\x75\x6D\x4D\x6F\x76\x65\x6D\x65\x6E\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerHeld_t2D73601702184B4AB62B43E6349F5932F51F5BE2_CustomAttributesCacheGenerator_onFingerDown(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x48\x65\x6C\x64\x44\x6F\x77\x6E"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x48\x65\x6C\x64\x44\x6F\x77\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerHeld_t2D73601702184B4AB62B43E6349F5932F51F5BE2_CustomAttributesCacheGenerator_onFingerUpdate(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x46\x69\x6E\x67\x65\x72\x53\x65\x74"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x48\x65\x6C\x64\x53\x65\x74"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[3];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x48\x65\x6C\x64\x53\x65\x74"), NULL);
	}
}
static void LeanFingerHeld_t2D73601702184B4AB62B43E6349F5932F51F5BE2_CustomAttributesCacheGenerator_onFingerUp(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x48\x65\x6C\x64\x55\x70"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x48\x65\x6C\x64\x55\x70"), NULL);
	}
}
static void LeanFingerHeld_t2D73601702184B4AB62B43E6349F5932F51F5BE2_CustomAttributesCacheGenerator_onWorldDown(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x50\x6F\x73\x69\x74\x69\x6F\x6E\x44\x6F\x77\x6E"), NULL);
	}
}
static void LeanFingerHeld_t2D73601702184B4AB62B43E6349F5932F51F5BE2_CustomAttributesCacheGenerator_onWorldUpdate(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x50\x6F\x73\x69\x74\x69\x6F\x6E\x53\x65\x74"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x57\x6F\x72\x6C\x64\x53\x65\x74"), NULL);
	}
}
static void LeanFingerHeld_t2D73601702184B4AB62B43E6349F5932F51F5BE2_CustomAttributesCacheGenerator_onWorldUp(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x50\x6F\x73\x69\x74\x69\x6F\x6E\x55\x70"), NULL);
	}
}
static void LeanFingerHeld_t2D73601702184B4AB62B43E6349F5932F51F5BE2_CustomAttributesCacheGenerator_fingerDatas(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerTapExpired_t0D740BAF5108BFB6FD656EBCDBF1A817A8CF62F3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x46\x69\x6E\x67\x65\x72\x54\x61\x70\x45\x78\x70\x69\x72\x65\x64"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x46\x69\x6E\x67\x65\x72\x20\x54\x61\x70\x20\x45\x78\x70\x69\x72\x65\x64"), NULL);
	}
}
static void LeanFingerTapExpired_t0D740BAF5108BFB6FD656EBCDBF1A817A8CF62F3_CustomAttributesCacheGenerator_ignoreStartedOverGui(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x49\x67\x6E\x6F\x72\x65\x53\x74\x61\x72\x74\x65\x64\x4F\x76\x65\x72\x47\x75\x69"), NULL);
	}
}
static void LeanFingerTapExpired_t0D740BAF5108BFB6FD656EBCDBF1A817A8CF62F3_CustomAttributesCacheGenerator_ignoreIsOverGui(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x49\x67\x6E\x6F\x72\x65\x49\x73\x4F\x76\x65\x72\x47\x75\x69"), NULL);
	}
}
static void LeanFingerTapExpired_t0D740BAF5108BFB6FD656EBCDBF1A817A8CF62F3_CustomAttributesCacheGenerator_requiredSelectable(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x71\x75\x69\x72\x65\x64\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerTapExpired_t0D740BAF5108BFB6FD656EBCDBF1A817A8CF62F3_CustomAttributesCacheGenerator_requiredTapCount(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x71\x75\x69\x72\x65\x64\x54\x61\x70\x43\x6F\x75\x6E\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerTapExpired_t0D740BAF5108BFB6FD656EBCDBF1A817A8CF62F3_CustomAttributesCacheGenerator_requiredTapInterval(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x71\x75\x69\x72\x65\x64\x54\x61\x70\x49\x6E\x74\x65\x72\x76\x61\x6C"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerTapExpired_t0D740BAF5108BFB6FD656EBCDBF1A817A8CF62F3_CustomAttributesCacheGenerator_onFinger(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x54\x61\x70"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x54\x61\x70"), NULL);
	}
}
static void LeanFingerTapExpired_t0D740BAF5108BFB6FD656EBCDBF1A817A8CF62F3_CustomAttributesCacheGenerator_onCount(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerTapExpired_t0D740BAF5108BFB6FD656EBCDBF1A817A8CF62F3_CustomAttributesCacheGenerator_onWorld(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerTapQuick_tA073AB23792F860201989828ABFC075396160E51_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x46\x69\x6E\x67\x65\x72\x20\x54\x61\x70\x20\x51\x75\x69\x63\x6B"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x46\x69\x6E\x67\x65\x72\x54\x61\x70\x51\x75\x69\x63\x6B"), NULL);
	}
}
static void LeanFingerTapQuick_tA073AB23792F860201989828ABFC075396160E51_CustomAttributesCacheGenerator_ignoreStartedOverGui(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x49\x67\x6E\x6F\x72\x65\x53\x74\x61\x72\x74\x65\x64\x4F\x76\x65\x72\x47\x75\x69"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerTapQuick_tA073AB23792F860201989828ABFC075396160E51_CustomAttributesCacheGenerator_requiredSelectable(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x71\x75\x69\x72\x65\x64\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65"), NULL);
	}
}
static void LeanFingerTapQuick_tA073AB23792F860201989828ABFC075396160E51_CustomAttributesCacheGenerator_requiredTapCount(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x71\x75\x69\x72\x65\x64\x54\x61\x70\x43\x6F\x75\x6E\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerTapQuick_tA073AB23792F860201989828ABFC075396160E51_CustomAttributesCacheGenerator_onFinger(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x44\x6F\x77\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x44\x6F\x77\x6E"), NULL);
	}
}
static void LeanFingerTapQuick_tA073AB23792F860201989828ABFC075396160E51_CustomAttributesCacheGenerator_onWorld(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerTapQuick_tA073AB23792F860201989828ABFC075396160E51_CustomAttributesCacheGenerator_onScreen(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFirstDown_t453C7DC8250866E727FDB970F8D1335D168BF2E9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x46\x69\x72\x73\x74\x20\x44\x6F\x77\x6E"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x46\x69\x72\x73\x74\x44\x6F\x77\x6E"), NULL);
	}
}
static void LeanFirstDown_t453C7DC8250866E727FDB970F8D1335D168BF2E9_CustomAttributesCacheGenerator_ignoreStartedOverGui(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x49\x67\x6E\x6F\x72\x65\x53\x74\x61\x72\x74\x65\x64\x4F\x76\x65\x72\x47\x75\x69"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFirstDown_t453C7DC8250866E727FDB970F8D1335D168BF2E9_CustomAttributesCacheGenerator_requiredSelectable(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x71\x75\x69\x72\x65\x64\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65"), NULL);
	}
}
static void LeanFirstDown_t453C7DC8250866E727FDB970F8D1335D168BF2E9_CustomAttributesCacheGenerator_onFinger(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFirstDown_t453C7DC8250866E727FDB970F8D1335D168BF2E9_CustomAttributesCacheGenerator_onWorld(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFirstDown_t453C7DC8250866E727FDB970F8D1335D168BF2E9_CustomAttributesCacheGenerator_onScreen(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFirstDownCanvas_t59F37BF3AE99DCBF95FD58FF13F236E6BC8853CB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x46\x69\x72\x73\x74\x20\x44\x6F\x77\x6E\x20\x43\x61\x6E\x76\x61\x73"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x46\x69\x72\x73\x74\x44\x6F\x77\x6E\x43\x61\x6E\x76\x61\x73"), NULL);
	}
}
static void LeanGestureToggle_t54C824553167AC8C6C32BA47CF39CF8BF7425B24_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x47\x65\x73\x74\x75\x72\x65\x20\x54\x6F\x67\x67\x6C\x65"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x47\x65\x73\x74\x75\x72\x65\x54\x6F\x67\x67\x6C\x65"), NULL);
	}
}
static void LeanGestureToggle_t54C824553167AC8C6C32BA47CF39CF8BF7425B24_CustomAttributesCacheGenerator_enableWithoutIsolation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x57\x69\x74\x68\x6F\x75\x74\x49\x73\x6F\x6C\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void LeanGestureToggle_t54C824553167AC8C6C32BA47CF39CF8BF7425B24_CustomAttributesCacheGenerator_dragComponent(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x72\x61\x67\x43\x6F\x6D\x70\x6F\x6E\x65\x6E\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanGestureToggle_t54C824553167AC8C6C32BA47CF39CF8BF7425B24_CustomAttributesCacheGenerator_dragThreshold(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x72\x61\x67\x54\x68\x72\x65\x73\x68\x6F\x6C\x64"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanGestureToggle_t54C824553167AC8C6C32BA47CF39CF8BF7425B24_CustomAttributesCacheGenerator_pinchComponent(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x50\x69\x6E\x63\x68\x43\x6F\x6D\x70\x6F\x6E\x65\x6E\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanGestureToggle_t54C824553167AC8C6C32BA47CF39CF8BF7425B24_CustomAttributesCacheGenerator_pinchThreshold(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x50\x69\x6E\x63\x68\x54\x68\x72\x65\x73\x68\x6F\x6C\x64"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanGestureToggle_t54C824553167AC8C6C32BA47CF39CF8BF7425B24_CustomAttributesCacheGenerator_twistComponent(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x77\x69\x73\x74\x43\x6F\x6D\x70\x6F\x6E\x65\x6E\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanGestureToggle_t54C824553167AC8C6C32BA47CF39CF8BF7425B24_CustomAttributesCacheGenerator_twistThreshold(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x77\x69\x73\x74\x54\x68\x72\x65\x73\x68\x6F\x6C\x64"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanGestureToggle_t54C824553167AC8C6C32BA47CF39CF8BF7425B24_CustomAttributesCacheGenerator_twistWithPinch(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x77\x69\x73\x74\x57\x69\x74\x68\x50\x69\x6E\x63\x68"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanLastUp_t6082DB2D4EBD9B7B9E5B7B11C50C3AC29AB6ABE3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x4C\x61\x73\x74\x20\x55\x70"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x4C\x61\x73\x74\x55\x70"), NULL);
	}
}
static void LeanLastUp_t6082DB2D4EBD9B7B9E5B7B11C50C3AC29AB6ABE3_CustomAttributesCacheGenerator_ignoreStartedOverGui(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x49\x67\x6E\x6F\x72\x65\x53\x74\x61\x72\x74\x65\x64\x4F\x76\x65\x72\x47\x75\x69"), NULL);
	}
}
static void LeanLastUp_t6082DB2D4EBD9B7B9E5B7B11C50C3AC29AB6ABE3_CustomAttributesCacheGenerator_requiredSelectable(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x71\x75\x69\x72\x65\x64\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65"), NULL);
	}
}
static void LeanLastUp_t6082DB2D4EBD9B7B9E5B7B11C50C3AC29AB6ABE3_CustomAttributesCacheGenerator_onFinger(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanLastUp_t6082DB2D4EBD9B7B9E5B7B11C50C3AC29AB6ABE3_CustomAttributesCacheGenerator_onWorld(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanLastUp_t6082DB2D4EBD9B7B9E5B7B11C50C3AC29AB6ABE3_CustomAttributesCacheGenerator_onScreen(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanLastUpCanvas_tB593233F7F8FDE6D6AE25D8651AC606DE9881954_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x4C\x61\x73\x74\x55\x70\x43\x61\x6E\x76\x61\x73"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x4C\x61\x73\x74\x20\x55\x70\x20\x43\x61\x6E\x76\x61\x73"), NULL);
	}
}
static void LeanManualFlick_tAC5969B0E49B99941EED2C714CE1AEBEA4E28992_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x4D\x61\x6E\x75\x61\x6C\x20\x46\x6C\x69\x63\x6B"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x23\x4C\x65\x61\x6E\x4D\x61\x6E\x75\x61\x6C\x46\x6C\x69\x63\x6B"), NULL);
	}
}
static void LeanManualFlick_tAC5969B0E49B99941EED2C714CE1AEBEA4E28992_CustomAttributesCacheGenerator_ignoreIsOverGui(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x49\x67\x6E\x6F\x72\x65\x49\x73\x4F\x76\x65\x72\x47\x75\x69"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualFlick_tAC5969B0E49B99941EED2C714CE1AEBEA4E28992_CustomAttributesCacheGenerator_requiredSelectable(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x71\x75\x69\x72\x65\x64\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65"), NULL);
	}
}
static void LeanManualFlick_tAC5969B0E49B99941EED2C714CE1AEBEA4E28992_CustomAttributesCacheGenerator_check(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x68\x65\x63\x6B"), NULL);
	}
}
static void LeanManualFlick_tAC5969B0E49B99941EED2C714CE1AEBEA4E28992_CustomAttributesCacheGenerator_fingerDatas(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualSwipe_t4B6C0D082FBFFB43D1E2D54A4732467EED9EB11F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x23\x4C\x65\x61\x6E\x4D\x61\x6E\x75\x61\x6C\x53\x77\x69\x70\x65"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x4D\x61\x6E\x75\x61\x6C\x20\x53\x77\x69\x70\x65"), NULL);
	}
}
static void LeanManualSwipe_t4B6C0D082FBFFB43D1E2D54A4732467EED9EB11F_CustomAttributesCacheGenerator_ignoreIsOverGui(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x49\x67\x6E\x6F\x72\x65\x49\x73\x4F\x76\x65\x72\x47\x75\x69"), NULL);
	}
}
static void LeanManualSwipe_t4B6C0D082FBFFB43D1E2D54A4732467EED9EB11F_CustomAttributesCacheGenerator_requiredSelectable(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x71\x75\x69\x72\x65\x64\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65"), NULL);
	}
}
static void LeanMouseWheel_t89CE9C4EA8C22590F4F0A664EFD7F75D87D3E632_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x4D\x6F\x75\x73\x65\x20\x57\x68\x65\x65\x6C"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x4D\x6F\x75\x73\x65\x57\x68\x65\x65\x6C"), NULL);
	}
}
static void LeanMouseWheel_t89CE9C4EA8C22590F4F0A664EFD7F75D87D3E632_CustomAttributesCacheGenerator_requiredSelectable(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x71\x75\x69\x72\x65\x64\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMouseWheel_t89CE9C4EA8C22590F4F0A664EFD7F75D87D3E632_CustomAttributesCacheGenerator_requiredMouseButton(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x71\x75\x69\x72\x65\x64\x4D\x6F\x75\x73\x65\x42\x75\x74\x74\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMouseWheel_t89CE9C4EA8C22590F4F0A664EFD7F75D87D3E632_CustomAttributesCacheGenerator_modify(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x69\x66\x79"), NULL);
	}
}
static void LeanMouseWheel_t89CE9C4EA8C22590F4F0A664EFD7F75D87D3E632_CustomAttributesCacheGenerator_multiplier(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x75\x6C\x74\x69\x70\x6C\x69\x65\x72"), NULL);
	}
}
static void LeanMouseWheel_t89CE9C4EA8C22590F4F0A664EFD7F75D87D3E632_CustomAttributesCacheGenerator_coordinate(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6F\x72\x64\x69\x6E\x61\x74\x65"), NULL);
	}
}
static void LeanMouseWheel_t89CE9C4EA8C22590F4F0A664EFD7F75D87D3E632_CustomAttributesCacheGenerator_onDelta(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiDirection_tE1CE9A8D954D423C3CBD37639359606B9E7CEC23_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x4D\x75\x6C\x74\x69\x44\x69\x72\x65\x63\x74\x69\x6F\x6E"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x4D\x75\x6C\x74\x69\x20\x44\x69\x72\x65\x63\x74\x69\x6F\x6E"), NULL);
	}
}
static void LeanMultiDirection_tE1CE9A8D954D423C3CBD37639359606B9E7CEC23_CustomAttributesCacheGenerator_ignoreIfStatic(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x49\x67\x6E\x6F\x72\x65\x49\x66\x53\x74\x61\x74\x69\x63"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiDirection_tE1CE9A8D954D423C3CBD37639359606B9E7CEC23_CustomAttributesCacheGenerator_angle(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x67\x6C\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiDirection_tE1CE9A8D954D423C3CBD37639359606B9E7CEC23_CustomAttributesCacheGenerator_oneWay(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x65\x57\x61\x79"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiDirection_tE1CE9A8D954D423C3CBD37639359606B9E7CEC23_CustomAttributesCacheGenerator_coordinate(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6F\x72\x64\x69\x6E\x61\x74\x65"), NULL);
	}
}
static void LeanMultiDirection_tE1CE9A8D954D423C3CBD37639359606B9E7CEC23_CustomAttributesCacheGenerator_multiplier(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x75\x6C\x74\x69\x70\x6C\x69\x65\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiDirection_tE1CE9A8D954D423C3CBD37639359606B9E7CEC23_CustomAttributesCacheGenerator_onDelta(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiDown_t87D39553D60C975F9E133450FA4B6AB74B4D24A2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x4D\x75\x6C\x74\x69\x44\x6F\x77\x6E"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x4D\x75\x6C\x74\x69\x20\x44\x6F\x77\x6E"), NULL);
	}
}
static void LeanMultiDown_t87D39553D60C975F9E133450FA4B6AB74B4D24A2_CustomAttributesCacheGenerator_ignoreStartedOverGui(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x49\x67\x6E\x6F\x72\x65\x53\x74\x61\x72\x74\x65\x64\x4F\x76\x65\x72\x47\x75\x69"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiDown_t87D39553D60C975F9E133450FA4B6AB74B4D24A2_CustomAttributesCacheGenerator_requiredSelectable(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x71\x75\x69\x72\x65\x64\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiDown_t87D39553D60C975F9E133450FA4B6AB74B4D24A2_CustomAttributesCacheGenerator_requiredCount(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x71\x75\x69\x72\x65\x64\x43\x6F\x75\x6E\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiDown_t87D39553D60C975F9E133450FA4B6AB74B4D24A2_CustomAttributesCacheGenerator_onFingers(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiDown_t87D39553D60C975F9E133450FA4B6AB74B4D24A2_CustomAttributesCacheGenerator_onWorld(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiDown_t87D39553D60C975F9E133450FA4B6AB74B4D24A2_CustomAttributesCacheGenerator_onScreen(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x4D\x75\x6C\x74\x69\x48\x65\x6C\x64"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x4D\x75\x6C\x74\x69\x20\x48\x65\x6C\x64"), NULL);
	}
}
static void LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_ignoreStartedOverGui(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x49\x67\x6E\x6F\x72\x65\x53\x74\x61\x72\x74\x65\x64\x4F\x76\x65\x72\x47\x75\x69"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_ignoreIsOverGui(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x49\x67\x6E\x6F\x72\x65\x49\x73\x4F\x76\x65\x72\x47\x75\x69"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_requiredSelectable(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x71\x75\x69\x72\x65\x64\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_requiredCount(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x71\x75\x69\x72\x65\x64\x43\x6F\x75\x6E\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_minimumAge(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E\x69\x6D\x75\x6D\x41\x67\x65"), NULL);
	}
}
static void LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_maximumMovement(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x69\x6D\x75\x6D\x4D\x6F\x76\x65\x6D\x65\x6E\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_onFingersDown(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_onFingersUpdate(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_onFingersUp(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_onWorldDown(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_onWorldUpdate(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_onWorldUp(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_onScreenDown(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_onScreenUpdate(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_onScreenUp(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_fingers(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_fingerDatas(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_held(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_duration(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiPinch_t8A25F40415662B66266BE64ACB1FD4145FE9CFC2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x4D\x75\x6C\x74\x69\x20\x50\x69\x6E\x63\x68"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x4D\x75\x6C\x74\x69\x50\x69\x6E\x63\x68"), NULL);
	}
}
static void LeanMultiPinch_t8A25F40415662B66266BE64ACB1FD4145FE9CFC2_CustomAttributesCacheGenerator_ignoreIfStatic(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x49\x67\x6E\x6F\x72\x65\x49\x66\x53\x74\x61\x74\x69\x63"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiPinch_t8A25F40415662B66266BE64ACB1FD4145FE9CFC2_CustomAttributesCacheGenerator_coordinate(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6F\x72\x64\x69\x6E\x61\x74\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiPinch_t8A25F40415662B66266BE64ACB1FD4145FE9CFC2_CustomAttributesCacheGenerator_multiplier(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x75\x6C\x74\x69\x70\x6C\x69\x65\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiPinch_t8A25F40415662B66266BE64ACB1FD4145FE9CFC2_CustomAttributesCacheGenerator_onPinch(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiPull_tBC5075E22D98EE889301850B0437AAC3AE7F02C6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x4D\x75\x6C\x74\x69\x50\x75\x6C\x6C"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x4D\x75\x6C\x74\x69\x20\x50\x75\x6C\x6C"), NULL);
	}
}
static void LeanMultiPull_tBC5075E22D98EE889301850B0437AAC3AE7F02C6_CustomAttributesCacheGenerator_coordinate(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6F\x72\x64\x69\x6E\x61\x74\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiPull_tBC5075E22D98EE889301850B0437AAC3AE7F02C6_CustomAttributesCacheGenerator_multiplier(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x75\x6C\x74\x69\x70\x6C\x69\x65\x72"), NULL);
	}
}
static void LeanMultiPull_tBC5075E22D98EE889301850B0437AAC3AE7F02C6_CustomAttributesCacheGenerator_scaleByTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x61\x6C\x65\x42\x79\x54\x69\x6D\x65"), NULL);
	}
}
static void LeanMultiPull_tBC5075E22D98EE889301850B0437AAC3AE7F02C6_CustomAttributesCacheGenerator_onVector(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiPull_tBC5075E22D98EE889301850B0437AAC3AE7F02C6_CustomAttributesCacheGenerator_onDistance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiPull_tBC5075E22D98EE889301850B0437AAC3AE7F02C6_CustomAttributesCacheGenerator_onWorldFrom(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiPull_tBC5075E22D98EE889301850B0437AAC3AE7F02C6_CustomAttributesCacheGenerator_onWorldTo(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiPull_tBC5075E22D98EE889301850B0437AAC3AE7F02C6_CustomAttributesCacheGenerator_onWorldDelta(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiPull_tBC5075E22D98EE889301850B0437AAC3AE7F02C6_CustomAttributesCacheGenerator_onWorldFromTo(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiSwipe_t0A7F14E3CF4D31BB16D75ED5344828D32236ED91_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x4D\x75\x6C\x74\x69\x20\x53\x77\x69\x70\x65"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x4D\x75\x6C\x74\x69\x53\x77\x69\x70\x65"), NULL);
	}
}
static void LeanMultiSwipe_t0A7F14E3CF4D31BB16D75ED5344828D32236ED91_CustomAttributesCacheGenerator_scaledDistanceThreshold(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x61\x6C\x65\x64\x44\x69\x73\x74\x61\x6E\x63\x65\x54\x68\x72\x65\x73\x68\x6F\x6C\x64"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiSwipe_t0A7F14E3CF4D31BB16D75ED5344828D32236ED91_CustomAttributesCacheGenerator_parallelAngleThreshold(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x50\x61\x72\x61\x6C\x6C\x65\x6C\x41\x6E\x67\x6C\x65\x54\x68\x72\x65\x73\x68\x6F\x6C\x64"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiSwipe_t0A7F14E3CF4D31BB16D75ED5344828D32236ED91_CustomAttributesCacheGenerator_pinchScaledDistanceThreshold(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x50\x69\x6E\x63\x68\x53\x63\x61\x6C\x65\x64\x44\x69\x73\x74\x61\x6E\x63\x65\x54\x68\x72\x65\x73\x68\x6F\x6C\x64"), NULL);
	}
}
static void LeanMultiSwipe_t0A7F14E3CF4D31BB16D75ED5344828D32236ED91_CustomAttributesCacheGenerator_onFingers(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x53\x77\x69\x70\x65"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x53\x77\x69\x70\x65"), NULL);
	}
}
static void LeanMultiSwipe_t0A7F14E3CF4D31BB16D75ED5344828D32236ED91_CustomAttributesCacheGenerator_onSwipeParallel(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x53\x77\x69\x70\x65\x50\x61\x72\x61\x6C\x6C\x65\x6C"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiSwipe_t0A7F14E3CF4D31BB16D75ED5344828D32236ED91_CustomAttributesCacheGenerator_onSwipeIn(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x53\x77\x69\x70\x65\x49\x6E"), NULL);
	}
}
static void LeanMultiSwipe_t0A7F14E3CF4D31BB16D75ED5344828D32236ED91_CustomAttributesCacheGenerator_onSwipeOut(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x53\x77\x69\x70\x65\x4F\x75\x74"), NULL);
	}
}
static void LeanMultiTap_t502D6311C2CEB8E7C54406823EF8025EA35F3373_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x4D\x75\x6C\x74\x69\x20\x54\x61\x70"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x4D\x75\x6C\x74\x69\x54\x61\x70"), NULL);
	}
}
static void LeanMultiTap_t502D6311C2CEB8E7C54406823EF8025EA35F3373_CustomAttributesCacheGenerator_onTap(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiTap_t502D6311C2CEB8E7C54406823EF8025EA35F3373_CustomAttributesCacheGenerator_onCount(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiTap_t502D6311C2CEB8E7C54406823EF8025EA35F3373_CustomAttributesCacheGenerator_onHighest(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiTap_t502D6311C2CEB8E7C54406823EF8025EA35F3373_CustomAttributesCacheGenerator_onCountHighest(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x54\x61\x70"), NULL);
	}
}
static void LeanMultiTwist_tFF0088194D2DBA58D36E5D677BD8C56C680AFA28_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x4D\x75\x6C\x74\x69\x20\x54\x77\x69\x73\x74"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x4D\x75\x6C\x74\x69\x54\x77\x69\x73\x74"), NULL);
	}
}
static void LeanMultiTwist_tFF0088194D2DBA58D36E5D677BD8C56C680AFA28_CustomAttributesCacheGenerator_ignoreIfStatic(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x49\x67\x6E\x6F\x72\x65\x49\x66\x53\x74\x61\x74\x69\x63"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiTwist_tFF0088194D2DBA58D36E5D677BD8C56C680AFA28_CustomAttributesCacheGenerator_oneFinger(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x65\x46\x69\x6E\x67\x65\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiTwist_tFF0088194D2DBA58D36E5D677BD8C56C680AFA28_CustomAttributesCacheGenerator_onTwistDegrees(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x54\x77\x69\x73\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiUp_t8E4EE9C5AF6A43A7E39386AF32C34AED70B00E5A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x4D\x75\x6C\x74\x69\x55\x70"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x4D\x75\x6C\x74\x69\x20\x55\x70"), NULL);
	}
}
static void LeanMultiUp_t8E4EE9C5AF6A43A7E39386AF32C34AED70B00E5A_CustomAttributesCacheGenerator_onFinger(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiUp_t8E4EE9C5AF6A43A7E39386AF32C34AED70B00E5A_CustomAttributesCacheGenerator_onWorld(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiUp_t8E4EE9C5AF6A43A7E39386AF32C34AED70B00E5A_CustomAttributesCacheGenerator_onScreen(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiUpdate_t6F6FFADB9EB0F2FEE3F6C92A3903D8C3C78205AE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x4D\x75\x6C\x74\x69\x20\x55\x70\x64\x61\x74\x65"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x4D\x75\x6C\x74\x69\x55\x70\x64\x61\x74\x65"), NULL);
	}
}
static void LeanMultiUpdate_t6F6FFADB9EB0F2FEE3F6C92A3903D8C3C78205AE_CustomAttributesCacheGenerator_ignoreIfStatic(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x49\x67\x6E\x6F\x72\x65\x49\x66\x53\x74\x61\x74\x69\x63"), NULL);
	}
}
static void LeanMultiUpdate_t6F6FFADB9EB0F2FEE3F6C92A3903D8C3C78205AE_CustomAttributesCacheGenerator_onFingers(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x53\x65\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiUpdate_t6F6FFADB9EB0F2FEE3F6C92A3903D8C3C78205AE_CustomAttributesCacheGenerator_coordinate(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6F\x72\x64\x69\x6E\x61\x74\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiUpdate_t6F6FFADB9EB0F2FEE3F6C92A3903D8C3C78205AE_CustomAttributesCacheGenerator_multiplier(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x75\x6C\x74\x69\x70\x6C\x69\x65\x72"), NULL);
	}
}
static void LeanMultiUpdate_t6F6FFADB9EB0F2FEE3F6C92A3903D8C3C78205AE_CustomAttributesCacheGenerator_onDelta(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x44\x72\x61\x67\x44\x65\x6C\x74\x61"), NULL);
	}
}
static void LeanMultiUpdate_t6F6FFADB9EB0F2FEE3F6C92A3903D8C3C78205AE_CustomAttributesCacheGenerator_onDistance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiUpdate_t6F6FFADB9EB0F2FEE3F6C92A3903D8C3C78205AE_CustomAttributesCacheGenerator_onWorldFrom(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiUpdate_t6F6FFADB9EB0F2FEE3F6C92A3903D8C3C78205AE_CustomAttributesCacheGenerator_onWorldTo(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiUpdate_t6F6FFADB9EB0F2FEE3F6C92A3903D8C3C78205AE_CustomAttributesCacheGenerator_onWorldDelta(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiUpdate_t6F6FFADB9EB0F2FEE3F6C92A3903D8C3C78205AE_CustomAttributesCacheGenerator_onWorldFromTo(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiUpdateCanvas_tDAD437108A5ECCE0B5429FFFA65D56CA2E3EC1AD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x4D\x75\x6C\x74\x69\x55\x70\x64\x61\x74\x65\x43\x61\x6E\x76\x61\x73"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x4D\x75\x6C\x74\x69\x20\x55\x70\x64\x61\x74\x65\x20\x43\x61\x6E\x76\x61\x73"), NULL);
	}
}
static void LeanMultiUpdateCanvas_tDAD437108A5ECCE0B5429FFFA65D56CA2E3EC1AD_CustomAttributesCacheGenerator_ignoreIfOff(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x49\x67\x6E\x6F\x72\x65\x49\x66\x4F\x66\x66"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiUpdateCanvas_tDAD437108A5ECCE0B5429FFFA65D56CA2E3EC1AD_CustomAttributesCacheGenerator_onFingers(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMultiUpdateCanvas_tDAD437108A5ECCE0B5429FFFA65D56CA2E3EC1AD_CustomAttributesCacheGenerator_onWorld(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanPick_t7AC38F818E2067FC14015F54A1396824461FDFC1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x50\x69\x63\x6B"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x50\x69\x63\x6B"), NULL);
	}
}
static void LeanPick_t7AC38F818E2067FC14015F54A1396824461FDFC1_CustomAttributesCacheGenerator_requiredTag(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanPick_t7AC38F818E2067FC14015F54A1396824461FDFC1_CustomAttributesCacheGenerator_onPickable(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanPickable_t39FF156752D677DBB205A7658738C50B8882A2C4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x50\x69\x63\x6B\x61\x62\x6C\x65"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x50\x69\x63\x6B\x61\x62\x6C\x65"), NULL);
	}
}
static void LeanPickable_t39FF156752D677DBB205A7658738C50B8882A2C4_CustomAttributesCacheGenerator_onFinger(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanPickable_t39FF156752D677DBB205A7658738C50B8882A2C4_CustomAttributesCacheGenerator_onWorld(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanPickable_t39FF156752D677DBB205A7658738C50B8882A2C4_CustomAttributesCacheGenerator_onScreen(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanPinchCamera_t4BBC5706A596D0A7AE4271D24CDC56883D816A59_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x50\x69\x6E\x63\x68\x43\x61\x6D\x65\x72\x61"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x50\x69\x6E\x63\x68\x20\x43\x61\x6D\x65\x72\x61"), NULL);
	}
}
static void LeanPinchCamera_t4BBC5706A596D0A7AE4271D24CDC56883D816A59_CustomAttributesCacheGenerator__camera(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6D\x65\x72\x61"), NULL);
	}
}
static void LeanPinchCamera_t4BBC5706A596D0A7AE4271D24CDC56883D816A59_CustomAttributesCacheGenerator_zoom(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x5A\x6F\x6F\x6D"), NULL);
	}
}
static void LeanPinchCamera_t4BBC5706A596D0A7AE4271D24CDC56883D816A59_CustomAttributesCacheGenerator_damping(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x69\x6E\x67"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x65\x6E\x69\x6E\x67"), NULL);
	}
}
static void LeanPinchCamera_t4BBC5706A596D0A7AE4271D24CDC56883D816A59_CustomAttributesCacheGenerator_clamp(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6C\x61\x6D\x70"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x5A\x6F\x6F\x6D\x43\x6C\x61\x6D\x70"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanPinchCamera_t4BBC5706A596D0A7AE4271D24CDC56883D816A59_CustomAttributesCacheGenerator_clampMin(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6C\x61\x6D\x70\x4D\x69\x6E"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x5A\x6F\x6F\x6D\x4D\x69\x6E"), NULL);
	}
}
static void LeanPinchCamera_t4BBC5706A596D0A7AE4271D24CDC56883D816A59_CustomAttributesCacheGenerator_clampMax(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6C\x61\x6D\x70\x4D\x61\x78"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x5A\x6F\x6F\x6D\x4D\x61\x78"), NULL);
	}
}
static void LeanPinchCamera_t4BBC5706A596D0A7AE4271D24CDC56883D816A59_CustomAttributesCacheGenerator_relative(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x6C\x61\x74\x69\x76\x65"), NULL);
	}
}
static void LeanPinchCamera_t4BBC5706A596D0A7AE4271D24CDC56883D816A59_CustomAttributesCacheGenerator_ignoreZ(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x49\x67\x6E\x6F\x72\x65\x5A"), NULL);
	}
}
static void LeanPinchCamera_t4BBC5706A596D0A7AE4271D24CDC56883D816A59_CustomAttributesCacheGenerator_currentZoom(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanPinchCamera_t4BBC5706A596D0A7AE4271D24CDC56883D816A59_CustomAttributesCacheGenerator_remainingTranslation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanReplayFinger_tFF28D44D6E4F13B826B5FA95AFAD6B7F1F2D2C30_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x52\x65\x70\x6C\x61\x79\x46\x69\x6E\x67\x65\x72"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x52\x65\x70\x6C\x61\x79\x20\x46\x69\x6E\x67\x65\x72"), NULL);
	}
}
static void LeanReplayFinger_tFF28D44D6E4F13B826B5FA95AFAD6B7F1F2D2C30_CustomAttributesCacheGenerator_cursor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x75\x72\x73\x6F\x72"), NULL);
	}
}
static void LeanReplayFinger_tFF28D44D6E4F13B826B5FA95AFAD6B7F1F2D2C30_CustomAttributesCacheGenerator_playing(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x61\x79\x69\x6E\x67"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanReplayFinger_tFF28D44D6E4F13B826B5FA95AFAD6B7F1F2D2C30_CustomAttributesCacheGenerator_playTime(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x61\x79\x54\x69\x6D\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectableBlock_t4686257957B99475EF70DA5E48F053A6D32B9811_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65\x42\x6C\x6F\x63\x6B"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65\x20\x42\x6C\x6F\x63\x6B"), NULL);
	}
}
static void LeanSelectableBlock_t4686257957B99475EF70DA5E48F053A6D32B9811_CustomAttributesCacheGenerator_x(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x58"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectableBlock_t4686257957B99475EF70DA5E48F053A6D32B9811_CustomAttributesCacheGenerator_y(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x59"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectableBlock_t4686257957B99475EF70DA5E48F053A6D32B9811_CustomAttributesCacheGenerator_blockSize(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x42\x6C\x6F\x63\x6B\x53\x69\x7A\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectableBlock_t4686257957B99475EF70DA5E48F053A6D32B9811_CustomAttributesCacheGenerator_deselectOnSwap(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x73\x65\x6C\x65\x63\x74\x4F\x6E\x53\x77\x61\x70"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectableBlock_t4686257957B99475EF70DA5E48F053A6D32B9811_CustomAttributesCacheGenerator_damping(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x65\x6E\x69\x6E\x67"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x69\x6E\x67"), NULL);
	}
}
static void LeanSelectableCenter_tC439B58BA06F9E94E614DFA8B38E3575478D8A53_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65\x43\x65\x6E\x74\x65\x72"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65\x20\x43\x65\x6E\x74\x65\x72"), NULL);
	}
}
static void LeanSelectableCenter_tC439B58BA06F9E94E614DFA8B38E3575478D8A53_CustomAttributesCacheGenerator_onPosition(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectableCenter_tC439B58BA06F9E94E614DFA8B38E3575478D8A53_CustomAttributesCacheGenerator_LeanSelectableCenter_Calculate_m30CCA82E5C0E2B6C838A6DBA5C43DDD627F0AF4C(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6C\x63\x75\x6C\x61\x74\x65"), NULL);
	}
}
static void LeanSelectableCount_t940958E9FD3A349EB3EAAACE0616A4A8F36EC770_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65\x20\x43\x6F\x75\x6E\x74"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65\x43\x6F\x75\x6E\x74"), NULL);
	}
}
static void LeanSelectableCount_t940958E9FD3A349EB3EAAACE0616A4A8F36EC770_CustomAttributesCacheGenerator_count(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectableCount_t940958E9FD3A349EB3EAAACE0616A4A8F36EC770_CustomAttributesCacheGenerator_onCount(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectableCount_t940958E9FD3A349EB3EAAACE0616A4A8F36EC770_CustomAttributesCacheGenerator_matchMin(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectableCount_t940958E9FD3A349EB3EAAACE0616A4A8F36EC770_CustomAttributesCacheGenerator_matchMax(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectableCount_t940958E9FD3A349EB3EAAACE0616A4A8F36EC770_CustomAttributesCacheGenerator_onMatch(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectableCount_t940958E9FD3A349EB3EAAACE0616A4A8F36EC770_CustomAttributesCacheGenerator_onUnmatch(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectableCount_t940958E9FD3A349EB3EAAACE0616A4A8F36EC770_CustomAttributesCacheGenerator_inside(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectableDial_t1D562EED60558A453D5A54084CB1EB573719AC3F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65\x20\x44\x69\x61\x6C"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65\x44\x69\x61\x6C"), NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[2];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void LeanSelectableDial_t1D562EED60558A453D5A54084CB1EB573719AC3F_CustomAttributesCacheGenerator__camera(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6D\x65\x72\x61"), NULL);
	}
}
static void LeanSelectableDial_t1D562EED60558A453D5A54084CB1EB573719AC3F_CustomAttributesCacheGenerator_tilt(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x69\x6C\x74"), NULL);
	}
}
static void LeanSelectableDial_t1D562EED60558A453D5A54084CB1EB573719AC3F_CustomAttributesCacheGenerator_axis(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x41\x78\x69\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectableDial_t1D562EED60558A453D5A54084CB1EB573719AC3F_CustomAttributesCacheGenerator_angle(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x67\x6C\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectableDial_t1D562EED60558A453D5A54084CB1EB573719AC3F_CustomAttributesCacheGenerator_clamp(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6C\x61\x6D\x70"), NULL);
	}
}
static void LeanSelectableDial_t1D562EED60558A453D5A54084CB1EB573719AC3F_CustomAttributesCacheGenerator_clampMin(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6C\x61\x6D\x70\x4D\x69\x6E"), NULL);
	}
}
static void LeanSelectableDial_t1D562EED60558A453D5A54084CB1EB573719AC3F_CustomAttributesCacheGenerator_clampMax(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6C\x61\x6D\x70\x4D\x61\x78"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectableDial_t1D562EED60558A453D5A54084CB1EB573719AC3F_CustomAttributesCacheGenerator_triggers(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x72\x69\x67\x67\x65\x72\x73"), NULL);
	}
}
static void LeanSelectableDial_t1D562EED60558A453D5A54084CB1EB573719AC3F_CustomAttributesCacheGenerator_onAngleChanged(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Trigger_tCDE0F6503E25FA1F39A693069031F6D3CA0BBC2A_CustomAttributesCacheGenerator_Angle(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x63\x65\x6E\x74\x72\x61\x6C\x20\x41\x6E\x67\x6C\x65\x20\x6F\x66\x20\x74\x68\x69\x73\x20\x74\x72\x69\x67\x67\x65\x72\x20\x69\x6E\x20\x64\x65\x67\x72\x65\x65\x73\x2E"), NULL);
	}
}
static void Trigger_tCDE0F6503E25FA1F39A693069031F6D3CA0BBC2A_CustomAttributesCacheGenerator_Arc(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x61\x6E\x67\x6C\x65\x20\x72\x61\x6E\x67\x65\x20\x6F\x66\x20\x74\x68\x69\x73\x20\x74\x72\x69\x67\x67\x65\x72\x20\x69\x6E\x20\x64\x65\x67\x72\x65\x65\x73\x2E\xA\xA\x39\x30\x20\x3D\x20\x51\x75\x61\x72\x74\x65\x72\x20\x63\x69\x72\x63\x6C\x65\x2E\xA\x31\x38\x30\x20\x3D\x20\x48\x61\x6C\x66\x20\x63\x69\x72\x63\x6C\x65\x2E"), NULL);
	}
}
static void Trigger_tCDE0F6503E25FA1F39A693069031F6D3CA0BBC2A_CustomAttributesCacheGenerator_Inside(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void Trigger_tCDE0F6503E25FA1F39A693069031F6D3CA0BBC2A_CustomAttributesCacheGenerator_onEnter(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Trigger_tCDE0F6503E25FA1F39A693069031F6D3CA0BBC2A_CustomAttributesCacheGenerator_onExit(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectableDragTorque_t5AA1E3CBFD81E3E8E983845CB55AFE2A17F62D33_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65\x20\x44\x72\x61\x67\x20\x54\x6F\x72\x71\x75\x65"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65\x44\x72\x61\x67\x54\x6F\x72\x71\x75\x65"), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[2];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_0_0_0_var), NULL);
	}
}
static void LeanSelectableDragTorque_t5AA1E3CBFD81E3E8E983845CB55AFE2A17F62D33_CustomAttributesCacheGenerator__camera(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6D\x65\x72\x61"), NULL);
	}
}
static void LeanSelectableDragTorque_t5AA1E3CBFD81E3E8E983845CB55AFE2A17F62D33_CustomAttributesCacheGenerator_force(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x46\x6F\x72\x63\x65"), NULL);
	}
}
static void LeanSelectableDrop_tCB563847DF952D144B8C919EE0302F02C9D78D5C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65\x44\x72\x6F\x70"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65\x20\x44\x72\x6F\x70"), NULL);
	}
}
static void LeanSelectableDrop_tCB563847DF952D144B8C919EE0302F02C9D78D5C_CustomAttributesCacheGenerator_ignore(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectableDrop_tCB563847DF952D144B8C919EE0302F02C9D78D5C_CustomAttributesCacheGenerator_onGameObject(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectableDrop_tCB563847DF952D144B8C919EE0302F02C9D78D5C_CustomAttributesCacheGenerator_onDropHandler(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectablePressureScale_t88C9F67057F1EB71A53EE205D83DEAAD0FFFB8C0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65\x50\x72\x65\x73\x73\x75\x72\x65\x53\x63\x61\x6C\x65"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65\x20\x50\x72\x65\x73\x73\x75\x72\x65\x20\x53\x63\x61\x6C\x65"), NULL);
	}
}
static void LeanSelectablePressureScale_t88C9F67057F1EB71A53EE205D83DEAAD0FFFB8C0_CustomAttributesCacheGenerator_baseScale(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x42\x61\x73\x65\x53\x63\x61\x6C\x65"), NULL);
	}
}
static void LeanSelectablePressureScale_t88C9F67057F1EB71A53EE205D83DEAAD0FFFB8C0_CustomAttributesCacheGenerator_pressureMultiplier(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x65\x73\x73\x75\x72\x65\x4D\x75\x6C\x74\x69\x70\x6C\x69\x65\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectablePressureScale_t88C9F67057F1EB71A53EE205D83DEAAD0FFFB8C0_CustomAttributesCacheGenerator_pressureClamp(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x65\x73\x73\x75\x72\x65\x43\x6C\x61\x6D\x70"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectableSelected_t7DED535D379F288839974F3DE7251E82A392AF56_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65\x53\x65\x6C\x65\x63\x74\x65\x64"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65\x20\x53\x65\x6C\x65\x63\x74\x65\x64"), NULL);
	}
}
static void LeanSelectableSelected_t7DED535D379F288839974F3DE7251E82A392AF56_CustomAttributesCacheGenerator_threshold(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x72\x65\x73\x68\x6F\x6C\x64"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectableSelected_t7DED535D379F288839974F3DE7251E82A392AF56_CustomAttributesCacheGenerator_reset(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x73\x65\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectableSelected_t7DED535D379F288839974F3DE7251E82A392AF56_CustomAttributesCacheGenerator_rawSelection(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x61\x77\x53\x65\x6C\x65\x63\x74\x69\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectableSelected_t7DED535D379F288839974F3DE7251E82A392AF56_CustomAttributesCacheGenerator_requireFinger(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x71\x75\x69\x72\x65\x46\x69\x6E\x67\x65\x72"), NULL);
	}
}
static void LeanSelectableSelected_t7DED535D379F288839974F3DE7251E82A392AF56_CustomAttributesCacheGenerator_onSelectableDown(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x44\x6F\x77\x6E"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x44\x6F\x77\x6E"), NULL);
	}
}
static void LeanSelectableSelected_t7DED535D379F288839974F3DE7251E82A392AF56_CustomAttributesCacheGenerator_onSelectableUpdate(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x53\x65\x74"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x53\x65\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[3];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65\x53\x65\x74"), NULL);
	}
}
static void LeanSelectableSelected_t7DED535D379F288839974F3DE7251E82A392AF56_CustomAttributesCacheGenerator_onSelectableUp(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x55\x70"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x55\x70"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectableSelected_t7DED535D379F288839974F3DE7251E82A392AF56_CustomAttributesCacheGenerator_lastSet(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectableSelected_t7DED535D379F288839974F3DE7251E82A392AF56_CustomAttributesCacheGenerator_seconds(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectableTime_t9E8C411A0CEE294619CE3CF7777EE3D7405904A1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65\x54\x69\x6D\x65"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65\x20\x54\x69\x6D\x65"), NULL);
	}
}
static void LeanSelectableTime_t9E8C411A0CEE294619CE3CF7777EE3D7405904A1_CustomAttributesCacheGenerator_send(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectableTime_t9E8C411A0CEE294619CE3CF7777EE3D7405904A1_CustomAttributesCacheGenerator_onSeconds(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectableTime_t9E8C411A0CEE294619CE3CF7777EE3D7405904A1_CustomAttributesCacheGenerator_seconds(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectionBox_tB91EB102282663B609ACF5C33D2F0949D525F02A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x53\x65\x6C\x65\x63\x74\x69\x6F\x6E\x20\x42\x6F\x78"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x53\x65\x6C\x65\x63\x74\x69\x6F\x6E\x42\x6F\x78"), NULL);
	}
}
static void LeanSelectionBox_tB91EB102282663B609ACF5C33D2F0949D525F02A_CustomAttributesCacheGenerator__camera(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6D\x65\x72\x61"), NULL);
	}
}
static void LeanSelectionBox_tB91EB102282663B609ACF5C33D2F0949D525F02A_CustomAttributesCacheGenerator_ignoreIfStartedOverGui(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x49\x67\x6E\x6F\x72\x65\x49\x66\x53\x74\x61\x72\x74\x65\x64\x4F\x76\x65\x72\x47\x75\x69"), NULL);
	}
}
static void LeanSelectionBox_tB91EB102282663B609ACF5C33D2F0949D525F02A_CustomAttributesCacheGenerator_prefab(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x65\x66\x61\x62"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectionBox_tB91EB102282663B609ACF5C33D2F0949D525F02A_CustomAttributesCacheGenerator_root(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x6F\x6F\x74"), NULL);
	}
}
static void LeanSelectionBox_tB91EB102282663B609ACF5C33D2F0949D525F02A_CustomAttributesCacheGenerator_select(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x6C\x65\x63\x74"), NULL);
	}
}
static void LeanSelectionBox_tB91EB102282663B609ACF5C33D2F0949D525F02A_CustomAttributesCacheGenerator_requiredLayers(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectionBox_tB91EB102282663B609ACF5C33D2F0949D525F02A_CustomAttributesCacheGenerator_requiredTags(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanShape_t5D52B9E24475D82B0ED8B75B97D95A35AE845C2F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x53\x68\x61\x70\x65"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x53\x68\x61\x70\x65"), NULL);
	}
}
static void LeanShape_t5D52B9E24475D82B0ED8B75B97D95A35AE845C2F_CustomAttributesCacheGenerator_connectEnds(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x6E\x65\x63\x74\x45\x6E\x64\x73"), NULL);
	}
}
static void LeanShape_t5D52B9E24475D82B0ED8B75B97D95A35AE845C2F_CustomAttributesCacheGenerator_visual(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x56\x69\x73\x75\x61\x6C"), NULL);
	}
}
static void LeanShape_t5D52B9E24475D82B0ED8B75B97D95A35AE845C2F_CustomAttributesCacheGenerator_points(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6F\x69\x6E\x74\x73"), NULL);
	}
}
static void LeanShapeDetector_t35E8FCF47AEFC36E057213DFD75779BEAAE4BF17_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x53\x68\x61\x70\x65\x44\x65\x74\x65\x63\x74\x6F\x72"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x53\x68\x61\x70\x65\x20\x44\x65\x74\x65\x63\x74\x6F\x72"), NULL);
	}
}
static void LeanShapeDetector_t35E8FCF47AEFC36E057213DFD75779BEAAE4BF17_CustomAttributesCacheGenerator_shape(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x68\x61\x70\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanShapeDetector_t35E8FCF47AEFC36E057213DFD75779BEAAE4BF17_CustomAttributesCacheGenerator_stepThreshold(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x65\x70\x54\x68\x72\x65\x73\x68\x6F\x6C\x64"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanShapeDetector_t35E8FCF47AEFC36E057213DFD75779BEAAE4BF17_CustomAttributesCacheGenerator_distanceThreshold(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x73\x74\x61\x6E\x63\x65\x54\x68\x72\x65\x73\x68\x6F\x6C\x64"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanShapeDetector_t35E8FCF47AEFC36E057213DFD75779BEAAE4BF17_CustomAttributesCacheGenerator_errorThreshold(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x45\x72\x72\x6F\x72\x54\x68\x72\x65\x73\x68\x6F\x6C\x64"), NULL);
	}
}
static void LeanShapeDetector_t35E8FCF47AEFC36E057213DFD75779BEAAE4BF17_CustomAttributesCacheGenerator_minimumPoints(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E\x69\x6D\x75\x6D\x50\x6F\x69\x6E\x74\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanShapeDetector_t35E8FCF47AEFC36E057213DFD75779BEAAE4BF17_CustomAttributesCacheGenerator_direction(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x72\x65\x63\x74\x69\x6F\x6E"), NULL);
	}
}
static void LeanShapeDetector_t35E8FCF47AEFC36E057213DFD75779BEAAE4BF17_CustomAttributesCacheGenerator_onDetected(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSpawnWithFinger_t1005A142FB8FD4AAB1976B2AF6E24EEE8F869E20_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x53\x70\x61\x77\x6E\x20\x57\x69\x74\x68\x20\x46\x69\x6E\x67\x65\x72"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x23\x4C\x65\x61\x6E\x53\x70\x61\x77\x6E\x57\x69\x74\x68\x46\x69\x6E\x67\x65\x72"), NULL);
	}
}
static void LeanSpawnWithFinger_t1005A142FB8FD4AAB1976B2AF6E24EEE8F869E20_CustomAttributesCacheGenerator_prefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x65\x66\x61\x62"), NULL);
	}
}
static void LeanSpawnWithFinger_t1005A142FB8FD4AAB1976B2AF6E24EEE8F869E20_CustomAttributesCacheGenerator_rotateTo(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x6F\x74\x61\x74\x65\x54\x6F"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSpawnWithFinger_t1005A142FB8FD4AAB1976B2AF6E24EEE8F869E20_CustomAttributesCacheGenerator_dragAfterSpawn(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x72\x61\x67\x41\x66\x74\x65\x72\x53\x70\x61\x77\x6E"), NULL);
	}
}
static void LeanSpawnWithFinger_t1005A142FB8FD4AAB1976B2AF6E24EEE8F869E20_CustomAttributesCacheGenerator_destroyIfBlocked(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSpawnWithFinger_t1005A142FB8FD4AAB1976B2AF6E24EEE8F869E20_CustomAttributesCacheGenerator_selectOnSpawn(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSpawnWithFinger_t1005A142FB8FD4AAB1976B2AF6E24EEE8F869E20_CustomAttributesCacheGenerator_selectWith(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSpawnWithFinger_t1005A142FB8FD4AAB1976B2AF6E24EEE8F869E20_CustomAttributesCacheGenerator_deselectOnUp(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSpawnWithFinger_t1005A142FB8FD4AAB1976B2AF6E24EEE8F869E20_CustomAttributesCacheGenerator_pixelOffset(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x50\x69\x78\x65\x6C\x4F\x66\x66\x73\x65\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSpawnWithFinger_t1005A142FB8FD4AAB1976B2AF6E24EEE8F869E20_CustomAttributesCacheGenerator_pixelScale(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x50\x69\x78\x65\x6C\x53\x63\x61\x6C\x65"), NULL);
	}
}
static void LeanSpawnWithFinger_t1005A142FB8FD4AAB1976B2AF6E24EEE8F869E20_CustomAttributesCacheGenerator_worldOffset(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x57\x6F\x72\x6C\x64\x4F\x66\x66\x73\x65\x74"), NULL);
	}
}
static void LeanSpawnWithFinger_t1005A142FB8FD4AAB1976B2AF6E24EEE8F869E20_CustomAttributesCacheGenerator_worldRelativeTo(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x57\x6F\x72\x6C\x64\x52\x65\x6C\x61\x74\x69\x76\x65\x54\x6F"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSpawnWithFinger_t1005A142FB8FD4AAB1976B2AF6E24EEE8F869E20_CustomAttributesCacheGenerator_fingerDatas(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSwipeEdge_tE0072AD209A8D99272265207F70250856118FF46_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x53\x77\x69\x70\x65\x20\x45\x64\x67\x65"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x53\x77\x69\x70\x65\x45\x64\x67\x65"), NULL);
	}
}
static void LeanSwipeEdge_tE0072AD209A8D99272265207F70250856118FF46_CustomAttributesCacheGenerator_left(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x66\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSwipeEdge_tE0072AD209A8D99272265207F70250856118FF46_CustomAttributesCacheGenerator_right(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x69\x67\x68\x74"), NULL);
	}
}
static void LeanSwipeEdge_tE0072AD209A8D99272265207F70250856118FF46_CustomAttributesCacheGenerator_bottom(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x42\x6F\x74\x74\x6F\x6D"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSwipeEdge_tE0072AD209A8D99272265207F70250856118FF46_CustomAttributesCacheGenerator_top(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x6F\x70"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSwipeEdge_tE0072AD209A8D99272265207F70250856118FF46_CustomAttributesCacheGenerator_angleThreshold(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x67\x6C\x65\x54\x68\x72\x65\x73\x68\x6F\x6C\x64"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[2];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 90.0f, NULL);
	}
}
static void LeanSwipeEdge_tE0072AD209A8D99272265207F70250856118FF46_CustomAttributesCacheGenerator_edgeThreshold(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x45\x64\x67\x65\x54\x68\x72\x65\x73\x68\x6F\x6C\x64"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSwipeEdge_tE0072AD209A8D99272265207F70250856118FF46_CustomAttributesCacheGenerator_onEdge(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanTwistCamera_t15BDB51368087222C7FD4095BB6C19E433DCC60F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x54\x77\x69\x73\x74\x43\x61\x6D\x65\x72\x61"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x54\x77\x69\x73\x74\x20\x43\x61\x6D\x65\x72\x61"), NULL);
	}
}
static void LeanTwistCamera_t15BDB51368087222C7FD4095BB6C19E433DCC60F_CustomAttributesCacheGenerator_damping(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x69\x6E\x67"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x65\x6E\x69\x6E\x67"), NULL);
	}
}
static void LeanTwistCamera_t15BDB51368087222C7FD4095BB6C19E433DCC60F_CustomAttributesCacheGenerator_relative(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x6C\x61\x74\x69\x76\x65"), NULL);
	}
}
static void LeanTwistCamera_t15BDB51368087222C7FD4095BB6C19E433DCC60F_CustomAttributesCacheGenerator_remainingTranslation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanTwistCamera_t15BDB51368087222C7FD4095BB6C19E433DCC60F_CustomAttributesCacheGenerator_remainingRotation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_LeanTouchPlus_AttributeGenerators[];
const CustomAttributesCacheGenerator g_LeanTouchPlus_AttributeGenerators[333] = 
{
	LeanDragColorMesh_tC3EEA8593FCA1935C16D6C86951A0819E2695BCE_CustomAttributesCacheGenerator,
	LeanDragDeformMesh_t67021E1B1C14EE2F7E9C6ED3BD2E1D636E4BB284_CustomAttributesCacheGenerator,
	LeanDragLine_t19C2B178D12E85AC6B607B08F32FD3003C3CEFED_CustomAttributesCacheGenerator,
	LeanDragSelect_t081AF0F20B2579E49E757E42A8C30B8A3964E99E_CustomAttributesCacheGenerator,
	LeanDragTranslateAlong_tACA31121D5367B039300B0EE76656C3CF9C4D30D_CustomAttributesCacheGenerator,
	LeanDragTranslateRigidbody_t5ECFD6EACD00232E073F81F7C8964BF1E983A74F_CustomAttributesCacheGenerator,
	LeanDragTranslateRigidbody2D_tF462EA4DFA66EB0B9587FBDD52294448D068454B_CustomAttributesCacheGenerator,
	LeanDrop_tEF0A60D70FDD5DE77D81E36E3BD9C15751EA9D89_CustomAttributesCacheGenerator,
	LeanDropCount_tC0C50670F8D2E1159EF63EA0E2DAC6430FDD974C_CustomAttributesCacheGenerator,
	LeanFingerDownCanvas_t83AFC349A56E4FA44F186C3B01769F1E7104EDBD_CustomAttributesCacheGenerator,
	LeanFingerFlick_t59E62BBD06F440FF7849B79C9CC62FE5E34658E2_CustomAttributesCacheGenerator,
	LeanFingerHeld_t2D73601702184B4AB62B43E6349F5932F51F5BE2_CustomAttributesCacheGenerator,
	LeanFingerTapExpired_t0D740BAF5108BFB6FD656EBCDBF1A817A8CF62F3_CustomAttributesCacheGenerator,
	LeanFingerTapQuick_tA073AB23792F860201989828ABFC075396160E51_CustomAttributesCacheGenerator,
	LeanFirstDown_t453C7DC8250866E727FDB970F8D1335D168BF2E9_CustomAttributesCacheGenerator,
	LeanFirstDownCanvas_t59F37BF3AE99DCBF95FD58FF13F236E6BC8853CB_CustomAttributesCacheGenerator,
	LeanGestureToggle_t54C824553167AC8C6C32BA47CF39CF8BF7425B24_CustomAttributesCacheGenerator,
	LeanLastUp_t6082DB2D4EBD9B7B9E5B7B11C50C3AC29AB6ABE3_CustomAttributesCacheGenerator,
	LeanLastUpCanvas_tB593233F7F8FDE6D6AE25D8651AC606DE9881954_CustomAttributesCacheGenerator,
	LeanManualFlick_tAC5969B0E49B99941EED2C714CE1AEBEA4E28992_CustomAttributesCacheGenerator,
	LeanManualSwipe_t4B6C0D082FBFFB43D1E2D54A4732467EED9EB11F_CustomAttributesCacheGenerator,
	LeanMouseWheel_t89CE9C4EA8C22590F4F0A664EFD7F75D87D3E632_CustomAttributesCacheGenerator,
	LeanMultiDirection_tE1CE9A8D954D423C3CBD37639359606B9E7CEC23_CustomAttributesCacheGenerator,
	LeanMultiDown_t87D39553D60C975F9E133450FA4B6AB74B4D24A2_CustomAttributesCacheGenerator,
	LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator,
	LeanMultiPinch_t8A25F40415662B66266BE64ACB1FD4145FE9CFC2_CustomAttributesCacheGenerator,
	LeanMultiPull_tBC5075E22D98EE889301850B0437AAC3AE7F02C6_CustomAttributesCacheGenerator,
	LeanMultiSwipe_t0A7F14E3CF4D31BB16D75ED5344828D32236ED91_CustomAttributesCacheGenerator,
	LeanMultiTap_t502D6311C2CEB8E7C54406823EF8025EA35F3373_CustomAttributesCacheGenerator,
	LeanMultiTwist_tFF0088194D2DBA58D36E5D677BD8C56C680AFA28_CustomAttributesCacheGenerator,
	LeanMultiUp_t8E4EE9C5AF6A43A7E39386AF32C34AED70B00E5A_CustomAttributesCacheGenerator,
	LeanMultiUpdate_t6F6FFADB9EB0F2FEE3F6C92A3903D8C3C78205AE_CustomAttributesCacheGenerator,
	LeanMultiUpdateCanvas_tDAD437108A5ECCE0B5429FFFA65D56CA2E3EC1AD_CustomAttributesCacheGenerator,
	LeanPick_t7AC38F818E2067FC14015F54A1396824461FDFC1_CustomAttributesCacheGenerator,
	LeanPickable_t39FF156752D677DBB205A7658738C50B8882A2C4_CustomAttributesCacheGenerator,
	LeanPinchCamera_t4BBC5706A596D0A7AE4271D24CDC56883D816A59_CustomAttributesCacheGenerator,
	LeanReplayFinger_tFF28D44D6E4F13B826B5FA95AFAD6B7F1F2D2C30_CustomAttributesCacheGenerator,
	LeanSelectableBlock_t4686257957B99475EF70DA5E48F053A6D32B9811_CustomAttributesCacheGenerator,
	LeanSelectableCenter_tC439B58BA06F9E94E614DFA8B38E3575478D8A53_CustomAttributesCacheGenerator,
	LeanSelectableCount_t940958E9FD3A349EB3EAAACE0616A4A8F36EC770_CustomAttributesCacheGenerator,
	LeanSelectableDial_t1D562EED60558A453D5A54084CB1EB573719AC3F_CustomAttributesCacheGenerator,
	LeanSelectableDragTorque_t5AA1E3CBFD81E3E8E983845CB55AFE2A17F62D33_CustomAttributesCacheGenerator,
	LeanSelectableDrop_tCB563847DF952D144B8C919EE0302F02C9D78D5C_CustomAttributesCacheGenerator,
	LeanSelectablePressureScale_t88C9F67057F1EB71A53EE205D83DEAAD0FFFB8C0_CustomAttributesCacheGenerator,
	LeanSelectableSelected_t7DED535D379F288839974F3DE7251E82A392AF56_CustomAttributesCacheGenerator,
	LeanSelectableTime_t9E8C411A0CEE294619CE3CF7777EE3D7405904A1_CustomAttributesCacheGenerator,
	LeanSelectionBox_tB91EB102282663B609ACF5C33D2F0949D525F02A_CustomAttributesCacheGenerator,
	LeanShape_t5D52B9E24475D82B0ED8B75B97D95A35AE845C2F_CustomAttributesCacheGenerator,
	LeanShapeDetector_t35E8FCF47AEFC36E057213DFD75779BEAAE4BF17_CustomAttributesCacheGenerator,
	LeanSpawnWithFinger_t1005A142FB8FD4AAB1976B2AF6E24EEE8F869E20_CustomAttributesCacheGenerator,
	LeanSwipeEdge_tE0072AD209A8D99272265207F70250856118FF46_CustomAttributesCacheGenerator,
	LeanTwistCamera_t15BDB51368087222C7FD4095BB6C19E433DCC60F_CustomAttributesCacheGenerator,
	LeanDragColorMesh_tC3EEA8593FCA1935C16D6C86951A0819E2695BCE_CustomAttributesCacheGenerator_paintColor,
	LeanDragColorMesh_tC3EEA8593FCA1935C16D6C86951A0819E2695BCE_CustomAttributesCacheGenerator__camera,
	LeanDragDeformMesh_t67021E1B1C14EE2F7E9C6ED3BD2E1D636E4BB284_CustomAttributesCacheGenerator_scaledRadius,
	LeanDragDeformMesh_t67021E1B1C14EE2F7E9C6ED3BD2E1D636E4BB284_CustomAttributesCacheGenerator_applyToMeshCollider,
	LeanDragDeformMesh_t67021E1B1C14EE2F7E9C6ED3BD2E1D636E4BB284_CustomAttributesCacheGenerator__camera,
	LeanDragLine_t19C2B178D12E85AC6B607B08F32FD3003C3CEFED_CustomAttributesCacheGenerator_widthScale,
	LeanDragLine_t19C2B178D12E85AC6B607B08F32FD3003C3CEFED_CustomAttributesCacheGenerator_lengthMin,
	LeanDragLine_t19C2B178D12E85AC6B607B08F32FD3003C3CEFED_CustomAttributesCacheGenerator_lengthMax,
	LeanDragLine_t19C2B178D12E85AC6B607B08F32FD3003C3CEFED_CustomAttributesCacheGenerator_startAtOrigin,
	LeanDragLine_t19C2B178D12E85AC6B607B08F32FD3003C3CEFED_CustomAttributesCacheGenerator_invert,
	LeanDragLine_t19C2B178D12E85AC6B607B08F32FD3003C3CEFED_CustomAttributesCacheGenerator_onReleasedFrom,
	LeanDragLine_t19C2B178D12E85AC6B607B08F32FD3003C3CEFED_CustomAttributesCacheGenerator_onReleasedTo,
	LeanDragLine_t19C2B178D12E85AC6B607B08F32FD3003C3CEFED_CustomAttributesCacheGenerator_onReleasedDelta,
	LeanDragLine_t19C2B178D12E85AC6B607B08F32FD3003C3CEFED_CustomAttributesCacheGenerator_onReleasedFromTo,
	LeanDragSelect_t081AF0F20B2579E49E757E42A8C30B8A3964E99E_CustomAttributesCacheGenerator_select,
	LeanDragSelect_t081AF0F20B2579E49E757E42A8C30B8A3964E99E_CustomAttributesCacheGenerator_requireNoSelectables,
	LeanDragSelect_t081AF0F20B2579E49E757E42A8C30B8A3964E99E_CustomAttributesCacheGenerator_requireInitialSelection,
	LeanDragSelect_t081AF0F20B2579E49E757E42A8C30B8A3964E99E_CustomAttributesCacheGenerator_deselectAllAtStart,
	LeanDragSelect_t081AF0F20B2579E49E757E42A8C30B8A3964E99E_CustomAttributesCacheGenerator_maximumSeparation,
	LeanDragTranslateAlong_tACA31121D5367B039300B0EE76656C3CF9C4D30D_CustomAttributesCacheGenerator_target,
	LeanDragTranslateAlong_tACA31121D5367B039300B0EE76656C3CF9C4D30D_CustomAttributesCacheGenerator_trackScreenPosition,
	LeanDragTranslateAlong_tACA31121D5367B039300B0EE76656C3CF9C4D30D_CustomAttributesCacheGenerator_damping,
	LeanDragTranslateAlong_tACA31121D5367B039300B0EE76656C3CF9C4D30D_CustomAttributesCacheGenerator_remainingDelta,
	LeanDragTranslateRigidbody_t5ECFD6EACD00232E073F81F7C8964BF1E983A74F_CustomAttributesCacheGenerator__camera,
	LeanDragTranslateRigidbody_t5ECFD6EACD00232E073F81F7C8964BF1E983A74F_CustomAttributesCacheGenerator_damping,
	LeanDragTranslateRigidbody2D_tF462EA4DFA66EB0B9587FBDD52294448D068454B_CustomAttributesCacheGenerator__camera,
	LeanDragTranslateRigidbody2D_tF462EA4DFA66EB0B9587FBDD52294448D068454B_CustomAttributesCacheGenerator_damping,
	LeanDrop_tEF0A60D70FDD5DE77D81E36E3BD9C15751EA9D89_CustomAttributesCacheGenerator_onDropped,
	LeanDropCount_tC0C50670F8D2E1159EF63EA0E2DAC6430FDD974C_CustomAttributesCacheGenerator_count,
	LeanDropCount_tC0C50670F8D2E1159EF63EA0E2DAC6430FDD974C_CustomAttributesCacheGenerator_onCount,
	LeanDropCount_tC0C50670F8D2E1159EF63EA0E2DAC6430FDD974C_CustomAttributesCacheGenerator_matchMin,
	LeanDropCount_tC0C50670F8D2E1159EF63EA0E2DAC6430FDD974C_CustomAttributesCacheGenerator_matchMax,
	LeanDropCount_tC0C50670F8D2E1159EF63EA0E2DAC6430FDD974C_CustomAttributesCacheGenerator_onMatch,
	LeanDropCount_tC0C50670F8D2E1159EF63EA0E2DAC6430FDD974C_CustomAttributesCacheGenerator_onUnmatch,
	LeanDropCount_tC0C50670F8D2E1159EF63EA0E2DAC6430FDD974C_CustomAttributesCacheGenerator_inside,
	LeanFingerDownCanvas_t83AFC349A56E4FA44F186C3B01769F1E7104EDBD_CustomAttributesCacheGenerator_ignoreStartedOverGui,
	LeanFingerDownCanvas_t83AFC349A56E4FA44F186C3B01769F1E7104EDBD_CustomAttributesCacheGenerator_requiredSelectable,
	LeanFingerDownCanvas_t83AFC349A56E4FA44F186C3B01769F1E7104EDBD_CustomAttributesCacheGenerator_onFinger,
	LeanFingerDownCanvas_t83AFC349A56E4FA44F186C3B01769F1E7104EDBD_CustomAttributesCacheGenerator_onWorld,
	LeanFingerFlick_t59E62BBD06F440FF7849B79C9CC62FE5E34658E2_CustomAttributesCacheGenerator_ignoreStartedOverGui,
	LeanFingerFlick_t59E62BBD06F440FF7849B79C9CC62FE5E34658E2_CustomAttributesCacheGenerator_ignoreIsOverGui,
	LeanFingerFlick_t59E62BBD06F440FF7849B79C9CC62FE5E34658E2_CustomAttributesCacheGenerator_requiredSelectable,
	LeanFingerFlick_t59E62BBD06F440FF7849B79C9CC62FE5E34658E2_CustomAttributesCacheGenerator_check,
	LeanFingerFlick_t59E62BBD06F440FF7849B79C9CC62FE5E34658E2_CustomAttributesCacheGenerator_fingerDatas,
	LeanFingerHeld_t2D73601702184B4AB62B43E6349F5932F51F5BE2_CustomAttributesCacheGenerator_ignoreStartedOverGui,
	LeanFingerHeld_t2D73601702184B4AB62B43E6349F5932F51F5BE2_CustomAttributesCacheGenerator_ignoreIsOverGui,
	LeanFingerHeld_t2D73601702184B4AB62B43E6349F5932F51F5BE2_CustomAttributesCacheGenerator_requiredSelectable,
	LeanFingerHeld_t2D73601702184B4AB62B43E6349F5932F51F5BE2_CustomAttributesCacheGenerator_minimumAge,
	LeanFingerHeld_t2D73601702184B4AB62B43E6349F5932F51F5BE2_CustomAttributesCacheGenerator_maximumMovement,
	LeanFingerHeld_t2D73601702184B4AB62B43E6349F5932F51F5BE2_CustomAttributesCacheGenerator_onFingerDown,
	LeanFingerHeld_t2D73601702184B4AB62B43E6349F5932F51F5BE2_CustomAttributesCacheGenerator_onFingerUpdate,
	LeanFingerHeld_t2D73601702184B4AB62B43E6349F5932F51F5BE2_CustomAttributesCacheGenerator_onFingerUp,
	LeanFingerHeld_t2D73601702184B4AB62B43E6349F5932F51F5BE2_CustomAttributesCacheGenerator_onWorldDown,
	LeanFingerHeld_t2D73601702184B4AB62B43E6349F5932F51F5BE2_CustomAttributesCacheGenerator_onWorldUpdate,
	LeanFingerHeld_t2D73601702184B4AB62B43E6349F5932F51F5BE2_CustomAttributesCacheGenerator_onWorldUp,
	LeanFingerHeld_t2D73601702184B4AB62B43E6349F5932F51F5BE2_CustomAttributesCacheGenerator_fingerDatas,
	LeanFingerTapExpired_t0D740BAF5108BFB6FD656EBCDBF1A817A8CF62F3_CustomAttributesCacheGenerator_ignoreStartedOverGui,
	LeanFingerTapExpired_t0D740BAF5108BFB6FD656EBCDBF1A817A8CF62F3_CustomAttributesCacheGenerator_ignoreIsOverGui,
	LeanFingerTapExpired_t0D740BAF5108BFB6FD656EBCDBF1A817A8CF62F3_CustomAttributesCacheGenerator_requiredSelectable,
	LeanFingerTapExpired_t0D740BAF5108BFB6FD656EBCDBF1A817A8CF62F3_CustomAttributesCacheGenerator_requiredTapCount,
	LeanFingerTapExpired_t0D740BAF5108BFB6FD656EBCDBF1A817A8CF62F3_CustomAttributesCacheGenerator_requiredTapInterval,
	LeanFingerTapExpired_t0D740BAF5108BFB6FD656EBCDBF1A817A8CF62F3_CustomAttributesCacheGenerator_onFinger,
	LeanFingerTapExpired_t0D740BAF5108BFB6FD656EBCDBF1A817A8CF62F3_CustomAttributesCacheGenerator_onCount,
	LeanFingerTapExpired_t0D740BAF5108BFB6FD656EBCDBF1A817A8CF62F3_CustomAttributesCacheGenerator_onWorld,
	LeanFingerTapQuick_tA073AB23792F860201989828ABFC075396160E51_CustomAttributesCacheGenerator_ignoreStartedOverGui,
	LeanFingerTapQuick_tA073AB23792F860201989828ABFC075396160E51_CustomAttributesCacheGenerator_requiredSelectable,
	LeanFingerTapQuick_tA073AB23792F860201989828ABFC075396160E51_CustomAttributesCacheGenerator_requiredTapCount,
	LeanFingerTapQuick_tA073AB23792F860201989828ABFC075396160E51_CustomAttributesCacheGenerator_onFinger,
	LeanFingerTapQuick_tA073AB23792F860201989828ABFC075396160E51_CustomAttributesCacheGenerator_onWorld,
	LeanFingerTapQuick_tA073AB23792F860201989828ABFC075396160E51_CustomAttributesCacheGenerator_onScreen,
	LeanFirstDown_t453C7DC8250866E727FDB970F8D1335D168BF2E9_CustomAttributesCacheGenerator_ignoreStartedOverGui,
	LeanFirstDown_t453C7DC8250866E727FDB970F8D1335D168BF2E9_CustomAttributesCacheGenerator_requiredSelectable,
	LeanFirstDown_t453C7DC8250866E727FDB970F8D1335D168BF2E9_CustomAttributesCacheGenerator_onFinger,
	LeanFirstDown_t453C7DC8250866E727FDB970F8D1335D168BF2E9_CustomAttributesCacheGenerator_onWorld,
	LeanFirstDown_t453C7DC8250866E727FDB970F8D1335D168BF2E9_CustomAttributesCacheGenerator_onScreen,
	LeanGestureToggle_t54C824553167AC8C6C32BA47CF39CF8BF7425B24_CustomAttributesCacheGenerator_enableWithoutIsolation,
	LeanGestureToggle_t54C824553167AC8C6C32BA47CF39CF8BF7425B24_CustomAttributesCacheGenerator_dragComponent,
	LeanGestureToggle_t54C824553167AC8C6C32BA47CF39CF8BF7425B24_CustomAttributesCacheGenerator_dragThreshold,
	LeanGestureToggle_t54C824553167AC8C6C32BA47CF39CF8BF7425B24_CustomAttributesCacheGenerator_pinchComponent,
	LeanGestureToggle_t54C824553167AC8C6C32BA47CF39CF8BF7425B24_CustomAttributesCacheGenerator_pinchThreshold,
	LeanGestureToggle_t54C824553167AC8C6C32BA47CF39CF8BF7425B24_CustomAttributesCacheGenerator_twistComponent,
	LeanGestureToggle_t54C824553167AC8C6C32BA47CF39CF8BF7425B24_CustomAttributesCacheGenerator_twistThreshold,
	LeanGestureToggle_t54C824553167AC8C6C32BA47CF39CF8BF7425B24_CustomAttributesCacheGenerator_twistWithPinch,
	LeanLastUp_t6082DB2D4EBD9B7B9E5B7B11C50C3AC29AB6ABE3_CustomAttributesCacheGenerator_ignoreStartedOverGui,
	LeanLastUp_t6082DB2D4EBD9B7B9E5B7B11C50C3AC29AB6ABE3_CustomAttributesCacheGenerator_requiredSelectable,
	LeanLastUp_t6082DB2D4EBD9B7B9E5B7B11C50C3AC29AB6ABE3_CustomAttributesCacheGenerator_onFinger,
	LeanLastUp_t6082DB2D4EBD9B7B9E5B7B11C50C3AC29AB6ABE3_CustomAttributesCacheGenerator_onWorld,
	LeanLastUp_t6082DB2D4EBD9B7B9E5B7B11C50C3AC29AB6ABE3_CustomAttributesCacheGenerator_onScreen,
	LeanManualFlick_tAC5969B0E49B99941EED2C714CE1AEBEA4E28992_CustomAttributesCacheGenerator_ignoreIsOverGui,
	LeanManualFlick_tAC5969B0E49B99941EED2C714CE1AEBEA4E28992_CustomAttributesCacheGenerator_requiredSelectable,
	LeanManualFlick_tAC5969B0E49B99941EED2C714CE1AEBEA4E28992_CustomAttributesCacheGenerator_check,
	LeanManualFlick_tAC5969B0E49B99941EED2C714CE1AEBEA4E28992_CustomAttributesCacheGenerator_fingerDatas,
	LeanManualSwipe_t4B6C0D082FBFFB43D1E2D54A4732467EED9EB11F_CustomAttributesCacheGenerator_ignoreIsOverGui,
	LeanManualSwipe_t4B6C0D082FBFFB43D1E2D54A4732467EED9EB11F_CustomAttributesCacheGenerator_requiredSelectable,
	LeanMouseWheel_t89CE9C4EA8C22590F4F0A664EFD7F75D87D3E632_CustomAttributesCacheGenerator_requiredSelectable,
	LeanMouseWheel_t89CE9C4EA8C22590F4F0A664EFD7F75D87D3E632_CustomAttributesCacheGenerator_requiredMouseButton,
	LeanMouseWheel_t89CE9C4EA8C22590F4F0A664EFD7F75D87D3E632_CustomAttributesCacheGenerator_modify,
	LeanMouseWheel_t89CE9C4EA8C22590F4F0A664EFD7F75D87D3E632_CustomAttributesCacheGenerator_multiplier,
	LeanMouseWheel_t89CE9C4EA8C22590F4F0A664EFD7F75D87D3E632_CustomAttributesCacheGenerator_coordinate,
	LeanMouseWheel_t89CE9C4EA8C22590F4F0A664EFD7F75D87D3E632_CustomAttributesCacheGenerator_onDelta,
	LeanMultiDirection_tE1CE9A8D954D423C3CBD37639359606B9E7CEC23_CustomAttributesCacheGenerator_ignoreIfStatic,
	LeanMultiDirection_tE1CE9A8D954D423C3CBD37639359606B9E7CEC23_CustomAttributesCacheGenerator_angle,
	LeanMultiDirection_tE1CE9A8D954D423C3CBD37639359606B9E7CEC23_CustomAttributesCacheGenerator_oneWay,
	LeanMultiDirection_tE1CE9A8D954D423C3CBD37639359606B9E7CEC23_CustomAttributesCacheGenerator_coordinate,
	LeanMultiDirection_tE1CE9A8D954D423C3CBD37639359606B9E7CEC23_CustomAttributesCacheGenerator_multiplier,
	LeanMultiDirection_tE1CE9A8D954D423C3CBD37639359606B9E7CEC23_CustomAttributesCacheGenerator_onDelta,
	LeanMultiDown_t87D39553D60C975F9E133450FA4B6AB74B4D24A2_CustomAttributesCacheGenerator_ignoreStartedOverGui,
	LeanMultiDown_t87D39553D60C975F9E133450FA4B6AB74B4D24A2_CustomAttributesCacheGenerator_requiredSelectable,
	LeanMultiDown_t87D39553D60C975F9E133450FA4B6AB74B4D24A2_CustomAttributesCacheGenerator_requiredCount,
	LeanMultiDown_t87D39553D60C975F9E133450FA4B6AB74B4D24A2_CustomAttributesCacheGenerator_onFingers,
	LeanMultiDown_t87D39553D60C975F9E133450FA4B6AB74B4D24A2_CustomAttributesCacheGenerator_onWorld,
	LeanMultiDown_t87D39553D60C975F9E133450FA4B6AB74B4D24A2_CustomAttributesCacheGenerator_onScreen,
	LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_ignoreStartedOverGui,
	LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_ignoreIsOverGui,
	LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_requiredSelectable,
	LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_requiredCount,
	LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_minimumAge,
	LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_maximumMovement,
	LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_onFingersDown,
	LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_onFingersUpdate,
	LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_onFingersUp,
	LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_onWorldDown,
	LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_onWorldUpdate,
	LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_onWorldUp,
	LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_onScreenDown,
	LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_onScreenUpdate,
	LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_onScreenUp,
	LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_fingers,
	LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_fingerDatas,
	LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_held,
	LeanMultiHeld_tDCB02135AA3508E7E43CFAF45707B6DE515EF780_CustomAttributesCacheGenerator_duration,
	LeanMultiPinch_t8A25F40415662B66266BE64ACB1FD4145FE9CFC2_CustomAttributesCacheGenerator_ignoreIfStatic,
	LeanMultiPinch_t8A25F40415662B66266BE64ACB1FD4145FE9CFC2_CustomAttributesCacheGenerator_coordinate,
	LeanMultiPinch_t8A25F40415662B66266BE64ACB1FD4145FE9CFC2_CustomAttributesCacheGenerator_multiplier,
	LeanMultiPinch_t8A25F40415662B66266BE64ACB1FD4145FE9CFC2_CustomAttributesCacheGenerator_onPinch,
	LeanMultiPull_tBC5075E22D98EE889301850B0437AAC3AE7F02C6_CustomAttributesCacheGenerator_coordinate,
	LeanMultiPull_tBC5075E22D98EE889301850B0437AAC3AE7F02C6_CustomAttributesCacheGenerator_multiplier,
	LeanMultiPull_tBC5075E22D98EE889301850B0437AAC3AE7F02C6_CustomAttributesCacheGenerator_scaleByTime,
	LeanMultiPull_tBC5075E22D98EE889301850B0437AAC3AE7F02C6_CustomAttributesCacheGenerator_onVector,
	LeanMultiPull_tBC5075E22D98EE889301850B0437AAC3AE7F02C6_CustomAttributesCacheGenerator_onDistance,
	LeanMultiPull_tBC5075E22D98EE889301850B0437AAC3AE7F02C6_CustomAttributesCacheGenerator_onWorldFrom,
	LeanMultiPull_tBC5075E22D98EE889301850B0437AAC3AE7F02C6_CustomAttributesCacheGenerator_onWorldTo,
	LeanMultiPull_tBC5075E22D98EE889301850B0437AAC3AE7F02C6_CustomAttributesCacheGenerator_onWorldDelta,
	LeanMultiPull_tBC5075E22D98EE889301850B0437AAC3AE7F02C6_CustomAttributesCacheGenerator_onWorldFromTo,
	LeanMultiSwipe_t0A7F14E3CF4D31BB16D75ED5344828D32236ED91_CustomAttributesCacheGenerator_scaledDistanceThreshold,
	LeanMultiSwipe_t0A7F14E3CF4D31BB16D75ED5344828D32236ED91_CustomAttributesCacheGenerator_parallelAngleThreshold,
	LeanMultiSwipe_t0A7F14E3CF4D31BB16D75ED5344828D32236ED91_CustomAttributesCacheGenerator_pinchScaledDistanceThreshold,
	LeanMultiSwipe_t0A7F14E3CF4D31BB16D75ED5344828D32236ED91_CustomAttributesCacheGenerator_onFingers,
	LeanMultiSwipe_t0A7F14E3CF4D31BB16D75ED5344828D32236ED91_CustomAttributesCacheGenerator_onSwipeParallel,
	LeanMultiSwipe_t0A7F14E3CF4D31BB16D75ED5344828D32236ED91_CustomAttributesCacheGenerator_onSwipeIn,
	LeanMultiSwipe_t0A7F14E3CF4D31BB16D75ED5344828D32236ED91_CustomAttributesCacheGenerator_onSwipeOut,
	LeanMultiTap_t502D6311C2CEB8E7C54406823EF8025EA35F3373_CustomAttributesCacheGenerator_onTap,
	LeanMultiTap_t502D6311C2CEB8E7C54406823EF8025EA35F3373_CustomAttributesCacheGenerator_onCount,
	LeanMultiTap_t502D6311C2CEB8E7C54406823EF8025EA35F3373_CustomAttributesCacheGenerator_onHighest,
	LeanMultiTap_t502D6311C2CEB8E7C54406823EF8025EA35F3373_CustomAttributesCacheGenerator_onCountHighest,
	LeanMultiTwist_tFF0088194D2DBA58D36E5D677BD8C56C680AFA28_CustomAttributesCacheGenerator_ignoreIfStatic,
	LeanMultiTwist_tFF0088194D2DBA58D36E5D677BD8C56C680AFA28_CustomAttributesCacheGenerator_oneFinger,
	LeanMultiTwist_tFF0088194D2DBA58D36E5D677BD8C56C680AFA28_CustomAttributesCacheGenerator_onTwistDegrees,
	LeanMultiUp_t8E4EE9C5AF6A43A7E39386AF32C34AED70B00E5A_CustomAttributesCacheGenerator_onFinger,
	LeanMultiUp_t8E4EE9C5AF6A43A7E39386AF32C34AED70B00E5A_CustomAttributesCacheGenerator_onWorld,
	LeanMultiUp_t8E4EE9C5AF6A43A7E39386AF32C34AED70B00E5A_CustomAttributesCacheGenerator_onScreen,
	LeanMultiUpdate_t6F6FFADB9EB0F2FEE3F6C92A3903D8C3C78205AE_CustomAttributesCacheGenerator_ignoreIfStatic,
	LeanMultiUpdate_t6F6FFADB9EB0F2FEE3F6C92A3903D8C3C78205AE_CustomAttributesCacheGenerator_onFingers,
	LeanMultiUpdate_t6F6FFADB9EB0F2FEE3F6C92A3903D8C3C78205AE_CustomAttributesCacheGenerator_coordinate,
	LeanMultiUpdate_t6F6FFADB9EB0F2FEE3F6C92A3903D8C3C78205AE_CustomAttributesCacheGenerator_multiplier,
	LeanMultiUpdate_t6F6FFADB9EB0F2FEE3F6C92A3903D8C3C78205AE_CustomAttributesCacheGenerator_onDelta,
	LeanMultiUpdate_t6F6FFADB9EB0F2FEE3F6C92A3903D8C3C78205AE_CustomAttributesCacheGenerator_onDistance,
	LeanMultiUpdate_t6F6FFADB9EB0F2FEE3F6C92A3903D8C3C78205AE_CustomAttributesCacheGenerator_onWorldFrom,
	LeanMultiUpdate_t6F6FFADB9EB0F2FEE3F6C92A3903D8C3C78205AE_CustomAttributesCacheGenerator_onWorldTo,
	LeanMultiUpdate_t6F6FFADB9EB0F2FEE3F6C92A3903D8C3C78205AE_CustomAttributesCacheGenerator_onWorldDelta,
	LeanMultiUpdate_t6F6FFADB9EB0F2FEE3F6C92A3903D8C3C78205AE_CustomAttributesCacheGenerator_onWorldFromTo,
	LeanMultiUpdateCanvas_tDAD437108A5ECCE0B5429FFFA65D56CA2E3EC1AD_CustomAttributesCacheGenerator_ignoreIfOff,
	LeanMultiUpdateCanvas_tDAD437108A5ECCE0B5429FFFA65D56CA2E3EC1AD_CustomAttributesCacheGenerator_onFingers,
	LeanMultiUpdateCanvas_tDAD437108A5ECCE0B5429FFFA65D56CA2E3EC1AD_CustomAttributesCacheGenerator_onWorld,
	LeanPick_t7AC38F818E2067FC14015F54A1396824461FDFC1_CustomAttributesCacheGenerator_requiredTag,
	LeanPick_t7AC38F818E2067FC14015F54A1396824461FDFC1_CustomAttributesCacheGenerator_onPickable,
	LeanPickable_t39FF156752D677DBB205A7658738C50B8882A2C4_CustomAttributesCacheGenerator_onFinger,
	LeanPickable_t39FF156752D677DBB205A7658738C50B8882A2C4_CustomAttributesCacheGenerator_onWorld,
	LeanPickable_t39FF156752D677DBB205A7658738C50B8882A2C4_CustomAttributesCacheGenerator_onScreen,
	LeanPinchCamera_t4BBC5706A596D0A7AE4271D24CDC56883D816A59_CustomAttributesCacheGenerator__camera,
	LeanPinchCamera_t4BBC5706A596D0A7AE4271D24CDC56883D816A59_CustomAttributesCacheGenerator_zoom,
	LeanPinchCamera_t4BBC5706A596D0A7AE4271D24CDC56883D816A59_CustomAttributesCacheGenerator_damping,
	LeanPinchCamera_t4BBC5706A596D0A7AE4271D24CDC56883D816A59_CustomAttributesCacheGenerator_clamp,
	LeanPinchCamera_t4BBC5706A596D0A7AE4271D24CDC56883D816A59_CustomAttributesCacheGenerator_clampMin,
	LeanPinchCamera_t4BBC5706A596D0A7AE4271D24CDC56883D816A59_CustomAttributesCacheGenerator_clampMax,
	LeanPinchCamera_t4BBC5706A596D0A7AE4271D24CDC56883D816A59_CustomAttributesCacheGenerator_relative,
	LeanPinchCamera_t4BBC5706A596D0A7AE4271D24CDC56883D816A59_CustomAttributesCacheGenerator_ignoreZ,
	LeanPinchCamera_t4BBC5706A596D0A7AE4271D24CDC56883D816A59_CustomAttributesCacheGenerator_currentZoom,
	LeanPinchCamera_t4BBC5706A596D0A7AE4271D24CDC56883D816A59_CustomAttributesCacheGenerator_remainingTranslation,
	LeanReplayFinger_tFF28D44D6E4F13B826B5FA95AFAD6B7F1F2D2C30_CustomAttributesCacheGenerator_cursor,
	LeanReplayFinger_tFF28D44D6E4F13B826B5FA95AFAD6B7F1F2D2C30_CustomAttributesCacheGenerator_playing,
	LeanReplayFinger_tFF28D44D6E4F13B826B5FA95AFAD6B7F1F2D2C30_CustomAttributesCacheGenerator_playTime,
	LeanSelectableBlock_t4686257957B99475EF70DA5E48F053A6D32B9811_CustomAttributesCacheGenerator_x,
	LeanSelectableBlock_t4686257957B99475EF70DA5E48F053A6D32B9811_CustomAttributesCacheGenerator_y,
	LeanSelectableBlock_t4686257957B99475EF70DA5E48F053A6D32B9811_CustomAttributesCacheGenerator_blockSize,
	LeanSelectableBlock_t4686257957B99475EF70DA5E48F053A6D32B9811_CustomAttributesCacheGenerator_deselectOnSwap,
	LeanSelectableBlock_t4686257957B99475EF70DA5E48F053A6D32B9811_CustomAttributesCacheGenerator_damping,
	LeanSelectableCenter_tC439B58BA06F9E94E614DFA8B38E3575478D8A53_CustomAttributesCacheGenerator_onPosition,
	LeanSelectableCount_t940958E9FD3A349EB3EAAACE0616A4A8F36EC770_CustomAttributesCacheGenerator_count,
	LeanSelectableCount_t940958E9FD3A349EB3EAAACE0616A4A8F36EC770_CustomAttributesCacheGenerator_onCount,
	LeanSelectableCount_t940958E9FD3A349EB3EAAACE0616A4A8F36EC770_CustomAttributesCacheGenerator_matchMin,
	LeanSelectableCount_t940958E9FD3A349EB3EAAACE0616A4A8F36EC770_CustomAttributesCacheGenerator_matchMax,
	LeanSelectableCount_t940958E9FD3A349EB3EAAACE0616A4A8F36EC770_CustomAttributesCacheGenerator_onMatch,
	LeanSelectableCount_t940958E9FD3A349EB3EAAACE0616A4A8F36EC770_CustomAttributesCacheGenerator_onUnmatch,
	LeanSelectableCount_t940958E9FD3A349EB3EAAACE0616A4A8F36EC770_CustomAttributesCacheGenerator_inside,
	LeanSelectableDial_t1D562EED60558A453D5A54084CB1EB573719AC3F_CustomAttributesCacheGenerator__camera,
	LeanSelectableDial_t1D562EED60558A453D5A54084CB1EB573719AC3F_CustomAttributesCacheGenerator_tilt,
	LeanSelectableDial_t1D562EED60558A453D5A54084CB1EB573719AC3F_CustomAttributesCacheGenerator_axis,
	LeanSelectableDial_t1D562EED60558A453D5A54084CB1EB573719AC3F_CustomAttributesCacheGenerator_angle,
	LeanSelectableDial_t1D562EED60558A453D5A54084CB1EB573719AC3F_CustomAttributesCacheGenerator_clamp,
	LeanSelectableDial_t1D562EED60558A453D5A54084CB1EB573719AC3F_CustomAttributesCacheGenerator_clampMin,
	LeanSelectableDial_t1D562EED60558A453D5A54084CB1EB573719AC3F_CustomAttributesCacheGenerator_clampMax,
	LeanSelectableDial_t1D562EED60558A453D5A54084CB1EB573719AC3F_CustomAttributesCacheGenerator_triggers,
	LeanSelectableDial_t1D562EED60558A453D5A54084CB1EB573719AC3F_CustomAttributesCacheGenerator_onAngleChanged,
	Trigger_tCDE0F6503E25FA1F39A693069031F6D3CA0BBC2A_CustomAttributesCacheGenerator_Angle,
	Trigger_tCDE0F6503E25FA1F39A693069031F6D3CA0BBC2A_CustomAttributesCacheGenerator_Arc,
	Trigger_tCDE0F6503E25FA1F39A693069031F6D3CA0BBC2A_CustomAttributesCacheGenerator_Inside,
	Trigger_tCDE0F6503E25FA1F39A693069031F6D3CA0BBC2A_CustomAttributesCacheGenerator_onEnter,
	Trigger_tCDE0F6503E25FA1F39A693069031F6D3CA0BBC2A_CustomAttributesCacheGenerator_onExit,
	LeanSelectableDragTorque_t5AA1E3CBFD81E3E8E983845CB55AFE2A17F62D33_CustomAttributesCacheGenerator__camera,
	LeanSelectableDragTorque_t5AA1E3CBFD81E3E8E983845CB55AFE2A17F62D33_CustomAttributesCacheGenerator_force,
	LeanSelectableDrop_tCB563847DF952D144B8C919EE0302F02C9D78D5C_CustomAttributesCacheGenerator_ignore,
	LeanSelectableDrop_tCB563847DF952D144B8C919EE0302F02C9D78D5C_CustomAttributesCacheGenerator_onGameObject,
	LeanSelectableDrop_tCB563847DF952D144B8C919EE0302F02C9D78D5C_CustomAttributesCacheGenerator_onDropHandler,
	LeanSelectablePressureScale_t88C9F67057F1EB71A53EE205D83DEAAD0FFFB8C0_CustomAttributesCacheGenerator_baseScale,
	LeanSelectablePressureScale_t88C9F67057F1EB71A53EE205D83DEAAD0FFFB8C0_CustomAttributesCacheGenerator_pressureMultiplier,
	LeanSelectablePressureScale_t88C9F67057F1EB71A53EE205D83DEAAD0FFFB8C0_CustomAttributesCacheGenerator_pressureClamp,
	LeanSelectableSelected_t7DED535D379F288839974F3DE7251E82A392AF56_CustomAttributesCacheGenerator_threshold,
	LeanSelectableSelected_t7DED535D379F288839974F3DE7251E82A392AF56_CustomAttributesCacheGenerator_reset,
	LeanSelectableSelected_t7DED535D379F288839974F3DE7251E82A392AF56_CustomAttributesCacheGenerator_rawSelection,
	LeanSelectableSelected_t7DED535D379F288839974F3DE7251E82A392AF56_CustomAttributesCacheGenerator_requireFinger,
	LeanSelectableSelected_t7DED535D379F288839974F3DE7251E82A392AF56_CustomAttributesCacheGenerator_onSelectableDown,
	LeanSelectableSelected_t7DED535D379F288839974F3DE7251E82A392AF56_CustomAttributesCacheGenerator_onSelectableUpdate,
	LeanSelectableSelected_t7DED535D379F288839974F3DE7251E82A392AF56_CustomAttributesCacheGenerator_onSelectableUp,
	LeanSelectableSelected_t7DED535D379F288839974F3DE7251E82A392AF56_CustomAttributesCacheGenerator_lastSet,
	LeanSelectableSelected_t7DED535D379F288839974F3DE7251E82A392AF56_CustomAttributesCacheGenerator_seconds,
	LeanSelectableTime_t9E8C411A0CEE294619CE3CF7777EE3D7405904A1_CustomAttributesCacheGenerator_send,
	LeanSelectableTime_t9E8C411A0CEE294619CE3CF7777EE3D7405904A1_CustomAttributesCacheGenerator_onSeconds,
	LeanSelectableTime_t9E8C411A0CEE294619CE3CF7777EE3D7405904A1_CustomAttributesCacheGenerator_seconds,
	LeanSelectionBox_tB91EB102282663B609ACF5C33D2F0949D525F02A_CustomAttributesCacheGenerator__camera,
	LeanSelectionBox_tB91EB102282663B609ACF5C33D2F0949D525F02A_CustomAttributesCacheGenerator_ignoreIfStartedOverGui,
	LeanSelectionBox_tB91EB102282663B609ACF5C33D2F0949D525F02A_CustomAttributesCacheGenerator_prefab,
	LeanSelectionBox_tB91EB102282663B609ACF5C33D2F0949D525F02A_CustomAttributesCacheGenerator_root,
	LeanSelectionBox_tB91EB102282663B609ACF5C33D2F0949D525F02A_CustomAttributesCacheGenerator_select,
	LeanSelectionBox_tB91EB102282663B609ACF5C33D2F0949D525F02A_CustomAttributesCacheGenerator_requiredLayers,
	LeanSelectionBox_tB91EB102282663B609ACF5C33D2F0949D525F02A_CustomAttributesCacheGenerator_requiredTags,
	LeanShape_t5D52B9E24475D82B0ED8B75B97D95A35AE845C2F_CustomAttributesCacheGenerator_connectEnds,
	LeanShape_t5D52B9E24475D82B0ED8B75B97D95A35AE845C2F_CustomAttributesCacheGenerator_visual,
	LeanShape_t5D52B9E24475D82B0ED8B75B97D95A35AE845C2F_CustomAttributesCacheGenerator_points,
	LeanShapeDetector_t35E8FCF47AEFC36E057213DFD75779BEAAE4BF17_CustomAttributesCacheGenerator_shape,
	LeanShapeDetector_t35E8FCF47AEFC36E057213DFD75779BEAAE4BF17_CustomAttributesCacheGenerator_stepThreshold,
	LeanShapeDetector_t35E8FCF47AEFC36E057213DFD75779BEAAE4BF17_CustomAttributesCacheGenerator_distanceThreshold,
	LeanShapeDetector_t35E8FCF47AEFC36E057213DFD75779BEAAE4BF17_CustomAttributesCacheGenerator_errorThreshold,
	LeanShapeDetector_t35E8FCF47AEFC36E057213DFD75779BEAAE4BF17_CustomAttributesCacheGenerator_minimumPoints,
	LeanShapeDetector_t35E8FCF47AEFC36E057213DFD75779BEAAE4BF17_CustomAttributesCacheGenerator_direction,
	LeanShapeDetector_t35E8FCF47AEFC36E057213DFD75779BEAAE4BF17_CustomAttributesCacheGenerator_onDetected,
	LeanSpawnWithFinger_t1005A142FB8FD4AAB1976B2AF6E24EEE8F869E20_CustomAttributesCacheGenerator_prefab,
	LeanSpawnWithFinger_t1005A142FB8FD4AAB1976B2AF6E24EEE8F869E20_CustomAttributesCacheGenerator_rotateTo,
	LeanSpawnWithFinger_t1005A142FB8FD4AAB1976B2AF6E24EEE8F869E20_CustomAttributesCacheGenerator_dragAfterSpawn,
	LeanSpawnWithFinger_t1005A142FB8FD4AAB1976B2AF6E24EEE8F869E20_CustomAttributesCacheGenerator_destroyIfBlocked,
	LeanSpawnWithFinger_t1005A142FB8FD4AAB1976B2AF6E24EEE8F869E20_CustomAttributesCacheGenerator_selectOnSpawn,
	LeanSpawnWithFinger_t1005A142FB8FD4AAB1976B2AF6E24EEE8F869E20_CustomAttributesCacheGenerator_selectWith,
	LeanSpawnWithFinger_t1005A142FB8FD4AAB1976B2AF6E24EEE8F869E20_CustomAttributesCacheGenerator_deselectOnUp,
	LeanSpawnWithFinger_t1005A142FB8FD4AAB1976B2AF6E24EEE8F869E20_CustomAttributesCacheGenerator_pixelOffset,
	LeanSpawnWithFinger_t1005A142FB8FD4AAB1976B2AF6E24EEE8F869E20_CustomAttributesCacheGenerator_pixelScale,
	LeanSpawnWithFinger_t1005A142FB8FD4AAB1976B2AF6E24EEE8F869E20_CustomAttributesCacheGenerator_worldOffset,
	LeanSpawnWithFinger_t1005A142FB8FD4AAB1976B2AF6E24EEE8F869E20_CustomAttributesCacheGenerator_worldRelativeTo,
	LeanSpawnWithFinger_t1005A142FB8FD4AAB1976B2AF6E24EEE8F869E20_CustomAttributesCacheGenerator_fingerDatas,
	LeanSwipeEdge_tE0072AD209A8D99272265207F70250856118FF46_CustomAttributesCacheGenerator_left,
	LeanSwipeEdge_tE0072AD209A8D99272265207F70250856118FF46_CustomAttributesCacheGenerator_right,
	LeanSwipeEdge_tE0072AD209A8D99272265207F70250856118FF46_CustomAttributesCacheGenerator_bottom,
	LeanSwipeEdge_tE0072AD209A8D99272265207F70250856118FF46_CustomAttributesCacheGenerator_top,
	LeanSwipeEdge_tE0072AD209A8D99272265207F70250856118FF46_CustomAttributesCacheGenerator_angleThreshold,
	LeanSwipeEdge_tE0072AD209A8D99272265207F70250856118FF46_CustomAttributesCacheGenerator_edgeThreshold,
	LeanSwipeEdge_tE0072AD209A8D99272265207F70250856118FF46_CustomAttributesCacheGenerator_onEdge,
	LeanTwistCamera_t15BDB51368087222C7FD4095BB6C19E433DCC60F_CustomAttributesCacheGenerator_damping,
	LeanTwistCamera_t15BDB51368087222C7FD4095BB6C19E433DCC60F_CustomAttributesCacheGenerator_relative,
	LeanTwistCamera_t15BDB51368087222C7FD4095BB6C19E433DCC60F_CustomAttributesCacheGenerator_remainingTranslation,
	LeanTwistCamera_t15BDB51368087222C7FD4095BB6C19E433DCC60F_CustomAttributesCacheGenerator_remainingRotation,
	LeanSelectableCenter_tC439B58BA06F9E94E614DFA8B38E3575478D8A53_CustomAttributesCacheGenerator_LeanSelectableCenter_Calculate_m30CCA82E5C0E2B6C838A6DBA5C43DDD627F0AF4C,
	LeanTouchPlus_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
