﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9;
// UnityEngine.Events.UnityEvent`1<System.Collections.Generic.List`1<Lean.Touch.LeanFinger>>
struct UnityEvent_1_t4B73E8A5E7FCB83B3A9ACFDBB9932EF0B4E0B39A;
// UnityEngine.Events.UnityEvent`1<UnityEngine.GameObject>
struct UnityEvent_1_t1DC2DB931FE9E53AEC9A04F4DE9B4F7B469BC78E;
// UnityEngine.Events.UnityEvent`1<Lean.Touch.IDropHandler>
struct UnityEvent_1_t2F5AE12EB5154F02C654F49C1DFD43714173A0C3;
// UnityEngine.Events.UnityEvent`1<System.Int32>
struct UnityEvent_1_tB235B5DAD099AC425DC059D10DEB8B97A35E2BBF;
// UnityEngine.Events.UnityEvent`1<Lean.Touch.LeanFinger>
struct UnityEvent_1_t88B7ACE02A62AB7E41FF842AFDDDFA5860884BDA;
// UnityEngine.Events.UnityEvent`1<Lean.Touch.LeanPickable>
struct UnityEvent_1_t2B45C3F6635571A8C6D2013E7FB551792B14967A;
// UnityEngine.Events.UnityEvent`1<Lean.Common.LeanSelectable>
struct UnityEvent_1_t3102238E497F1A03968983BDEFDB53CA8754BA66;
// UnityEngine.Events.UnityEvent`1<System.Object>
struct UnityEvent_1_t32063FE815890FF672DF76288FAC4ABE089B899F;
// UnityEngine.Events.UnityEvent`1<System.Single>
struct UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC;
// UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>
struct UnityEvent_1_t3E6599546F71BCEFF271ED16D5DF9646BD868D7C;
// UnityEngine.Events.UnityEvent`1<UnityEngine.Vector3>
struct UnityEvent_1_t0D27852CE4491ECD77DB83CDC47BEFDE77503829;
// UnityEngine.Events.UnityEvent`2<System.Int32,System.Int32>
struct UnityEvent_2_t4466EA6EFC69AB8918D65EEDF0ADB0A9DCC7B8E3;
// UnityEngine.Events.UnityEvent`2<UnityEngine.Vector3,UnityEngine.Vector3>
struct UnityEvent_2_t7498A4B8C5E75A81F4E86F3F28CDC2E585CBE4D5;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9;
// Lean.Touch.LeanFinger
struct LeanFinger_t1946F69284DB4E17CCF85A60E0D2481F4E0CEFE5;
// Lean.Touch.LeanFingerData
struct LeanFingerData_t328C450AB89CFDD08B20C08DA6D11DDECAAB2138;
// Lean.Common.LeanSelectable
struct LeanSelectable_t13E1CFAF8EE59B540FD6FFD8D7221B38F6C9F88C;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// UnityEngine.Events.UnityEvent
struct UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4;
// Lean.Touch.LeanLastUp/Vector2Event
struct Vector2Event_t76245A810A59CF029CB90FB6C09F13CF673F6A16;
// Lean.Touch.LeanLastUp/Vector3Event
struct Vector3Event_t6497ADA553FB0219F72FDAB1B28B8DA6CA0CCF98;
// Lean.Touch.LeanManualFlick/FingerData
struct FingerData_tA693309440FFF62DF1318F31DF9DE6D60CA78599;
// Lean.Touch.LeanMouseWheel/FloatEvent
struct FloatEvent_t558EF23D9ACA2E234F374A076572700163CBC4EC;
// Lean.Touch.LeanMultiDirection/FloatEvent
struct FloatEvent_tC461FC41ED59B2878728F92A3326F7E5301E66A5;
// Lean.Touch.LeanMultiDown/LeanFingerListEvent
struct LeanFingerListEvent_t4274EF1678712C379AE3443B84AE74F3E830511F;
// Lean.Touch.LeanMultiDown/Vector2Event
struct Vector2Event_t49301FE0AF6F2860157E77CE73C11BB7E1899F74;
// Lean.Touch.LeanMultiDown/Vector3Event
struct Vector3Event_t926F35A94CB9A9A4948079F9BF7611A04F771567;
// Lean.Touch.LeanMultiHeld/FingerData
struct FingerData_t3B100938E369689AC520A735775E2551358BCDD9;
// Lean.Touch.LeanMultiHeld/LeanFingerListEvent
struct LeanFingerListEvent_t5C3C6B04068B5F941FCAEB2120B2268BA24943BF;
// Lean.Touch.LeanMultiHeld/Vector2Event
struct Vector2Event_t859DB0998D957E7C474DF5D40EE4994506BCA526;
// Lean.Touch.LeanMultiHeld/Vector3Event
struct Vector3Event_tD2E3AAB5429575C1AC5102F1A5B497E4FCF822EF;
// Lean.Touch.LeanMultiPinch/FloatEvent
struct FloatEvent_tE4250CE5F1D5FCFEE7513ABEC1197997DD8A6D3C;
// Lean.Touch.LeanMultiPull/FloatEvent
struct FloatEvent_t52EA07CE1AEFF9D36CE3DFF7DC7B022F22DD314A;
// Lean.Touch.LeanMultiPull/Vector2Event
struct Vector2Event_t6F04BE5FB03CD574DA62371DC09BFE114668BC19;
// Lean.Touch.LeanMultiPull/Vector3Event
struct Vector3Event_tFC38367080297930D0E4319CD5CEEBDCAFEC063C;
// Lean.Touch.LeanMultiPull/Vector3Vector3Event
struct Vector3Vector3Event_t02A8D156358087F3EF5F9603BF6586CBDD1E586D;
// Lean.Touch.LeanMultiSwipe/FingerListEvent
struct FingerListEvent_t4D146A7DA4C2A74F61B251377A883515B6E530BB;
// Lean.Touch.LeanMultiSwipe/FloatEvent
struct FloatEvent_t6379D4D73A5BE4770B5C0EE9F9AF2D26B2186EED;
// Lean.Touch.LeanMultiSwipe/Vector2Event
struct Vector2Event_t3F4B11D56D2AB9502E93CA635ACC38C9BCB7BBE1;
// Lean.Touch.LeanMultiTap/IntEvent
struct IntEvent_t95BD74EA4CD3ACB376B2125294F26D7D61C9E53A;
// Lean.Touch.LeanMultiTap/IntIntEvent
struct IntIntEvent_t826C1A3699D71EEA932BE16511E6057FD5860A90;
// Lean.Touch.LeanMultiTwist/FloatEvent
struct FloatEvent_t94A4076DD66B100CD6C8D2A2EA33BCA0C65CACE9;
// Lean.Touch.LeanMultiUp/LeanFingerEvent
struct LeanFingerEvent_tCB1725CAE676B4DB695050D9A1F389AEDC375286;
// Lean.Touch.LeanMultiUp/Vector2Event
struct Vector2Event_t8B6CC82A61F3431DBC6D5323888D34ADB5F618FD;
// Lean.Touch.LeanMultiUp/Vector3Event
struct Vector3Event_tD484F3AD9A24C43ACA2065B92BC76955724B7B88;
// Lean.Touch.LeanMultiUpdate/FloatEvent
struct FloatEvent_t0C1E2C304AB9CBCCB0986165186C8CEA51B78A6E;
// Lean.Touch.LeanMultiUpdate/LeanFingerListEvent
struct LeanFingerListEvent_tE5C9A18477AA0141882F033112952492BD42A5B2;
// Lean.Touch.LeanMultiUpdate/Vector2Event
struct Vector2Event_t16FA65D6F9EDD3E67DE796CEF2A0E58D80B116EB;
// Lean.Touch.LeanMultiUpdate/Vector3Event
struct Vector3Event_tBC3694D74EF4542CAF05A69FD280A1B0C8195378;
// Lean.Touch.LeanMultiUpdate/Vector3Vector3Event
struct Vector3Vector3Event_t71C436ADE3DCA9F9BFFE4412D56323AFC3603BC5;
// Lean.Touch.LeanMultiUpdateCanvas/LeanFingerListEvent
struct LeanFingerListEvent_t41AD12C7D8558553948AE6F468DDDAC9231F7E60;
// Lean.Touch.LeanMultiUpdateCanvas/Vector3Event
struct Vector3Event_t3D07A493F09E5436AA7E40C33B9ECCEF5E5ADDC7;
// Lean.Touch.LeanPick/LeanPickableEvent
struct LeanPickableEvent_t2035637C5C5B3B1D78CF2AA6789D51AC0CF4FF24;
// Lean.Touch.LeanPickable/LeanFingerEvent
struct LeanFingerEvent_t537C4971FFFDB4E936119E39123CECCD8A09BD27;
// Lean.Touch.LeanPickable/Vector2Event
struct Vector2Event_t11759D6BE15AF79E12606AD28585BD610E4A488E;
// Lean.Touch.LeanPickable/Vector3Event
struct Vector3Event_tBA87028292EB78380A5443330E1BFAFE6F6252C0;
// Lean.Touch.LeanSelectableCenter/Vector3Event
struct Vector3Event_t5BB81343A5679B4A5ACA81B17B6FF70CFF69622E;
// Lean.Touch.LeanSelectableCount/IntEvent
struct IntEvent_t575CCDFCECE14C6B0AC894D6292A63C0347021C0;
// Lean.Touch.LeanSelectableDial/FloatEvent
struct FloatEvent_tDCF2F2F45E0094C75F402722A071C7039CC429E0;
// Lean.Touch.LeanSelectableDial/Trigger
struct Trigger_tCDE0F6503E25FA1F39A693069031F6D3CA0BBC2A;
// Lean.Touch.LeanSelectableDrop/GameObjectEvent
struct GameObjectEvent_t96555E9F187AAEE2755021672C67B96E1E49BE7A;
// Lean.Touch.LeanSelectableDrop/IDropHandlerEvent
struct IDropHandlerEvent_t7FAACF71D3C7DCA66B5941CBFAA7D973EB8C6A50;
// Lean.Touch.LeanSelectableSelected/SelectableEvent
struct SelectableEvent_t19EF1FD7E2CD83A5F68C5CC39BF06A8150F9E57F;
// Lean.Touch.LeanSelectableTime/FloatEvent
struct FloatEvent_t6007A8BA6CFE0E63BB997AB057E0559347222539;
// Lean.Touch.LeanSelectionBox/FingerData
struct FingerData_t6E16DC6ACC7AFF5E7729EE7CC3D25085C9C4D2D9;
// Lean.Touch.LeanShapeDetector/FingerData
struct FingerData_tCB056267858269BE48D3C3FE2D5259007930BD10;
// Lean.Touch.LeanShapeDetector/LeanFingerEvent
struct LeanFingerEvent_t3C3407D8CC16780A7CAEAE80DC13A2D81E810333;
// Lean.Touch.LeanSpawnWithFinger/FingerData
struct FingerData_t881CBDF9045D4EF2DFE0E3C8D855E4691F9334A5;

IL2CPP_EXTERN_C RuntimeClass* List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m0ED1CF21DC6B8863BF9FB9E9CE08331E11258F53_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_mD6469330B5381EC92DDF375773100F6372E6114C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m3E1AEDD64868D9F6901AFBF0FA6B0A7A0001BA1E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1__ctor_m30F443398054B5E3666B3C86E64A5C0FF97D93FF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1__ctor_m3699DCBD7E56C12E160DD6F866F75F233240D828_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1__ctor_m71F69F533D6B27B4184EE5F293407A8B945B9221_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1__ctor_m7D0885503EB891D82930D434289763AEE7C347C9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1__ctor_m9B3C480571121A197B4D6C2035EAF34243A710C0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1__ctor_mDD42C6C3EB41D2D99D65F39E70F578E0740B4495_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1__ctor_mF2353BD6855BD9E925E30E1CD4BC8582182DE0C7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1__ctor_mFEC498EC5642F0C80769E4D2E08EA2E3E4D3D0EB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_2__ctor_m5BD689AC7009890F9F24391FEBF7AE883CFF2B67_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_2__ctor_mB0024D2F132EC65C8DA83C650ECE1ADA38960E10_RuntimeMethod_var;

struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9, ____items_1)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get__items_1() const { return ____items_1; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9_StaticFields, ____emptyArray_5)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// Lean.Touch.LeanFingerData
struct LeanFingerData_t328C450AB89CFDD08B20C08DA6D11DDECAAB2138  : public RuntimeObject
{
public:
	// Lean.Touch.LeanFinger Lean.Touch.LeanFingerData::Finger
	LeanFinger_t1946F69284DB4E17CCF85A60E0D2481F4E0CEFE5 * ___Finger_0;

public:
	inline static int32_t get_offset_of_Finger_0() { return static_cast<int32_t>(offsetof(LeanFingerData_t328C450AB89CFDD08B20C08DA6D11DDECAAB2138, ___Finger_0)); }
	inline LeanFinger_t1946F69284DB4E17CCF85A60E0D2481F4E0CEFE5 * get_Finger_0() const { return ___Finger_0; }
	inline LeanFinger_t1946F69284DB4E17CCF85A60E0D2481F4E0CEFE5 ** get_address_of_Finger_0() { return &___Finger_0; }
	inline void set_Finger_0(LeanFinger_t1946F69284DB4E17CCF85A60E0D2481F4E0CEFE5 * value)
	{
		___Finger_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Finger_0), (void*)value);
	}
};


// UnityEngine.Events.UnityEventBase
struct UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_Calls_0)); }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Calls_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PersistentCalls_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// Lean.Touch.LeanSelectableDial/Trigger
struct Trigger_tCDE0F6503E25FA1F39A693069031F6D3CA0BBC2A  : public RuntimeObject
{
public:
	// System.Single Lean.Touch.LeanSelectableDial/Trigger::Angle
	float ___Angle_0;
	// System.Single Lean.Touch.LeanSelectableDial/Trigger::Arc
	float ___Arc_1;
	// System.Boolean Lean.Touch.LeanSelectableDial/Trigger::Inside
	bool ___Inside_2;
	// UnityEngine.Events.UnityEvent Lean.Touch.LeanSelectableDial/Trigger::onEnter
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___onEnter_3;
	// UnityEngine.Events.UnityEvent Lean.Touch.LeanSelectableDial/Trigger::onExit
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___onExit_4;

public:
	inline static int32_t get_offset_of_Angle_0() { return static_cast<int32_t>(offsetof(Trigger_tCDE0F6503E25FA1F39A693069031F6D3CA0BBC2A, ___Angle_0)); }
	inline float get_Angle_0() const { return ___Angle_0; }
	inline float* get_address_of_Angle_0() { return &___Angle_0; }
	inline void set_Angle_0(float value)
	{
		___Angle_0 = value;
	}

	inline static int32_t get_offset_of_Arc_1() { return static_cast<int32_t>(offsetof(Trigger_tCDE0F6503E25FA1F39A693069031F6D3CA0BBC2A, ___Arc_1)); }
	inline float get_Arc_1() const { return ___Arc_1; }
	inline float* get_address_of_Arc_1() { return &___Arc_1; }
	inline void set_Arc_1(float value)
	{
		___Arc_1 = value;
	}

	inline static int32_t get_offset_of_Inside_2() { return static_cast<int32_t>(offsetof(Trigger_tCDE0F6503E25FA1F39A693069031F6D3CA0BBC2A, ___Inside_2)); }
	inline bool get_Inside_2() const { return ___Inside_2; }
	inline bool* get_address_of_Inside_2() { return &___Inside_2; }
	inline void set_Inside_2(bool value)
	{
		___Inside_2 = value;
	}

	inline static int32_t get_offset_of_onEnter_3() { return static_cast<int32_t>(offsetof(Trigger_tCDE0F6503E25FA1F39A693069031F6D3CA0BBC2A, ___onEnter_3)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_onEnter_3() const { return ___onEnter_3; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_onEnter_3() { return &___onEnter_3; }
	inline void set_onEnter_3(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___onEnter_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onEnter_3), (void*)value);
	}

	inline static int32_t get_offset_of_onExit_4() { return static_cast<int32_t>(offsetof(Trigger_tCDE0F6503E25FA1F39A693069031F6D3CA0BBC2A, ___onExit_4)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_onExit_4() const { return ___onExit_4; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_onExit_4() { return &___onExit_4; }
	inline void set_onExit_4(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___onExit_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onExit_4), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<System.Collections.Generic.List`1<Lean.Touch.LeanFinger>>
struct UnityEvent_1_t4B73E8A5E7FCB83B3A9ACFDBB9932EF0B4E0B39A  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t4B73E8A5E7FCB83B3A9ACFDBB9932EF0B4E0B39A, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<UnityEngine.GameObject>
struct UnityEvent_1_t1DC2DB931FE9E53AEC9A04F4DE9B4F7B469BC78E  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t1DC2DB931FE9E53AEC9A04F4DE9B4F7B469BC78E, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<Lean.Touch.IDropHandler>
struct UnityEvent_1_t2F5AE12EB5154F02C654F49C1DFD43714173A0C3  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2F5AE12EB5154F02C654F49C1DFD43714173A0C3, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<System.Int32>
struct UnityEvent_1_tB235B5DAD099AC425DC059D10DEB8B97A35E2BBF  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_tB235B5DAD099AC425DC059D10DEB8B97A35E2BBF, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<Lean.Touch.LeanFinger>
struct UnityEvent_1_t88B7ACE02A62AB7E41FF842AFDDDFA5860884BDA  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t88B7ACE02A62AB7E41FF842AFDDDFA5860884BDA, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<Lean.Touch.LeanPickable>
struct UnityEvent_1_t2B45C3F6635571A8C6D2013E7FB551792B14967A  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2B45C3F6635571A8C6D2013E7FB551792B14967A, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<Lean.Common.LeanSelectable>
struct UnityEvent_1_t3102238E497F1A03968983BDEFDB53CA8754BA66  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3102238E497F1A03968983BDEFDB53CA8754BA66, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<System.Single>
struct UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>
struct UnityEvent_1_t3E6599546F71BCEFF271ED16D5DF9646BD868D7C  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3E6599546F71BCEFF271ED16D5DF9646BD868D7C, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<UnityEngine.Vector3>
struct UnityEvent_1_t0D27852CE4491ECD77DB83CDC47BEFDE77503829  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t0D27852CE4491ECD77DB83CDC47BEFDE77503829, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`2<System.Int32,System.Int32>
struct UnityEvent_2_t4466EA6EFC69AB8918D65EEDF0ADB0A9DCC7B8E3  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_2_t4466EA6EFC69AB8918D65EEDF0ADB0A9DCC7B8E3, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`2<UnityEngine.Vector3,UnityEngine.Vector3>
struct UnityEvent_2_t7498A4B8C5E75A81F4E86F3F28CDC2E585CBE4D5  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_2_t7498A4B8C5E75A81F4E86F3F28CDC2E585CBE4D5, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Events.UnityEvent
struct UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// Lean.Touch.LeanManualFlick/FingerData
struct FingerData_tA693309440FFF62DF1318F31DF9DE6D60CA78599  : public LeanFingerData_t328C450AB89CFDD08B20C08DA6D11DDECAAB2138
{
public:
	// System.Boolean Lean.Touch.LeanManualFlick/FingerData::Flicked
	bool ___Flicked_1;

public:
	inline static int32_t get_offset_of_Flicked_1() { return static_cast<int32_t>(offsetof(FingerData_tA693309440FFF62DF1318F31DF9DE6D60CA78599, ___Flicked_1)); }
	inline bool get_Flicked_1() const { return ___Flicked_1; }
	inline bool* get_address_of_Flicked_1() { return &___Flicked_1; }
	inline void set_Flicked_1(bool value)
	{
		___Flicked_1 = value;
	}
};


// Lean.Touch.LeanSelectionBox/FingerData
struct FingerData_t6E16DC6ACC7AFF5E7729EE7CC3D25085C9C4D2D9  : public LeanFingerData_t328C450AB89CFDD08B20C08DA6D11DDECAAB2138
{
public:
	// UnityEngine.RectTransform Lean.Touch.LeanSelectionBox/FingerData::Box
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___Box_1;

public:
	inline static int32_t get_offset_of_Box_1() { return static_cast<int32_t>(offsetof(FingerData_t6E16DC6ACC7AFF5E7729EE7CC3D25085C9C4D2D9, ___Box_1)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_Box_1() const { return ___Box_1; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_Box_1() { return &___Box_1; }
	inline void set_Box_1(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___Box_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Box_1), (void*)value);
	}
};


// Lean.Touch.LeanShapeDetector/FingerData
struct FingerData_tCB056267858269BE48D3C3FE2D5259007930BD10  : public LeanFingerData_t328C450AB89CFDD08B20C08DA6D11DDECAAB2138
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Lean.Touch.LeanShapeDetector/FingerData::Points
	List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * ___Points_1;

public:
	inline static int32_t get_offset_of_Points_1() { return static_cast<int32_t>(offsetof(FingerData_tCB056267858269BE48D3C3FE2D5259007930BD10, ___Points_1)); }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * get_Points_1() const { return ___Points_1; }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 ** get_address_of_Points_1() { return &___Points_1; }
	inline void set_Points_1(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * value)
	{
		___Points_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Points_1), (void*)value);
	}
};


// Lean.Touch.LeanSpawnWithFinger/FingerData
struct FingerData_t881CBDF9045D4EF2DFE0E3C8D855E4691F9334A5  : public LeanFingerData_t328C450AB89CFDD08B20C08DA6D11DDECAAB2138
{
public:
	// UnityEngine.Transform Lean.Touch.LeanSpawnWithFinger/FingerData::Clone
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___Clone_1;
	// Lean.Common.LeanSelectable Lean.Touch.LeanSpawnWithFinger/FingerData::Selectable
	LeanSelectable_t13E1CFAF8EE59B540FD6FFD8D7221B38F6C9F88C * ___Selectable_2;

public:
	inline static int32_t get_offset_of_Clone_1() { return static_cast<int32_t>(offsetof(FingerData_t881CBDF9045D4EF2DFE0E3C8D855E4691F9334A5, ___Clone_1)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_Clone_1() const { return ___Clone_1; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_Clone_1() { return &___Clone_1; }
	inline void set_Clone_1(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___Clone_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Clone_1), (void*)value);
	}

	inline static int32_t get_offset_of_Selectable_2() { return static_cast<int32_t>(offsetof(FingerData_t881CBDF9045D4EF2DFE0E3C8D855E4691F9334A5, ___Selectable_2)); }
	inline LeanSelectable_t13E1CFAF8EE59B540FD6FFD8D7221B38F6C9F88C * get_Selectable_2() const { return ___Selectable_2; }
	inline LeanSelectable_t13E1CFAF8EE59B540FD6FFD8D7221B38F6C9F88C ** get_address_of_Selectable_2() { return &___Selectable_2; }
	inline void set_Selectable_2(LeanSelectable_t13E1CFAF8EE59B540FD6FFD8D7221B38F6C9F88C * value)
	{
		___Selectable_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Selectable_2), (void*)value);
	}
};


// Lean.Touch.LeanLastUp/Vector2Event
struct Vector2Event_t76245A810A59CF029CB90FB6C09F13CF673F6A16  : public UnityEvent_1_t3E6599546F71BCEFF271ED16D5DF9646BD868D7C
{
public:

public:
};


// Lean.Touch.LeanLastUp/Vector3Event
struct Vector3Event_t6497ADA553FB0219F72FDAB1B28B8DA6CA0CCF98  : public UnityEvent_1_t0D27852CE4491ECD77DB83CDC47BEFDE77503829
{
public:

public:
};


// Lean.Touch.LeanManualFlick/CheckType
struct CheckType_tDA0B4D26E1804E3C420A2C74766165B193088016 
{
public:
	// System.Int32 Lean.Touch.LeanManualFlick/CheckType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CheckType_tDA0B4D26E1804E3C420A2C74766165B193088016, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Lean.Touch.LeanMouseWheel/CoordinateType
struct CoordinateType_tB7802B14E33D91991A1A3D02A623717DDD92BC90 
{
public:
	// System.Int32 Lean.Touch.LeanMouseWheel/CoordinateType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CoordinateType_tB7802B14E33D91991A1A3D02A623717DDD92BC90, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Lean.Touch.LeanMouseWheel/FloatEvent
struct FloatEvent_t558EF23D9ACA2E234F374A076572700163CBC4EC  : public UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC
{
public:

public:
};


// Lean.Touch.LeanMouseWheel/ModifyType
struct ModifyType_t3D55F84C6CBA81B1EAD6696C58E7151906AE1ADB 
{
public:
	// System.Int32 Lean.Touch.LeanMouseWheel/ModifyType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ModifyType_t3D55F84C6CBA81B1EAD6696C58E7151906AE1ADB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Lean.Touch.LeanMultiDirection/CoordinateType
struct CoordinateType_tBE7C021DB9495B402FF54E59AB6123989364AD34 
{
public:
	// System.Int32 Lean.Touch.LeanMultiDirection/CoordinateType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CoordinateType_tBE7C021DB9495B402FF54E59AB6123989364AD34, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Lean.Touch.LeanMultiDirection/FloatEvent
struct FloatEvent_tC461FC41ED59B2878728F92A3326F7E5301E66A5  : public UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC
{
public:

public:
};


// Lean.Touch.LeanMultiDown/LeanFingerListEvent
struct LeanFingerListEvent_t4274EF1678712C379AE3443B84AE74F3E830511F  : public UnityEvent_1_t4B73E8A5E7FCB83B3A9ACFDBB9932EF0B4E0B39A
{
public:

public:
};


// Lean.Touch.LeanMultiDown/Vector2Event
struct Vector2Event_t49301FE0AF6F2860157E77CE73C11BB7E1899F74  : public UnityEvent_1_t3E6599546F71BCEFF271ED16D5DF9646BD868D7C
{
public:

public:
};


// Lean.Touch.LeanMultiDown/Vector3Event
struct Vector3Event_t926F35A94CB9A9A4948079F9BF7611A04F771567  : public UnityEvent_1_t0D27852CE4491ECD77DB83CDC47BEFDE77503829
{
public:

public:
};


// Lean.Touch.LeanMultiHeld/FingerData
struct FingerData_t3B100938E369689AC520A735775E2551358BCDD9  : public LeanFingerData_t328C450AB89CFDD08B20C08DA6D11DDECAAB2138
{
public:
	// UnityEngine.Vector2 Lean.Touch.LeanMultiHeld/FingerData::Movement
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___Movement_1;

public:
	inline static int32_t get_offset_of_Movement_1() { return static_cast<int32_t>(offsetof(FingerData_t3B100938E369689AC520A735775E2551358BCDD9, ___Movement_1)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_Movement_1() const { return ___Movement_1; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_Movement_1() { return &___Movement_1; }
	inline void set_Movement_1(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___Movement_1 = value;
	}
};


// Lean.Touch.LeanMultiHeld/LeanFingerListEvent
struct LeanFingerListEvent_t5C3C6B04068B5F941FCAEB2120B2268BA24943BF  : public UnityEvent_1_t4B73E8A5E7FCB83B3A9ACFDBB9932EF0B4E0B39A
{
public:

public:
};


// Lean.Touch.LeanMultiHeld/Vector2Event
struct Vector2Event_t859DB0998D957E7C474DF5D40EE4994506BCA526  : public UnityEvent_1_t3E6599546F71BCEFF271ED16D5DF9646BD868D7C
{
public:

public:
};


// Lean.Touch.LeanMultiHeld/Vector3Event
struct Vector3Event_tD2E3AAB5429575C1AC5102F1A5B497E4FCF822EF  : public UnityEvent_1_t0D27852CE4491ECD77DB83CDC47BEFDE77503829
{
public:

public:
};


// Lean.Touch.LeanMultiPinch/CoordinateType
struct CoordinateType_t2AA5614729BD34C1634538CA27287DA23F5EDEA8 
{
public:
	// System.Int32 Lean.Touch.LeanMultiPinch/CoordinateType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CoordinateType_t2AA5614729BD34C1634538CA27287DA23F5EDEA8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Lean.Touch.LeanMultiPinch/FloatEvent
struct FloatEvent_tE4250CE5F1D5FCFEE7513ABEC1197997DD8A6D3C  : public UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC
{
public:

public:
};


// Lean.Touch.LeanMultiPull/CoordinateType
struct CoordinateType_tA876FC3345824D00871ED0D08BFC40F54B92FBE2 
{
public:
	// System.Int32 Lean.Touch.LeanMultiPull/CoordinateType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CoordinateType_tA876FC3345824D00871ED0D08BFC40F54B92FBE2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Lean.Touch.LeanMultiPull/FloatEvent
struct FloatEvent_t52EA07CE1AEFF9D36CE3DFF7DC7B022F22DD314A  : public UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC
{
public:

public:
};


// Lean.Touch.LeanMultiPull/Vector2Event
struct Vector2Event_t6F04BE5FB03CD574DA62371DC09BFE114668BC19  : public UnityEvent_1_t3E6599546F71BCEFF271ED16D5DF9646BD868D7C
{
public:

public:
};


// Lean.Touch.LeanMultiPull/Vector3Event
struct Vector3Event_tFC38367080297930D0E4319CD5CEEBDCAFEC063C  : public UnityEvent_1_t0D27852CE4491ECD77DB83CDC47BEFDE77503829
{
public:

public:
};


// Lean.Touch.LeanMultiPull/Vector3Vector3Event
struct Vector3Vector3Event_t02A8D156358087F3EF5F9603BF6586CBDD1E586D  : public UnityEvent_2_t7498A4B8C5E75A81F4E86F3F28CDC2E585CBE4D5
{
public:

public:
};


// Lean.Touch.LeanMultiSwipe/FingerListEvent
struct FingerListEvent_t4D146A7DA4C2A74F61B251377A883515B6E530BB  : public UnityEvent_1_t4B73E8A5E7FCB83B3A9ACFDBB9932EF0B4E0B39A
{
public:

public:
};


// Lean.Touch.LeanMultiSwipe/FloatEvent
struct FloatEvent_t6379D4D73A5BE4770B5C0EE9F9AF2D26B2186EED  : public UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC
{
public:

public:
};


// Lean.Touch.LeanMultiSwipe/Vector2Event
struct Vector2Event_t3F4B11D56D2AB9502E93CA635ACC38C9BCB7BBE1  : public UnityEvent_1_t3E6599546F71BCEFF271ED16D5DF9646BD868D7C
{
public:

public:
};


// Lean.Touch.LeanMultiTap/IntEvent
struct IntEvent_t95BD74EA4CD3ACB376B2125294F26D7D61C9E53A  : public UnityEvent_1_tB235B5DAD099AC425DC059D10DEB8B97A35E2BBF
{
public:

public:
};


// Lean.Touch.LeanMultiTap/IntIntEvent
struct IntIntEvent_t826C1A3699D71EEA932BE16511E6057FD5860A90  : public UnityEvent_2_t4466EA6EFC69AB8918D65EEDF0ADB0A9DCC7B8E3
{
public:

public:
};


// Lean.Touch.LeanMultiTwist/FloatEvent
struct FloatEvent_t94A4076DD66B100CD6C8D2A2EA33BCA0C65CACE9  : public UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC
{
public:

public:
};


// Lean.Touch.LeanMultiTwist/OneFingerType
struct OneFingerType_t5465A75FCD70C03A691D1ADA43378FA5B8A7DABA 
{
public:
	// System.Int32 Lean.Touch.LeanMultiTwist/OneFingerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OneFingerType_t5465A75FCD70C03A691D1ADA43378FA5B8A7DABA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Lean.Touch.LeanMultiUp/LeanFingerEvent
struct LeanFingerEvent_tCB1725CAE676B4DB695050D9A1F389AEDC375286  : public UnityEvent_1_t88B7ACE02A62AB7E41FF842AFDDDFA5860884BDA
{
public:

public:
};


// Lean.Touch.LeanMultiUp/Vector2Event
struct Vector2Event_t8B6CC82A61F3431DBC6D5323888D34ADB5F618FD  : public UnityEvent_1_t3E6599546F71BCEFF271ED16D5DF9646BD868D7C
{
public:

public:
};


// Lean.Touch.LeanMultiUp/Vector3Event
struct Vector3Event_tD484F3AD9A24C43ACA2065B92BC76955724B7B88  : public UnityEvent_1_t0D27852CE4491ECD77DB83CDC47BEFDE77503829
{
public:

public:
};


// Lean.Touch.LeanMultiUpdate/CoordinateType
struct CoordinateType_tD0581A95E92F15D33BD520F69C8FACCDD42B64ED 
{
public:
	// System.Int32 Lean.Touch.LeanMultiUpdate/CoordinateType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CoordinateType_tD0581A95E92F15D33BD520F69C8FACCDD42B64ED, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Lean.Touch.LeanMultiUpdate/FloatEvent
struct FloatEvent_t0C1E2C304AB9CBCCB0986165186C8CEA51B78A6E  : public UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC
{
public:

public:
};


// Lean.Touch.LeanMultiUpdate/LeanFingerListEvent
struct LeanFingerListEvent_tE5C9A18477AA0141882F033112952492BD42A5B2  : public UnityEvent_1_t4B73E8A5E7FCB83B3A9ACFDBB9932EF0B4E0B39A
{
public:

public:
};


// Lean.Touch.LeanMultiUpdate/Vector2Event
struct Vector2Event_t16FA65D6F9EDD3E67DE796CEF2A0E58D80B116EB  : public UnityEvent_1_t3E6599546F71BCEFF271ED16D5DF9646BD868D7C
{
public:

public:
};


// Lean.Touch.LeanMultiUpdate/Vector3Event
struct Vector3Event_tBC3694D74EF4542CAF05A69FD280A1B0C8195378  : public UnityEvent_1_t0D27852CE4491ECD77DB83CDC47BEFDE77503829
{
public:

public:
};


// Lean.Touch.LeanMultiUpdate/Vector3Vector3Event
struct Vector3Vector3Event_t71C436ADE3DCA9F9BFFE4412D56323AFC3603BC5  : public UnityEvent_2_t7498A4B8C5E75A81F4E86F3F28CDC2E585CBE4D5
{
public:

public:
};


// Lean.Touch.LeanMultiUpdateCanvas/LeanFingerListEvent
struct LeanFingerListEvent_t41AD12C7D8558553948AE6F468DDDAC9231F7E60  : public UnityEvent_1_t4B73E8A5E7FCB83B3A9ACFDBB9932EF0B4E0B39A
{
public:

public:
};


// Lean.Touch.LeanMultiUpdateCanvas/Vector3Event
struct Vector3Event_t3D07A493F09E5436AA7E40C33B9ECCEF5E5ADDC7  : public UnityEvent_1_t0D27852CE4491ECD77DB83CDC47BEFDE77503829
{
public:

public:
};


// Lean.Touch.LeanPick/LeanPickableEvent
struct LeanPickableEvent_t2035637C5C5B3B1D78CF2AA6789D51AC0CF4FF24  : public UnityEvent_1_t2B45C3F6635571A8C6D2013E7FB551792B14967A
{
public:

public:
};


// Lean.Touch.LeanPickable/LeanFingerEvent
struct LeanFingerEvent_t537C4971FFFDB4E936119E39123CECCD8A09BD27  : public UnityEvent_1_t88B7ACE02A62AB7E41FF842AFDDDFA5860884BDA
{
public:

public:
};


// Lean.Touch.LeanPickable/Vector2Event
struct Vector2Event_t11759D6BE15AF79E12606AD28585BD610E4A488E  : public UnityEvent_1_t3E6599546F71BCEFF271ED16D5DF9646BD868D7C
{
public:

public:
};


// Lean.Touch.LeanPickable/Vector3Event
struct Vector3Event_tBA87028292EB78380A5443330E1BFAFE6F6252C0  : public UnityEvent_1_t0D27852CE4491ECD77DB83CDC47BEFDE77503829
{
public:

public:
};


// Lean.Touch.LeanSelectableCenter/Vector3Event
struct Vector3Event_t5BB81343A5679B4A5ACA81B17B6FF70CFF69622E  : public UnityEvent_1_t0D27852CE4491ECD77DB83CDC47BEFDE77503829
{
public:

public:
};


// Lean.Touch.LeanSelectableCount/IntEvent
struct IntEvent_t575CCDFCECE14C6B0AC894D6292A63C0347021C0  : public UnityEvent_1_tB235B5DAD099AC425DC059D10DEB8B97A35E2BBF
{
public:

public:
};


// Lean.Touch.LeanSelectableDial/FloatEvent
struct FloatEvent_tDCF2F2F45E0094C75F402722A071C7039CC429E0  : public UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC
{
public:

public:
};


// Lean.Touch.LeanSelectableDrop/GameObjectEvent
struct GameObjectEvent_t96555E9F187AAEE2755021672C67B96E1E49BE7A  : public UnityEvent_1_t1DC2DB931FE9E53AEC9A04F4DE9B4F7B469BC78E
{
public:

public:
};


// Lean.Touch.LeanSelectableDrop/IDropHandlerEvent
struct IDropHandlerEvent_t7FAACF71D3C7DCA66B5941CBFAA7D973EB8C6A50  : public UnityEvent_1_t2F5AE12EB5154F02C654F49C1DFD43714173A0C3
{
public:

public:
};


// Lean.Touch.LeanSelectableDrop/IgnoreType
struct IgnoreType_t1837EAD689A3F7AE318771A4BAD91538B6603891 
{
public:
	// System.Int32 Lean.Touch.LeanSelectableDrop/IgnoreType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(IgnoreType_t1837EAD689A3F7AE318771A4BAD91538B6603891, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Lean.Touch.LeanSelectableSelected/ResetType
struct ResetType_tA29E4DA85EA82FC3DD3FC19E86A9B36EC9DEC5BF 
{
public:
	// System.Int32 Lean.Touch.LeanSelectableSelected/ResetType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ResetType_tA29E4DA85EA82FC3DD3FC19E86A9B36EC9DEC5BF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Lean.Touch.LeanSelectableSelected/SelectableEvent
struct SelectableEvent_t19EF1FD7E2CD83A5F68C5CC39BF06A8150F9E57F  : public UnityEvent_1_t3102238E497F1A03968983BDEFDB53CA8754BA66
{
public:

public:
};


// Lean.Touch.LeanSelectableTime/FloatEvent
struct FloatEvent_t6007A8BA6CFE0E63BB997AB057E0559347222539  : public UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC
{
public:

public:
};


// Lean.Touch.LeanSelectableTime/SendType
struct SendType_tBCFD1C4C8E2904A04AD9AB371F6C74429B206860 
{
public:
	// System.Int32 Lean.Touch.LeanSelectableTime/SendType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SendType_tBCFD1C4C8E2904A04AD9AB371F6C74429B206860, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Lean.Touch.LeanShapeDetector/DirectionType
struct DirectionType_t1CE51D829E3B91657ACB5004597AAABC1B3BAA9D 
{
public:
	// System.Int32 Lean.Touch.LeanShapeDetector/DirectionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DirectionType_t1CE51D829E3B91657ACB5004597AAABC1B3BAA9D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Lean.Touch.LeanShapeDetector/LeanFingerEvent
struct LeanFingerEvent_t3C3407D8CC16780A7CAEAE80DC13A2D81E810333  : public UnityEvent_1_t88B7ACE02A62AB7E41FF842AFDDDFA5860884BDA
{
public:

public:
};


// Lean.Touch.LeanShapeDetector/Line
struct Line_tD54BCC2D376196864D00E9AEF7D66C042AFE3003 
{
public:
	// UnityEngine.Vector2 Lean.Touch.LeanShapeDetector/Line::A
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___A_0;
	// UnityEngine.Vector2 Lean.Touch.LeanShapeDetector/Line::B
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___B_1;

public:
	inline static int32_t get_offset_of_A_0() { return static_cast<int32_t>(offsetof(Line_tD54BCC2D376196864D00E9AEF7D66C042AFE3003, ___A_0)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_A_0() const { return ___A_0; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_A_0() { return &___A_0; }
	inline void set_A_0(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___A_0 = value;
	}

	inline static int32_t get_offset_of_B_1() { return static_cast<int32_t>(offsetof(Line_tD54BCC2D376196864D00E9AEF7D66C042AFE3003, ___B_1)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_B_1() const { return ___B_1; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_B_1() { return &___B_1; }
	inline void set_B_1(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___B_1 = value;
	}
};


// Lean.Touch.LeanSpawnWithFinger/RotateType
struct RotateType_t8068CA324E458AC7DC2F935D079D79938EACA772 
{
public:
	// System.Int32 Lean.Touch.LeanSpawnWithFinger/RotateType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RotateType_t8068CA324E458AC7DC2F935D079D79938EACA772, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  m_Items[1];

public:
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		m_Items[index] = value;
	}
};


// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_1__ctor_mF2353BD6855BD9E925E30E1CD4BC8582182DE0C7_gshared (UnityEvent_1_t3E6599546F71BCEFF271ED16D5DF9646BD868D7C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Vector3>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8_gshared (UnityEvent_1_t0D27852CE4491ECD77DB83CDC47BEFDE77503829 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED_gshared (UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_1__ctor_mD87552C18A41196B69A62A366C8238FC246B151A_gshared (UnityEvent_1_t32063FE815890FF672DF76288FAC4ABE089B899F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`2<UnityEngine.Vector3,UnityEngine.Vector3>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_2__ctor_mB0024D2F132EC65C8DA83C650ECE1ADA38960E10_gshared (UnityEvent_2_t7498A4B8C5E75A81F4E86F3F28CDC2E585CBE4D5 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_1__ctor_m30F443398054B5E3666B3C86E64A5C0FF97D93FF_gshared (UnityEvent_1_tB235B5DAD099AC425DC059D10DEB8B97A35E2BBF * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`2<System.Int32,System.Int32>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_2__ctor_m5BD689AC7009890F9F24391FEBF7AE883CFF2B67_gshared (UnityEvent_2_t4466EA6EFC69AB8918D65EEDF0ADB0A9DCC7B8E3 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_mD6469330B5381EC92DDF375773100F6372E6114C_gshared_inline (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  List_1_get_Item_m3E1AEDD64868D9F6901AFBF0FA6B0A7A0001BA1E_gshared_inline (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m0ED1CF21DC6B8863BF9FB9E9CE08331E11258F53_gshared (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * __this, const RuntimeMethod* method);

// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::.ctor()
inline void UnityEvent_1__ctor_mF2353BD6855BD9E925E30E1CD4BC8582182DE0C7 (UnityEvent_1_t3E6599546F71BCEFF271ED16D5DF9646BD868D7C * __this, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_t3E6599546F71BCEFF271ED16D5DF9646BD868D7C *, const RuntimeMethod*))UnityEvent_1__ctor_mF2353BD6855BD9E925E30E1CD4BC8582182DE0C7_gshared)(__this, method);
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Vector3>::.ctor()
inline void UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8 (UnityEvent_1_t0D27852CE4491ECD77DB83CDC47BEFDE77503829 * __this, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_t0D27852CE4491ECD77DB83CDC47BEFDE77503829 *, const RuntimeMethod*))UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8_gshared)(__this, method);
}
// System.Void Lean.Touch.LeanFingerData::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFingerData__ctor_mDCA5B1F4733AAD5F5E033A892321A26B79F47366 (LeanFingerData_t328C450AB89CFDD08B20C08DA6D11DDECAAB2138 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::.ctor()
inline void UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED (UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC * __this, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC *, const RuntimeMethod*))UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED_gshared)(__this, method);
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Collections.Generic.List`1<Lean.Touch.LeanFinger>>::.ctor()
inline void UnityEvent_1__ctor_m71F69F533D6B27B4184EE5F293407A8B945B9221 (UnityEvent_1_t4B73E8A5E7FCB83B3A9ACFDBB9932EF0B4E0B39A * __this, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_t4B73E8A5E7FCB83B3A9ACFDBB9932EF0B4E0B39A *, const RuntimeMethod*))UnityEvent_1__ctor_mD87552C18A41196B69A62A366C8238FC246B151A_gshared)(__this, method);
}
// System.Void UnityEngine.Events.UnityEvent`2<UnityEngine.Vector3,UnityEngine.Vector3>::.ctor()
inline void UnityEvent_2__ctor_mB0024D2F132EC65C8DA83C650ECE1ADA38960E10 (UnityEvent_2_t7498A4B8C5E75A81F4E86F3F28CDC2E585CBE4D5 * __this, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_2_t7498A4B8C5E75A81F4E86F3F28CDC2E585CBE4D5 *, const RuntimeMethod*))UnityEvent_2__ctor_mB0024D2F132EC65C8DA83C650ECE1ADA38960E10_gshared)(__this, method);
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::.ctor()
inline void UnityEvent_1__ctor_m30F443398054B5E3666B3C86E64A5C0FF97D93FF (UnityEvent_1_tB235B5DAD099AC425DC059D10DEB8B97A35E2BBF * __this, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_tB235B5DAD099AC425DC059D10DEB8B97A35E2BBF *, const RuntimeMethod*))UnityEvent_1__ctor_m30F443398054B5E3666B3C86E64A5C0FF97D93FF_gshared)(__this, method);
}
// System.Void UnityEngine.Events.UnityEvent`2<System.Int32,System.Int32>::.ctor()
inline void UnityEvent_2__ctor_m5BD689AC7009890F9F24391FEBF7AE883CFF2B67 (UnityEvent_2_t4466EA6EFC69AB8918D65EEDF0ADB0A9DCC7B8E3 * __this, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_2_t4466EA6EFC69AB8918D65EEDF0ADB0A9DCC7B8E3 *, const RuntimeMethod*))UnityEvent_2__ctor_m5BD689AC7009890F9F24391FEBF7AE883CFF2B67_gshared)(__this, method);
}
// System.Void UnityEngine.Events.UnityEvent`1<Lean.Touch.LeanFinger>::.ctor()
inline void UnityEvent_1__ctor_m9B3C480571121A197B4D6C2035EAF34243A710C0 (UnityEvent_1_t88B7ACE02A62AB7E41FF842AFDDDFA5860884BDA * __this, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_t88B7ACE02A62AB7E41FF842AFDDDFA5860884BDA *, const RuntimeMethod*))UnityEvent_1__ctor_mD87552C18A41196B69A62A366C8238FC246B151A_gshared)(__this, method);
}
// System.Void UnityEngine.Events.UnityEvent`1<Lean.Touch.LeanPickable>::.ctor()
inline void UnityEvent_1__ctor_m7D0885503EB891D82930D434289763AEE7C347C9 (UnityEvent_1_t2B45C3F6635571A8C6D2013E7FB551792B14967A * __this, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_t2B45C3F6635571A8C6D2013E7FB551792B14967A *, const RuntimeMethod*))UnityEvent_1__ctor_mD87552C18A41196B69A62A366C8238FC246B151A_gshared)(__this, method);
}
// System.Void UnityEngine.Events.UnityEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent__ctor_m98D9C5A59898546B23A45388CFACA25F52A9E5A6 (UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::DeltaAngle(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_DeltaAngle_mB1BD0E139ACCAE694968F7D9CB096C60F69CE9FE (float ___current0, float ___target1, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.GameObject>::.ctor()
inline void UnityEvent_1__ctor_mFEC498EC5642F0C80769E4D2E08EA2E3E4D3D0EB (UnityEvent_1_t1DC2DB931FE9E53AEC9A04F4DE9B4F7B469BC78E * __this, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_t1DC2DB931FE9E53AEC9A04F4DE9B4F7B469BC78E *, const RuntimeMethod*))UnityEvent_1__ctor_mD87552C18A41196B69A62A366C8238FC246B151A_gshared)(__this, method);
}
// System.Void UnityEngine.Events.UnityEvent`1<Lean.Touch.IDropHandler>::.ctor()
inline void UnityEvent_1__ctor_mDD42C6C3EB41D2D99D65F39E70F578E0740B4495 (UnityEvent_1_t2F5AE12EB5154F02C654F49C1DFD43714173A0C3 * __this, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_t2F5AE12EB5154F02C654F49C1DFD43714173A0C3 *, const RuntimeMethod*))UnityEvent_1__ctor_mD87552C18A41196B69A62A366C8238FC246B151A_gshared)(__this, method);
}
// System.Void UnityEngine.Events.UnityEvent`1<Lean.Common.LeanSelectable>::.ctor()
inline void UnityEvent_1__ctor_m3699DCBD7E56C12E160DD6F866F75F233240D828 (UnityEvent_1_t3102238E497F1A03968983BDEFDB53CA8754BA66 * __this, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_t3102238E497F1A03968983BDEFDB53CA8754BA66 *, const RuntimeMethod*))UnityEvent_1__ctor_mD87552C18A41196B69A62A366C8238FC246B151A_gshared)(__this, method);
}
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Count()
inline int32_t List_1_get_Count_mD6469330B5381EC92DDF375773100F6372E6114C_inline (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *, const RuntimeMethod*))List_1_get_Count_mD6469330B5381EC92DDF375773100F6372E6114C_gshared_inline)(__this, method);
}
// !0 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Item(System.Int32)
inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  List_1_get_Item_m3E1AEDD64868D9F6901AFBF0FA6B0A7A0001BA1E_inline (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  (*) (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *, int32_t, const RuntimeMethod*))List_1_get_Item_m3E1AEDD64868D9F6901AFBF0FA6B0A7A0001BA1E_gshared_inline)(__this, ___index0, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor()
inline void List_1__ctor_m0ED1CF21DC6B8863BF9FB9E9CE08331E11258F53 (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *, const RuntimeMethod*))List_1__ctor_m0ED1CF21DC6B8863BF9FB9E9CE08331E11258F53_gshared)(__this, method);
}
// System.Single UnityEngine.Vector2::Distance(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector2_Distance_m7DFAD110E57AF0E903DDC47BDBD99D1CC62EA03F (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___b1, const RuntimeMethod* method);
// System.Single Lean.Touch.LeanShapeDetector/Line::GetFirstDistance(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Line_GetFirstDistance_m9890016A67A214E24E791787A493B349D61B3D47 (Line_tD54BCC2D376196864D00E9AEF7D66C042AFE3003 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___point0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Vector2::op_Equality(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector2_op_Equality_mAE5F31E8419538F0F6AF19D9897E0BE1CE8DB1B0_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___lhs0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rhs1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Subtraction_m6E536A8C72FEAA37FF8D5E26E11D6E71EB59599A_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___b1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector2::Dot(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector2_Dot_mB2DFFDDA2881BA755F0B75CB530A39E8EBE70B48_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___lhs0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rhs1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(System.Single,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Multiply_m841D5292C48DAD9746A2F4EED9CE7A76CDB652EA_inline (float ___d0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Addition_m5EACC2AEA80FEE29F380397CF1F4B11D04BE71CC_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___b1, const RuntimeMethod* method);
// System.Single Lean.Touch.LeanShapeDetector/Line::GetDistance(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Line_GetDistance_mC5BE3BE21A97308070621AD7FE6210404D551731 (Line_tD54BCC2D376196864D00E9AEF7D66C042AFE3003 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___point0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanLastUp/Vector2Event::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector2Event__ctor_mE437ACD2C37A47415016CB9E683E6F0B8076CDE0 (Vector2Event_t76245A810A59CF029CB90FB6C09F13CF673F6A16 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_mF2353BD6855BD9E925E30E1CD4BC8582182DE0C7_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_mF2353BD6855BD9E925E30E1CD4BC8582182DE0C7(__this, /*hidden argument*/UnityEvent_1__ctor_mF2353BD6855BD9E925E30E1CD4BC8582182DE0C7_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanLastUp/Vector3Event::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector3Event__ctor_m90633122E5BEAFDDC897F1F574A2DC9DFA50970F (Vector3Event_t6497ADA553FB0219F72FDAB1B28B8DA6CA0CCF98 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8(__this, /*hidden argument*/UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanManualFlick/FingerData::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FingerData__ctor_mD311A6891E83D6137A088835C47A50BE643C3CED (FingerData_tA693309440FFF62DF1318F31DF9DE6D60CA78599 * __this, const RuntimeMethod* method)
{
	{
		LeanFingerData__ctor_mDCA5B1F4733AAD5F5E033A892321A26B79F47366(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanMouseWheel/FloatEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FloatEvent__ctor_m4045C9F75FE81B33436A051E04419DEB1BA0FC33 (FloatEvent_t558EF23D9ACA2E234F374A076572700163CBC4EC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED(__this, /*hidden argument*/UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanMultiDirection/FloatEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FloatEvent__ctor_m9B3015223FB628FA6DF8DECDAC5919598322174F (FloatEvent_tC461FC41ED59B2878728F92A3326F7E5301E66A5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED(__this, /*hidden argument*/UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanMultiDown/LeanFingerListEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFingerListEvent__ctor_m8204585BF37E50623E14FA4A39E55DFB457ECF98 (LeanFingerListEvent_t4274EF1678712C379AE3443B84AE74F3E830511F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_m71F69F533D6B27B4184EE5F293407A8B945B9221_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m71F69F533D6B27B4184EE5F293407A8B945B9221(__this, /*hidden argument*/UnityEvent_1__ctor_m71F69F533D6B27B4184EE5F293407A8B945B9221_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanMultiDown/Vector2Event::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector2Event__ctor_m177A0E2F3260A2AE9E9B6208F4634680DB518CEC (Vector2Event_t49301FE0AF6F2860157E77CE73C11BB7E1899F74 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_mF2353BD6855BD9E925E30E1CD4BC8582182DE0C7_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_mF2353BD6855BD9E925E30E1CD4BC8582182DE0C7(__this, /*hidden argument*/UnityEvent_1__ctor_mF2353BD6855BD9E925E30E1CD4BC8582182DE0C7_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanMultiDown/Vector3Event::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector3Event__ctor_m97468BA6E523A6D1536FC38520D78F8C3F30EB07 (Vector3Event_t926F35A94CB9A9A4948079F9BF7611A04F771567 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8(__this, /*hidden argument*/UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanMultiHeld/FingerData::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FingerData__ctor_mA9DD5E40BE6AAF8BFF0BDD76CDAB91C5E8AC0985 (FingerData_t3B100938E369689AC520A735775E2551358BCDD9 * __this, const RuntimeMethod* method)
{
	{
		LeanFingerData__ctor_mDCA5B1F4733AAD5F5E033A892321A26B79F47366(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanMultiHeld/LeanFingerListEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFingerListEvent__ctor_m94FBECE94E0BB95C1E226E908CDCE0AEF5A541A1 (LeanFingerListEvent_t5C3C6B04068B5F941FCAEB2120B2268BA24943BF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_m71F69F533D6B27B4184EE5F293407A8B945B9221_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m71F69F533D6B27B4184EE5F293407A8B945B9221(__this, /*hidden argument*/UnityEvent_1__ctor_m71F69F533D6B27B4184EE5F293407A8B945B9221_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanMultiHeld/Vector2Event::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector2Event__ctor_mBC7EA484E139C93839A4C1655F0CE7BAB2389BB3 (Vector2Event_t859DB0998D957E7C474DF5D40EE4994506BCA526 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_mF2353BD6855BD9E925E30E1CD4BC8582182DE0C7_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_mF2353BD6855BD9E925E30E1CD4BC8582182DE0C7(__this, /*hidden argument*/UnityEvent_1__ctor_mF2353BD6855BD9E925E30E1CD4BC8582182DE0C7_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanMultiHeld/Vector3Event::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector3Event__ctor_mAEE74001980C33AD99AA240E073A0F12F2B5CCA1 (Vector3Event_tD2E3AAB5429575C1AC5102F1A5B497E4FCF822EF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8(__this, /*hidden argument*/UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanMultiPinch/FloatEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FloatEvent__ctor_m910CE5D3BE2A3DCEFA6CE1D703284732E615E656 (FloatEvent_tE4250CE5F1D5FCFEE7513ABEC1197997DD8A6D3C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED(__this, /*hidden argument*/UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanMultiPull/FloatEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FloatEvent__ctor_mF15EDE985E0976EDB7D781B74792093901C0C68D (FloatEvent_t52EA07CE1AEFF9D36CE3DFF7DC7B022F22DD314A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED(__this, /*hidden argument*/UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanMultiPull/Vector2Event::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector2Event__ctor_mBCDC9F6B503D0E4B630539787D357D423FD4A87A (Vector2Event_t6F04BE5FB03CD574DA62371DC09BFE114668BC19 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_mF2353BD6855BD9E925E30E1CD4BC8582182DE0C7_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_mF2353BD6855BD9E925E30E1CD4BC8582182DE0C7(__this, /*hidden argument*/UnityEvent_1__ctor_mF2353BD6855BD9E925E30E1CD4BC8582182DE0C7_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanMultiPull/Vector3Event::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector3Event__ctor_m59E022854A40CE20ACF20D90A21A9BE4FD19DAC8 (Vector3Event_tFC38367080297930D0E4319CD5CEEBDCAFEC063C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8(__this, /*hidden argument*/UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanMultiPull/Vector3Vector3Event::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector3Vector3Event__ctor_m411E7090968D0F493BD715BBEE3AE1F1D0D6B1BD (Vector3Vector3Event_t02A8D156358087F3EF5F9603BF6586CBDD1E586D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_2__ctor_mB0024D2F132EC65C8DA83C650ECE1ADA38960E10_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_2__ctor_mB0024D2F132EC65C8DA83C650ECE1ADA38960E10(__this, /*hidden argument*/UnityEvent_2__ctor_mB0024D2F132EC65C8DA83C650ECE1ADA38960E10_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanMultiSwipe/FingerListEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FingerListEvent__ctor_m557EF0EE2E4A43FFD59E9EB9E2D43C6762751643 (FingerListEvent_t4D146A7DA4C2A74F61B251377A883515B6E530BB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_m71F69F533D6B27B4184EE5F293407A8B945B9221_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m71F69F533D6B27B4184EE5F293407A8B945B9221(__this, /*hidden argument*/UnityEvent_1__ctor_m71F69F533D6B27B4184EE5F293407A8B945B9221_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanMultiSwipe/FloatEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FloatEvent__ctor_m398D1FEF3DF599A239130BFDEBD8611EAAE056B4 (FloatEvent_t6379D4D73A5BE4770B5C0EE9F9AF2D26B2186EED * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED(__this, /*hidden argument*/UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanMultiSwipe/Vector2Event::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector2Event__ctor_m61CDFD9F8992A503193ACC868E0A55C207711BFF (Vector2Event_t3F4B11D56D2AB9502E93CA635ACC38C9BCB7BBE1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_mF2353BD6855BD9E925E30E1CD4BC8582182DE0C7_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_mF2353BD6855BD9E925E30E1CD4BC8582182DE0C7(__this, /*hidden argument*/UnityEvent_1__ctor_mF2353BD6855BD9E925E30E1CD4BC8582182DE0C7_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanMultiTap/IntEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IntEvent__ctor_m5F9F1EE32595F5E2929C17087688143669160515 (IntEvent_t95BD74EA4CD3ACB376B2125294F26D7D61C9E53A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_m30F443398054B5E3666B3C86E64A5C0FF97D93FF_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m30F443398054B5E3666B3C86E64A5C0FF97D93FF(__this, /*hidden argument*/UnityEvent_1__ctor_m30F443398054B5E3666B3C86E64A5C0FF97D93FF_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanMultiTap/IntIntEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IntIntEvent__ctor_mE2AB6A560FAF5F5BF13F7DE7290A0ED41C812729 (IntIntEvent_t826C1A3699D71EEA932BE16511E6057FD5860A90 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_2__ctor_m5BD689AC7009890F9F24391FEBF7AE883CFF2B67_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_2__ctor_m5BD689AC7009890F9F24391FEBF7AE883CFF2B67(__this, /*hidden argument*/UnityEvent_2__ctor_m5BD689AC7009890F9F24391FEBF7AE883CFF2B67_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanMultiTwist/FloatEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FloatEvent__ctor_m9BF1D544D19B7F60F6E74C18B806C3CE953CF6AA (FloatEvent_t94A4076DD66B100CD6C8D2A2EA33BCA0C65CACE9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED(__this, /*hidden argument*/UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanMultiUp/LeanFingerEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFingerEvent__ctor_mA74984CF8AC970A9B777BC34547A463D734FD7CC (LeanFingerEvent_tCB1725CAE676B4DB695050D9A1F389AEDC375286 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_m9B3C480571121A197B4D6C2035EAF34243A710C0_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m9B3C480571121A197B4D6C2035EAF34243A710C0(__this, /*hidden argument*/UnityEvent_1__ctor_m9B3C480571121A197B4D6C2035EAF34243A710C0_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanMultiUp/Vector2Event::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector2Event__ctor_m358ED9AC5DC1A59BED43B0874A47E01FEBB68494 (Vector2Event_t8B6CC82A61F3431DBC6D5323888D34ADB5F618FD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_mF2353BD6855BD9E925E30E1CD4BC8582182DE0C7_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_mF2353BD6855BD9E925E30E1CD4BC8582182DE0C7(__this, /*hidden argument*/UnityEvent_1__ctor_mF2353BD6855BD9E925E30E1CD4BC8582182DE0C7_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanMultiUp/Vector3Event::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector3Event__ctor_m40E805E5E6B2F7CABFE63CF18942D60D59795DEB (Vector3Event_tD484F3AD9A24C43ACA2065B92BC76955724B7B88 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8(__this, /*hidden argument*/UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanMultiUpdate/FloatEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FloatEvent__ctor_m249A50B149BB51315E91B12065DEB77246EC037B (FloatEvent_t0C1E2C304AB9CBCCB0986165186C8CEA51B78A6E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED(__this, /*hidden argument*/UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanMultiUpdate/LeanFingerListEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFingerListEvent__ctor_mC23F299CA296A2DD3031D2F76C6BD4A62AA4A55E (LeanFingerListEvent_tE5C9A18477AA0141882F033112952492BD42A5B2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_m71F69F533D6B27B4184EE5F293407A8B945B9221_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m71F69F533D6B27B4184EE5F293407A8B945B9221(__this, /*hidden argument*/UnityEvent_1__ctor_m71F69F533D6B27B4184EE5F293407A8B945B9221_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanMultiUpdate/Vector2Event::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector2Event__ctor_mFB25A50F057AB4F0FDB4EC5DF853E38A47B542F8 (Vector2Event_t16FA65D6F9EDD3E67DE796CEF2A0E58D80B116EB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_mF2353BD6855BD9E925E30E1CD4BC8582182DE0C7_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_mF2353BD6855BD9E925E30E1CD4BC8582182DE0C7(__this, /*hidden argument*/UnityEvent_1__ctor_mF2353BD6855BD9E925E30E1CD4BC8582182DE0C7_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanMultiUpdate/Vector3Event::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector3Event__ctor_mFA76270CFBD1A2DFB78DD89ED0F34FA8DE833951 (Vector3Event_tBC3694D74EF4542CAF05A69FD280A1B0C8195378 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8(__this, /*hidden argument*/UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanMultiUpdate/Vector3Vector3Event::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector3Vector3Event__ctor_m03B321D0D6C1099D51B6FD8A33EF905DC54396DC (Vector3Vector3Event_t71C436ADE3DCA9F9BFFE4412D56323AFC3603BC5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_2__ctor_mB0024D2F132EC65C8DA83C650ECE1ADA38960E10_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_2__ctor_mB0024D2F132EC65C8DA83C650ECE1ADA38960E10(__this, /*hidden argument*/UnityEvent_2__ctor_mB0024D2F132EC65C8DA83C650ECE1ADA38960E10_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanMultiUpdateCanvas/LeanFingerListEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFingerListEvent__ctor_m07768BB1E8A58FE1284BFFFE5C2EAFA8D1E6ED77 (LeanFingerListEvent_t41AD12C7D8558553948AE6F468DDDAC9231F7E60 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_m71F69F533D6B27B4184EE5F293407A8B945B9221_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m71F69F533D6B27B4184EE5F293407A8B945B9221(__this, /*hidden argument*/UnityEvent_1__ctor_m71F69F533D6B27B4184EE5F293407A8B945B9221_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanMultiUpdateCanvas/Vector3Event::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector3Event__ctor_mAD5A428D75F7A17D8C2D5856835D75580CEF2B39 (Vector3Event_t3D07A493F09E5436AA7E40C33B9ECCEF5E5ADDC7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8(__this, /*hidden argument*/UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanPick/LeanPickableEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanPickableEvent__ctor_m0B53AEC2E4426AE30F4D3B78ABBBDAB4FEFF2976 (LeanPickableEvent_t2035637C5C5B3B1D78CF2AA6789D51AC0CF4FF24 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_m7D0885503EB891D82930D434289763AEE7C347C9_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m7D0885503EB891D82930D434289763AEE7C347C9(__this, /*hidden argument*/UnityEvent_1__ctor_m7D0885503EB891D82930D434289763AEE7C347C9_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanPickable/LeanFingerEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFingerEvent__ctor_m5C900BFC8AC2FAE876F7887922CE0FAA17529EFC (LeanFingerEvent_t537C4971FFFDB4E936119E39123CECCD8A09BD27 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_m9B3C480571121A197B4D6C2035EAF34243A710C0_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m9B3C480571121A197B4D6C2035EAF34243A710C0(__this, /*hidden argument*/UnityEvent_1__ctor_m9B3C480571121A197B4D6C2035EAF34243A710C0_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanPickable/Vector2Event::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector2Event__ctor_mC83B1971D763150A0A7927A3E8C231C3E867B28C (Vector2Event_t11759D6BE15AF79E12606AD28585BD610E4A488E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_mF2353BD6855BD9E925E30E1CD4BC8582182DE0C7_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_mF2353BD6855BD9E925E30E1CD4BC8582182DE0C7(__this, /*hidden argument*/UnityEvent_1__ctor_mF2353BD6855BD9E925E30E1CD4BC8582182DE0C7_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanPickable/Vector3Event::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector3Event__ctor_m0227F10D52B05CADFB936D78E4E597097B7D0585 (Vector3Event_tBA87028292EB78380A5443330E1BFAFE6F6252C0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8(__this, /*hidden argument*/UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanSelectableCenter/Vector3Event::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector3Event__ctor_mBD048EDE9910FBCE2F98C291B11F6B656FF850DD (Vector3Event_t5BB81343A5679B4A5ACA81B17B6FF70CFF69622E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8(__this, /*hidden argument*/UnityEvent_1__ctor_mC58365ECDF3275B845B1D16FAACD109627B507F8_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanSelectableCount/IntEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IntEvent__ctor_mFFD43B9FFEE825DE8A6D10174B91CCC4448726B8 (IntEvent_t575CCDFCECE14C6B0AC894D6292A63C0347021C0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_m30F443398054B5E3666B3C86E64A5C0FF97D93FF_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m30F443398054B5E3666B3C86E64A5C0FF97D93FF(__this, /*hidden argument*/UnityEvent_1__ctor_m30F443398054B5E3666B3C86E64A5C0FF97D93FF_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanSelectableDial/FloatEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FloatEvent__ctor_m81F86D5D9FB25AB91E75CBDDEA5D955D2A8673E6 (FloatEvent_tDCF2F2F45E0094C75F402722A071C7039CC429E0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED(__this, /*hidden argument*/UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Events.UnityEvent Lean.Touch.LeanSelectableDial/Trigger::get_OnEnter()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * Trigger_get_OnEnter_mB078BDAB6F8EDCD7C32E031BEA126D8E8344C3C5 (Trigger_tCDE0F6503E25FA1F39A693069031F6D3CA0BBC2A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public UnityEvent OnEnter { get { if (onEnter == null) onEnter = new UnityEvent(); return onEnter; } } [SerializeField] private UnityEvent onEnter;
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_0 = __this->get_onEnter_3();
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		// public UnityEvent OnEnter { get { if (onEnter == null) onEnter = new UnityEvent(); return onEnter; } } [SerializeField] private UnityEvent onEnter;
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_1 = (UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 *)il2cpp_codegen_object_new(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4_il2cpp_TypeInfo_var);
		UnityEvent__ctor_m98D9C5A59898546B23A45388CFACA25F52A9E5A6(L_1, /*hidden argument*/NULL);
		__this->set_onEnter_3(L_1);
	}

IL_0013:
	{
		// public UnityEvent OnEnter { get { if (onEnter == null) onEnter = new UnityEvent(); return onEnter; } } [SerializeField] private UnityEvent onEnter;
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_2 = __this->get_onEnter_3();
		return L_2;
	}
}
// UnityEngine.Events.UnityEvent Lean.Touch.LeanSelectableDial/Trigger::get_OnExit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * Trigger_get_OnExit_mA51B9884CADD3C8285FFA3B80A427FAB0B042E0C (Trigger_tCDE0F6503E25FA1F39A693069031F6D3CA0BBC2A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public UnityEvent OnExit { get { if (onExit == null) onExit = new UnityEvent(); return onExit; } } [SerializeField] private UnityEvent onExit;
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_0 = __this->get_onExit_4();
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		// public UnityEvent OnExit { get { if (onExit == null) onExit = new UnityEvent(); return onExit; } } [SerializeField] private UnityEvent onExit;
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_1 = (UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 *)il2cpp_codegen_object_new(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4_il2cpp_TypeInfo_var);
		UnityEvent__ctor_m98D9C5A59898546B23A45388CFACA25F52A9E5A6(L_1, /*hidden argument*/NULL);
		__this->set_onExit_4(L_1);
	}

IL_0013:
	{
		// public UnityEvent OnExit { get { if (onExit == null) onExit = new UnityEvent(); return onExit; } } [SerializeField] private UnityEvent onExit;
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_2 = __this->get_onExit_4();
		return L_2;
	}
}
// System.Boolean Lean.Touch.LeanSelectableDial/Trigger::IsInside(System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Trigger_IsInside_m5907DCDCAB9FC3F07A2539547C80A13E9ADE96F8 (Trigger_tCDE0F6503E25FA1F39A693069031F6D3CA0BBC2A * __this, float ___angle0, bool ___clamp1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		// var range = Arc * 0.5f;
		float L_0 = __this->get_Arc_1();
		V_0 = ((float)il2cpp_codegen_multiply((float)L_0, (float)(0.5f)));
		// if (clamp == false)
		bool L_1 = ___clamp1;
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		// var delta  = Mathf.Abs(Mathf.DeltaAngle(Angle, angle));
		float L_2 = __this->get_Angle_0();
		float L_3 = ___angle0;
		float L_4;
		L_4 = Mathf_DeltaAngle_mB1BD0E139ACCAE694968F7D9CB096C60F69CE9FE(L_2, L_3, /*hidden argument*/NULL);
		float L_5;
		L_5 = fabsf(L_4);
		// return delta < range;
		float L_6 = V_0;
		return (bool)((((float)L_5) < ((float)L_6))? 1 : 0);
	}

IL_0025:
	{
		// return angle >= Angle - range && angle <= Angle + range;
		float L_7 = ___angle0;
		float L_8 = __this->get_Angle_0();
		float L_9 = V_0;
		if ((!(((float)L_7) >= ((float)((float)il2cpp_codegen_subtract((float)L_8, (float)L_9))))))
		{
			goto IL_003f;
		}
	}
	{
		float L_10 = ___angle0;
		float L_11 = __this->get_Angle_0();
		float L_12 = V_0;
		return (bool)((((int32_t)((!(((float)L_10) <= ((float)((float)il2cpp_codegen_add((float)L_11, (float)L_12)))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_003f:
	{
		return (bool)0;
	}
}
// System.Void Lean.Touch.LeanSelectableDial/Trigger::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Trigger__ctor_m96E85FA706A948FC40388F7C242453E6D16B4E71 (Trigger_tCDE0F6503E25FA1F39A693069031F6D3CA0BBC2A * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanSelectableDrop/GameObjectEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObjectEvent__ctor_m5DF4A4A01416DFFDDA8B9326E1CF822DCD96F46B (GameObjectEvent_t96555E9F187AAEE2755021672C67B96E1E49BE7A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_mFEC498EC5642F0C80769E4D2E08EA2E3E4D3D0EB_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_mFEC498EC5642F0C80769E4D2E08EA2E3E4D3D0EB(__this, /*hidden argument*/UnityEvent_1__ctor_mFEC498EC5642F0C80769E4D2E08EA2E3E4D3D0EB_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanSelectableDrop/IDropHandlerEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IDropHandlerEvent__ctor_m9E6C867F445ADBAF24F5CE713712795D48140653 (IDropHandlerEvent_t7FAACF71D3C7DCA66B5941CBFAA7D973EB8C6A50 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_mDD42C6C3EB41D2D99D65F39E70F578E0740B4495_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_mDD42C6C3EB41D2D99D65F39E70F578E0740B4495(__this, /*hidden argument*/UnityEvent_1__ctor_mDD42C6C3EB41D2D99D65F39E70F578E0740B4495_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanSelectableSelected/SelectableEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SelectableEvent__ctor_m919E20AC6C5B01AB3571D1EC0D3306215703BBCE (SelectableEvent_t19EF1FD7E2CD83A5F68C5CC39BF06A8150F9E57F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_m3699DCBD7E56C12E160DD6F866F75F233240D828_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m3699DCBD7E56C12E160DD6F866F75F233240D828(__this, /*hidden argument*/UnityEvent_1__ctor_m3699DCBD7E56C12E160DD6F866F75F233240D828_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanSelectableTime/FloatEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FloatEvent__ctor_m9EADF2017C4D485563D1029979BC5A633594F5F9 (FloatEvent_t6007A8BA6CFE0E63BB997AB057E0559347222539 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED(__this, /*hidden argument*/UnityEvent_1__ctor_m246DC0A35C4C4D26AD4DCE093E231B72C66C86ED_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanSelectionBox/FingerData::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FingerData__ctor_m66A10889E5B66034D798A491359A21164C3FEC77 (FingerData_t6E16DC6ACC7AFF5E7729EE7CC3D25085C9C4D2D9 * __this, const RuntimeMethod* method)
{
	{
		LeanFingerData__ctor_mDCA5B1F4733AAD5F5E033A892321A26B79F47366(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector2 Lean.Touch.LeanShapeDetector/FingerData::get_EndPoint()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  FingerData_get_EndPoint_mD863A5CE572F621AEDB7C431E4A01C6C0837ED22 (FingerData_tCB056267858269BE48D3C3FE2D5259007930BD10 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mD6469330B5381EC92DDF375773100F6372E6114C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m3E1AEDD64868D9F6901AFBF0FA6B0A7A0001BA1E_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Points[Points.Count - 1];
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_0 = __this->get_Points_1();
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_1 = __this->get_Points_1();
		NullCheck(L_1);
		int32_t L_2;
		L_2 = List_1_get_Count_mD6469330B5381EC92DDF375773100F6372E6114C_inline(L_1, /*hidden argument*/List_1_get_Count_mD6469330B5381EC92DDF375773100F6372E6114C_RuntimeMethod_var);
		NullCheck(L_0);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_3;
		L_3 = List_1_get_Item_m3E1AEDD64868D9F6901AFBF0FA6B0A7A0001BA1E_inline(L_0, ((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)1)), /*hidden argument*/List_1_get_Item_m3E1AEDD64868D9F6901AFBF0FA6B0A7A0001BA1E_RuntimeMethod_var);
		return L_3;
	}
}
// System.Void Lean.Touch.LeanShapeDetector/FingerData::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FingerData__ctor_mFDCE2D7B0EE44BE6B628A4DF5D81B1A5F15F35B3 (FingerData_tCB056267858269BE48D3C3FE2D5259007930BD10 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m0ED1CF21DC6B8863BF9FB9E9CE08331E11258F53_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<Vector2> Points = new List<Vector2>(); // This stores the current shape this finger has drawn.
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_0 = (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *)il2cpp_codegen_object_new(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9_il2cpp_TypeInfo_var);
		List_1__ctor_m0ED1CF21DC6B8863BF9FB9E9CE08331E11258F53(L_0, /*hidden argument*/List_1__ctor_m0ED1CF21DC6B8863BF9FB9E9CE08331E11258F53_RuntimeMethod_var);
		__this->set_Points_1(L_0);
		LeanFingerData__ctor_mDCA5B1F4733AAD5F5E033A892321A26B79F47366(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanShapeDetector/LeanFingerEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFingerEvent__ctor_m464DB7B86BADBC50488DD963D2666D5CE8B4E4DA (LeanFingerEvent_t3C3407D8CC16780A7CAEAE80DC13A2D81E810333 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_m9B3C480571121A197B4D6C2035EAF34243A710C0_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m9B3C480571121A197B4D6C2035EAF34243A710C0(__this, /*hidden argument*/UnityEvent_1__ctor_m9B3C480571121A197B4D6C2035EAF34243A710C0_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single Lean.Touch.LeanShapeDetector/Line::GetFirstDistance(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Line_GetFirstDistance_m9890016A67A214E24E791787A493B349D61B3D47 (Line_tD54BCC2D376196864D00E9AEF7D66C042AFE3003 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___point0, const RuntimeMethod* method)
{
	{
		// return Vector2.Distance(point, A);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___point0;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_1 = __this->get_A_0();
		float L_2;
		L_2 = Vector2_Distance_m7DFAD110E57AF0E903DDC47BDBD99D1CC62EA03F(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
IL2CPP_EXTERN_C  float Line_GetFirstDistance_m9890016A67A214E24E791787A493B349D61B3D47_AdjustorThunk (RuntimeObject * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___point0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	Line_tD54BCC2D376196864D00E9AEF7D66C042AFE3003 * _thisAdjusted = reinterpret_cast<Line_tD54BCC2D376196864D00E9AEF7D66C042AFE3003 *>(__this + _offset);
	float _returnValue;
	_returnValue = Line_GetFirstDistance_m9890016A67A214E24E791787A493B349D61B3D47(_thisAdjusted, ___point0, method);
	return _returnValue;
}
// System.Single Lean.Touch.LeanShapeDetector/Line::GetDistance(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Line_GetDistance_mC5BE3BE21A97308070621AD7FE6210404D551731 (Line_tD54BCC2D376196864D00E9AEF7D66C042AFE3003 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___point0, const RuntimeMethod* method)
{
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_4;
	memset((&V_4), 0, sizeof(V_4));
	{
		// if (A == B) return Vector2.Distance(point, A);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = __this->get_A_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_1 = __this->get_B_1();
		bool L_2;
		L_2 = Vector2_op_Equality_mAE5F31E8419538F0F6AF19D9897E0BE1CE8DB1B0_inline(L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		// if (A == B) return Vector2.Distance(point, A);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_3 = ___point0;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4 = __this->get_A_0();
		float L_5;
		L_5 = Vector2_Distance_m7DFAD110E57AF0E903DDC47BDBD99D1CC62EA03F(L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0020:
	{
		// var v  = B - A;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6 = __this->get_B_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_7 = __this->get_A_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_8;
		L_8 = Vector2_op_Subtraction_m6E536A8C72FEAA37FF8D5E26E11D6E71EB59599A_inline(L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		// var w  = point - A;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_9 = ___point0;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_10 = __this->get_A_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_11;
		L_11 = Vector2_op_Subtraction_m6E536A8C72FEAA37FF8D5E26E11D6E71EB59599A_inline(L_9, L_10, /*hidden argument*/NULL);
		// var c1 = Vector2.Dot(w,v); if (c1 <= 0.0f) return Vector2.Distance(point, A);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_12 = V_0;
		float L_13;
		L_13 = Vector2_Dot_mB2DFFDDA2881BA755F0B75CB530A39E8EBE70B48_inline(L_11, L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		// var c1 = Vector2.Dot(w,v); if (c1 <= 0.0f) return Vector2.Distance(point, A);
		float L_14 = V_1;
		if ((!(((float)L_14) <= ((float)(0.0f)))))
		{
			goto IL_005a;
		}
	}
	{
		// var c1 = Vector2.Dot(w,v); if (c1 <= 0.0f) return Vector2.Distance(point, A);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_15 = ___point0;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_16 = __this->get_A_0();
		float L_17;
		L_17 = Vector2_Distance_m7DFAD110E57AF0E903DDC47BDBD99D1CC62EA03F(L_15, L_16, /*hidden argument*/NULL);
		return L_17;
	}

IL_005a:
	{
		// var c2 = Vector2.Dot(v,v); if (c2 <= c1) return Vector2.Distance(point, B);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_18 = V_0;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_19 = V_0;
		float L_20;
		L_20 = Vector2_Dot_mB2DFFDDA2881BA755F0B75CB530A39E8EBE70B48_inline(L_18, L_19, /*hidden argument*/NULL);
		V_2 = L_20;
		// var c2 = Vector2.Dot(v,v); if (c2 <= c1) return Vector2.Distance(point, B);
		float L_21 = V_2;
		float L_22 = V_1;
		if ((!(((float)L_21) <= ((float)L_22))))
		{
			goto IL_0073;
		}
	}
	{
		// var c2 = Vector2.Dot(v,v); if (c2 <= c1) return Vector2.Distance(point, B);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_23 = ___point0;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_24 = __this->get_B_1();
		float L_25;
		L_25 = Vector2_Distance_m7DFAD110E57AF0E903DDC47BDBD99D1CC62EA03F(L_23, L_24, /*hidden argument*/NULL);
		return L_25;
	}

IL_0073:
	{
		// var b  = c1 / c2;
		float L_26 = V_1;
		float L_27 = V_2;
		V_3 = ((float)((float)L_26/(float)L_27));
		// var Pb = A + b * v;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_28 = __this->get_A_0();
		float L_29 = V_3;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_30 = V_0;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_31;
		L_31 = Vector2_op_Multiply_m841D5292C48DAD9746A2F4EED9CE7A76CDB652EA_inline(L_29, L_30, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_32;
		L_32 = Vector2_op_Addition_m5EACC2AEA80FEE29F380397CF1F4B11D04BE71CC_inline(L_28, L_31, /*hidden argument*/NULL);
		V_4 = L_32;
		// return Vector2.Distance(point, Pb);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_33 = ___point0;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_34 = V_4;
		float L_35;
		L_35 = Vector2_Distance_m7DFAD110E57AF0E903DDC47BDBD99D1CC62EA03F(L_33, L_34, /*hidden argument*/NULL);
		return L_35;
	}
}
IL2CPP_EXTERN_C  float Line_GetDistance_mC5BE3BE21A97308070621AD7FE6210404D551731_AdjustorThunk (RuntimeObject * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___point0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	Line_tD54BCC2D376196864D00E9AEF7D66C042AFE3003 * _thisAdjusted = reinterpret_cast<Line_tD54BCC2D376196864D00E9AEF7D66C042AFE3003 *>(__this + _offset);
	float _returnValue;
	_returnValue = Line_GetDistance_mC5BE3BE21A97308070621AD7FE6210404D551731(_thisAdjusted, ___point0, method);
	return _returnValue;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanSpawnWithFinger/FingerData::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FingerData__ctor_m16F99113B9EF9F522810BE3FFC0FD2F0F8B23982 (FingerData_t881CBDF9045D4EF2DFE0E3C8D855E4691F9334A5 * __this, const RuntimeMethod* method)
{
	{
		LeanFingerData__ctor_mDCA5B1F4733AAD5F5E033A892321A26B79F47366(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector2_op_Equality_mAE5F31E8419538F0F6AF19D9897E0BE1CE8DB1B0_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___lhs0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rhs1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	bool V_2 = false;
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2 = ___rhs1;
		float L_3 = L_2.get_x_0();
		V_0 = ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3));
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4 = ___lhs0;
		float L_5 = L_4.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6 = ___rhs1;
		float L_7 = L_6.get_y_1();
		V_1 = ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7));
		float L_8 = V_0;
		float L_9 = V_0;
		float L_10 = V_1;
		float L_11 = V_1;
		V_2 = (bool)((((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_8, (float)L_9)), (float)((float)il2cpp_codegen_multiply((float)L_10, (float)L_11))))) < ((float)(9.99999944E-11f)))? 1 : 0);
		goto IL_002e;
	}

IL_002e:
	{
		bool L_12 = V_2;
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Subtraction_m6E536A8C72FEAA37FF8D5E26E11D6E71EB59599A_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___b1, const RuntimeMethod* method)
{
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___a0;
		float L_1 = L_0.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2 = ___b1;
		float L_3 = L_2.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4 = ___a0;
		float L_5 = L_4.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6 = ___b1;
		float L_7 = L_6.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_8;
		memset((&L_8), 0, sizeof(L_8));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_8), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0023;
	}

IL_0023:
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_9 = V_0;
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector2_Dot_mB2DFFDDA2881BA755F0B75CB530A39E8EBE70B48_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___lhs0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rhs1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2 = ___rhs1;
		float L_3 = L_2.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4 = ___lhs0;
		float L_5 = L_4.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6 = ___rhs1;
		float L_7 = L_6.get_y_1();
		V_0 = ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_1, (float)L_3)), (float)((float)il2cpp_codegen_multiply((float)L_5, (float)L_7))));
		goto IL_001f;
	}

IL_001f:
	{
		float L_8 = V_0;
		return L_8;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Multiply_m841D5292C48DAD9746A2F4EED9CE7A76CDB652EA_inline (float ___d0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a1, const RuntimeMethod* method)
{
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___a1;
		float L_1 = L_0.get_x_0();
		float L_2 = ___d0;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_3 = ___a1;
		float L_4 = L_3.get_y_1();
		float L_5 = ___d0;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_6), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0019;
	}

IL_0019:
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_7 = V_0;
		return L_7;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Addition_m5EACC2AEA80FEE29F380397CF1F4B11D04BE71CC_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___b1, const RuntimeMethod* method)
{
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___a0;
		float L_1 = L_0.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2 = ___b1;
		float L_3 = L_2.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4 = ___a0;
		float L_5 = L_4.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6 = ___b1;
		float L_7 = L_6.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_8;
		memset((&L_8), 0, sizeof(L_8));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_8), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0023;
	}

IL_0023:
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_9 = V_0;
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_mD6469330B5381EC92DDF375773100F6372E6114C_gshared_inline (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  List_1_get_Item_m3E1AEDD64868D9F6901AFBF0FA6B0A7A0001BA1E_gshared_inline (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* L_2 = (Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA*)__this->get__items_1();
		int32_t L_3 = ___index0;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA*)L_2, (int32_t)L_3);
		return (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 )L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		return;
	}
}
