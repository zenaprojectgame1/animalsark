﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class MgAnimationUnity : MgAnimation
{
    [SerializeField] float customDelay;
    [SerializeField] string showAnim_paramName = "show";
    [SerializeField] string hideAnim_paramName = "hide";
    Animator m_Animator;

    protected override void Awake()
    {
        m_Animator = GetComponent<Animator>();
        //base.Awake();
    }

    public override void Show()
    {
        base.Show();
        StartCoroutine(CoShow());
    }

    IEnumerator CoShow()
    {
        yield return new WaitForSeconds(customDelay);
        //Debug.Log(m_Animator);
        m_Animator.SetBool(showAnim_paramName, true);
        m_Animator.SetBool(hideAnim_paramName, false);
    }

    public override void Hide(bool immediately = false)
    {
        base.Hide(immediately);
        m_Animator.SetBool(showAnim_paramName, false);
        m_Animator.SetBool(hideAnim_paramName, true);
        //StartCoroutine(CoHide());
    }

    //IEnumerator CoHide()
    //{
    //    yield return new WaitForSeconds(MgAnimation.ANIM_DURATION + customDelay);
    //    m_Animator.SetBool(showAnim_paramName, false);
    //}
}
