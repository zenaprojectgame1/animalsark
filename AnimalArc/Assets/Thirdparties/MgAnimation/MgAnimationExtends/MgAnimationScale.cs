﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MgAnimationScale : MgAnimation
{
    public override void Show()
    {
        var rt = GetComponent<RectTransform>();
        rt.DOKill();

        if (gameObject.activeInHierarchy)
        {
            rt.DOScale(Vector3.one, MgAnimation.ANIM_DUR).SetEase(Ease.OutBack);
        }
        else
        {
            rt.localScale = Vector3.one;
        }
    }

    public override void Hide(bool immediately = false)
    {
        var rt = GetComponent<RectTransform>();
        rt.DOKill();

        if (immediately)
        {
            rt.localScale = Vector3.zero;
        }
        else
        {
            rt.DOScale(Vector3.zero, MgAnimation.ANIM_DUR).SetEase(Ease.InBack);
        }
    }
}
