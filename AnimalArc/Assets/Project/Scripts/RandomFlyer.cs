using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomFlyer : MonoBehaviour
{
    private Vector3[] positionPoint = new Vector3[5];
    [Range(0, 1)]
    public float value;
    public int speed;
    // Start is called before the first frame update
    private Vector3 moveSpot1;
    private Vector3 moveSpot2;
    private Vector3 moveSpot3;
    private Vector3 moveSpot4;
    private Vector3 moveSpot5;
    public float minX;
    public float maxX;
    public float minY;
    public float maxY;
    public float minZ;
    public float maxZ;

    public GameObject particle;
    public ParticleSystem particleTime;
    private Vector3 lTemp;
    void Start()
    {
        moveSpot2 = new Vector3(Random.Range(minX, maxX), Random.Range(minX, maxY), Random.Range(minZ, maxZ - 1));
        moveSpot3 = new Vector3(Random.Range(minX, maxX), Random.Range(minX, maxY), Random.Range(minZ, maxZ - 1));
        moveSpot4 = new Vector3(Random.Range(minX, maxX), Random.Range(minX, maxY), Random.Range(minZ, maxZ - 1));
        moveSpot5 = new Vector3(Random.Range(minX, maxX), Random.Range(minX, maxY), Random.Range(minZ, maxZ - 1));
        moveSpot1 = this.gameObject.transform.position;
        positionPoint[0] = moveSpot1;
        positionPoint[1] = moveSpot2;
        positionPoint[2] = moveSpot3;
        positionPoint[3] = moveSpot4;
        positionPoint[4] = moveSpot5;
        lTemp = transform.localScale;

    }
    float tempTime;
    // Update is called once per frame
    void Update()
    {
        if (value < 1)
        {
            value += Time.deltaTime / speed;
        }
        else
        {
            moveSpot1 = this.gameObject.transform.position;
            moveSpot2 = new Vector3(Random.Range(minX, maxX), Random.Range(minX, maxY), Random.Range(minZ, maxZ-1));
            moveSpot3 = new Vector3(Random.Range(minX, maxX), Random.Range(minX, maxY), Random.Range(minZ, maxZ-1));
            moveSpot4 = new Vector3(Random.Range(minX, maxX), Random.Range(minX, maxY), Random.Range(minZ, maxZ-1));
            moveSpot5 = new Vector3(Random.Range(minX, maxX), Random.Range(minX, maxY), Random.Range(minZ, maxZ-1));
            positionPoint[0] = moveSpot1;
            positionPoint[1] = moveSpot2;
            positionPoint[2] = moveSpot3;
            positionPoint[3] = moveSpot4;
            positionPoint[4] = moveSpot5;
            value = 0;
        }
        iTween.PutOnPath(this.gameObject, positionPoint, value);
        iTween.LookTo(this.gameObject, iTween.Hash(
            "looktarget", positionPoint[4],
            "time", 1,
            "easetype", iTween.EaseType.linear
            ));

        //lTemp.x = 0.4f * (maxZ - transform.position.z);
        //lTemp.y = 0.4f * (maxZ - transform.position.z);
        //lTemp.z = 0.4f * (maxZ - transform.position.z);
        //transform.localScale = lTemp;

    }

    private void OnDestroy()
    {
        particle = Instantiate(particle, transform.position, transform.rotation);
        Destroy(particle, particleTime.duration);
    }
}
